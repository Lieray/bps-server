module.exports = {
  //此项是用来告诉eslint找当前配置文件不能往父级查找
  root: true,
  // 此项指定环境的全局变量
  env: {
    es6: true,
    node: true
  },
  // 此项是用来配置标准的js风格
  extends: [
    'standard'
  ],
  //在配置文件或注释中指定额外的全局变量
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  //此项是用来指定javaScript语言类型和风格
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  /* 
   下面这些rules是用来设置从插件来的规范代码的规则，使用必须去掉前缀eslint-plugin-
    主要有如下的设置规则，可以设置字符串也可以设置数字，两者效果一致
    "off" -> 0 关闭规则
    "warn" -> 1 开启警告规则
    "error" -> 2 开启错误规则
  */
  rules: {
    "no-console": 1,
    "no-var": 1
  }
}
