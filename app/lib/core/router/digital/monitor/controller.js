/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboard(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     * @param {string} type
     * @param {string} search
     * @param {string} rpstatus
     * @param {string} channel
     * @param {string} result
     * @param {string} level
     * @param {string} analysis
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      type: bodyParams.type || '',
      search: bodyParams.search || '',
      rpstatus: bodyParams.rpstatus || bodyParams.rp_status || '',
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      channel: bodyParams.channel || bodyParams.channelids || '',
      result: bodyParams.result || bodyParams.discrimnants || '',
      level: bodyParams.level || bodyParams.confidences || '',
      analysis: bodyParams.analysis || '',
      mainid: req.baseInfo.mainid
    }
    if (bodyParams.rp_status === 0) params.rpstatus = '0'

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     * @param {string} type
     * @param {string} search
     * @param {number} page
     * @param {string} rpstatus
     * @param {string} channel
     * @param {string} result
     * @param {string} level
     * @param {string} analysis
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      type: bodyParams.type || '',
      search: bodyParams.search || '',
      rpstatus: bodyParams.rpstatus || bodyParams.rp_status || '',
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      channel: bodyParams.channel || bodyParams.channelids || '',
      result: bodyParams.result || bodyParams.discrimnants || '',
      analysis: bodyParams.analysis || '',
      level: bodyParams.level || bodyParams.confidences || '',
      mainid: req.baseInfo.mainid
    }
    if (bodyParams.rpstatus === 0) params.rpstatus = '0'

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const feedback = async (req, res) => {
  try {
    const docs = await service.feedback()
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const report = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} id
     * @param {string} feedbackOption
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      dataid: bodyParams.id,
      project: bodyParams.project ||  bodyParams.subid,
      reportid: bodyParams.feedbackOption,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      content: bodyParams.feedbackContent
    }
    await service.report(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const complain = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      dataid: bodyParams.id,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      content: bodyParams.feedbackContent
    }
    await service.complain(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const project = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      id: bodyParams.project || bodyParams.id,
      mainid: req.baseInfo.mainid,
      type: 2
    }

    const docs = await service.project(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  project
}
