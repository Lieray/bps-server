/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}
const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.digital.filter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const dashboard = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project
    }
    const productCount = await service.digital.chart.dashboardCount(query)
    const productTotal = await service.digital.chart.dashboardTotal(query)
    const productRate = (productCount.FakeProductCount * 100 / productTotal.FakeProductCount).toFixed(2) + '%'
    const data = [
      [productCount],
      [{
        name: 'FakeProductRate',
        FakeProductRate: productRate
      }],
      await service.digital.chart.dashboardDaily(query),
      await service.digital.chart.dashboardChannel(query),
      await service.digital.chart.dashboardResult(query)
    ]
    return data
  } else return []
}

const filter = async (params) => {
  const userProject = await checkProject(params)

  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      search: params.search,
      type: params.type,
      rpstatus: params.rpstatus,
      channel: params.channel,
      result: params.result,
      analysis: params.analysis,
      level: params.level
    }

    const defaultStatus = await service.feedback.list.getStatusList()
    const statusList = await service.digital.filter.filterStatus(query)
    const formatList = []
    defaultStatus.forEach(rpStatus => {
      const defaultDoc = {
        Name: 'CountByRightsProtectionStatus',
        RightsProtectionStatus: rpStatus.index,
        CountNum: 0
      }
      const doc = statusList.find(t => t.RightsProtectionStatus === defaultDoc.RightsProtectionStatus)
      if (doc) formatList.push(doc)
      else formatList.push(defaultDoc)
    })

    const data = [
      formatList,
      await service.digital.filter.filterChannel(query),
      await service.digital.filter.filterType(query),
      await service.digital.filter.filterResult(query),
      await service.digital.filter.filterLevel(query)
    ]
    return data
  } else return []
}

const paging = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      search: params.search,
      type: params.type,
      rpstatus: params.rpstatus,
      channel: params.channel,
      result: params.result,
      level: params.level,
      analysis: params.analysis,
      skip: (params.page - 1) * params.size,
      size: params.size
    }
    const pageList = await service.digital.list.pageList(query)
    if (pageList.length) {
      const pageDocs = []
      pageList.forEach(pageData => {
        pageDocs.push(pageData.DataID)
      })
      const fblist = await service.feedback.specific.getByBoth({ mainid: params.mainid, type: 32, id: pageDocs.toString() })
      pageList.forEach(pageData => {
        const fbchecker = fblist.find(t => t.resultId === pageData.DataID.toString())
        if (fbchecker) pageData.Feedback = 1
        else pageData.Feedback = 0
      })
    }

    const data = [
      await service.digital.list.pageCount(query),
      pageList
    ]
    return data
  } else return []
}
const feedback = async () => {
  const data = await service.feedback.list.getOpts(3)
  return data
}

const report = async (params) => {
  const userProject = await checkProject(params)

  if (userProject.length) {
    const queryDocs = []
    const dataid = params.dataid.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    dataid.forEach(id => {
      queryDocs.push([
        params.mainid,
        params.userid,
        id,
        params.reportid || 0,
        params.content || '',
        32,
        recordid,
        id + '@32:' + params.mainid,
        params.project
      ])
    })
    // 首先应确认这些条目属于该用户
    const userDocs = await service.digital.filter.getByBoth({ project: userProject.toString(), id: params.dataid })
    // 直接进行记录
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      try {
        await service.feedback.specific.subContent(queryDocs)
      } catch (e) {
        throw new Error(`Error happens with the status code: 406, the key is: ${params.dataid}`)
      }
      service.feedback.specific.subReward({ mainid: params.mainid, userid: params.userid, type: 32, recordid })
    }
    // 记录完成后执行奖励操作
    return params
  } else return []
}

const complain = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryDocs = []
    const addDocs = []
    const extDocs = []
    const dataid = params.dataid.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    dataid.forEach(id => {
      queryDocs.push([
        params.mainid,
        params.userid,
        `"${id}"`,
        params.content || '',
        31,
        recordid,
        id + '@31'
      ])
      addDocs.push([
        params.mainid,
        params.userid,
        id,
        2,
        recordid
      ])
      extDocs.push([
        recordid,
        params.mainid,
        `"${id}"`,
        31,
        0,
        'complain'
      ])
    })
    // 首先应确认这些条目属于该用户
    const userDocs = await service.digital.filter.getByBoth({ project: userProject.toString(), id: params.dataid })
    // 执行事务依次进行扣款、记录操作、插入状态
    const docs = {
      mainid: params.mainid,
      userid: params.userid,
      recordid,
      cost: queryDocs.length
    }
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      const recordCheck = await service.feedback.specific.checkRecord({projectId: params.project, cost: queryDocs.length})
      if (recordCheck.changedRows) {
        await service.feedback.specific.compMonitor(docs, queryDocs, addDocs)
        // 插入扣款明细
        service.feedback.specific.compDetail(extDocs)
      }
    }
    return params
  } else return []
}

const project = async (params) => {
  const data = await service.package.content.getContent(params)
  return {
    success: true,
    project: data
  }
  return data
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  project
}
