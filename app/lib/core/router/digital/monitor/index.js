/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/digital/monitor/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/digital/monitor/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/digital/monitor/paging', auth.baseInfo, controller.paging)
router.post(baseUri + '/digital/monitor/report', auth.baseInfo, controller.report)
router.put(baseUri + '/digital/monitor/complain', auth.baseInfo, controller.complain)
router.get(baseUri + '/digital/monitor/feedback', auth.baseInfo, controller.feedback)
router.get(baseUri + '/digital/monitor/policy', auth.baseInfo, controller.project)

export default router
