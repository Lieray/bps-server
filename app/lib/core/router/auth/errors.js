/**
 * 统一的错误处理
 */
import nconf from 'nconf'
const logger = global.logger

const errorCode = {
  400: {
    status: 'LOGIN_PASSWORD_INVALID',
    msg: 'invalid credentials'
  },
  401: {
    status: 'LOGIN_NOTACTIVATE',
    msg: 'user jwt status is invalid/expired, requiring re-login'
  },
  403: {
    status: 'LOGIN_NOTACTIVATE',
    msg: 'user has not been activated yet'
  },
  404: {
    status: 'LOGIN_USERNOTEXIST_OR_INVALID',
    msg: 'user does not exist, please signup first'
  },
  500: {
    status: 'LOGIN_ERRROR',
    msg: 'internal error, please try again later'
  },
}
const errorDecoupling = (str) => {
  const cberr = {
    code: 500,
    message: 'internal error, please try again later'
  }
  try {
    const [, codeStr, , key] = str.split(/[:,]/)
    const code = Number.parseInt(codeStr)
    if (code && code in errorCode) {
      cberr.code = code
      cberr.key = key
      cberr.status = errorCode[code].status
      cberr.message = errorCode[code].msg
    }
    return cberr
  } catch (e) {
    cberr.status = errorCode[500].status
    cberr.message = errorCode[500].msg
    logger.warn(e.stack)
    return cberr
  }
}

const serverErr = (e) => {
  const status = nconf.get('LOGIN_STATUS')
  const bcJson = {
    login_status: status.LOGIN_ERRROR,
    msg: 'internal error, please try again later'
  }

  if (e.message.startsWith('Error happens with the status code:')) {
    logger.info(e.message)
    const eStack = errorDecoupling(e.message)
    bcJson.login_status = eStack.status === 404 ? 400 : eStack.status
    bcJson.msg = eStack.message
    return { eStack, bcJson }
  } else {
    logger.warn(e.stack)
    const eStack = {
      code: 500,
      message: errorCode[500]
    }
    return { eStack, bcJson }
  }
}

export default serverErr
