import serverErr from './errors'
import service from './assembly'

const baseInfo = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization || req.query.authToken,
      ip: req.ip
    }
    const tokenInfo = await service.baseInfo(authToken)
    req.baseInfo = tokenInfo
    next()
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

/**
 * 所有列表均须验证身份
 * 非VIP只能看3页
 * 超过3页则判定用户的VIP身份
 * 记点VIP点数耗光时也只能看3页
 */
const listInfo = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization,
      ip: req.ip,
      status: 1
    }
    if (req.query.page > 3) {
      const memberInfo = await service.pageInfo(authToken)
      req.baseInfo = memberInfo
      next()
    } else {
      const tokenInfo = await service.baseInfo(authToken)
      req.baseInfo = tokenInfo
      next()
    }
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

/**
 * 根据以下规则，此处只判定是否在效期内：
 * 1、VIP身份仅按照到期时间来判定
 * 2、VIP可查看自己开通权限的所有页面，不管剩余点数有多少
 */
const mallInfo = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization,
      ip: req.ip,
      status: 1
    }
    const memberInfo = await service.authInfo(authToken)
    req.baseInfo = memberInfo
    next()
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

/**
 * VIP专属页面的详情列表仅针对充值点数的VIP作限制
 */
const mallData = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization,
      ip: req.ip,
      status: 1
    }
    const memberInfo = await service.pageInfo(authToken)
    req.baseInfo = memberInfo
    next()
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const newsInfo = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization,
      ip: req.ip,
      status: 2
    }
    const newsInfo = await service.authInfo(authToken)
    req.baseInfo = newsInfo
    next()
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const newsData = async (req, res, next) => {
  try {
    const authToken = {
      token: req.headers.authorization,
      ip: req.ip,
      status: 2
    }
    const memberInfo = await service.pageInfo(authToken)
    req.baseInfo = memberInfo
    next()
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  baseInfo,
  listInfo,
  mallInfo,
  mallData,
  newsInfo,
  newsData
}
