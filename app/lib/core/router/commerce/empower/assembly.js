/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../components'

const getlist = async (docs) => {
  const data = await service.package.content.getStore({mainId: docs.mainid})
  return data
}

const getproject = async (docs) => {
  const query = {
    project: docs.project,
    mainid: docs.mainid,
    skip: (docs.page - 1) * docs.size,
    size: docs.size
  }

  const num = await service.package.empower.getCount(query)
  const data = await service.package.empower.getList(docs)
  return {
    count: num,
    list: data
  }
}

const addlist = async (params) => {
  let sqlStr = ''
  params.docs.forEach(element => {
    sqlStr += `(${params.mainid},${params.id}, 
      md5(concat("${element.name}","-","${element.channel}")),
      ${element.channel}, "${element.name}",`
      sqlStr += element.url? `"${element.url}",`: 'null,'
      sqlStr += element.desc? `"${element.desc}",`: 'null,'
      sqlStr += element.init? `"${element.init}",`: 'null,'
      sqlStr += element.expire? `"${element.expire}"),`: 'null),'
  })

  sqlStr = sqlStr.substring(0,sqlStr.length-1)

  const data = await service.package.empower.addlist(sqlStr)
  return data
}

const editlist = async (params) => {
  const data = await service.package.empower.editlist(params)
  return data
}


const setlist = async (params) => {
  const data = await service.package.empower.setlist(params)
  return data
}

export default {
  getlist,
  getproject,
  addlist,
  editlist,
  setlist
}
