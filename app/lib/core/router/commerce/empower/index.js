/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/empower/list', auth.baseInfo, controller.getlist)
router.get(baseUri + '/commerce/empower/project', auth.baseInfo, controller.getproject)
router.post(baseUri + '/commerce/empower/add', auth.baseInfo, controller.addlist)
router.put(baseUri + '/commerce/empower/edit', auth.baseInfo, controller.editlist)
router.put(baseUri + '/commerce/empower/delete', auth.baseInfo, controller.setlist)

export default router
