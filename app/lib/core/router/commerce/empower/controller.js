/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const getlist = async (req, res) => {
  try {
    const params = {
      mainid: req.baseInfo.mainid,
    }

    const docs = await service.getlist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getproject = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      project: bodyParams.project,
      mainid: req.baseInfo.mainid,
      page: Number.parseInt(bodyParams.page) || 1,
      size: 10
    }

    const docs = await service.getproject(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addlist = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.project,
      mainid: req.baseInfo.mainid,
      docs: bodyParams.data
    }
    await service.addlist(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editlist = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.id,
      project: bodyParams.project,
      mainid: req.baseInfo.mainid,
      channel: bodyParams.data,
      name: bodyParams.name,
      url: bodyParams.url
    }
    await service.editlist(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setlist = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.id,
      project: bodyParams.project,
      mainid: req.baseInfo.mainid
    }

    await service.setlist(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getlist,
  getproject,
  addlist,
  setlist,
  editlist
}
