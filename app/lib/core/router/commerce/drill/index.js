/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/project', auth.baseInfo, controller.project)
router.get(baseUri + '/commerce/drill/list', auth.baseInfo, controller.getlist)
router.put(baseUri + '/commerce/drill/report', auth.baseInfo, controller.report)
router.put(baseUri + '/commerce/drill/change', auth.baseInfo, controller.change)
router.put(baseUri + '/commerce/drill/finish', auth.baseInfo, controller.setpro)

export default router
