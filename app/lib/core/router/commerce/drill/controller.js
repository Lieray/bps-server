/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const getlist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      project: bodyParams.project,
      mainid: req.baseInfo.mainid,
      page: Number.parseInt(bodyParams.page) || 1,
      size: 10
    }

    const docs = await service.paging(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const report = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} project
     * @param {string} id
     * @param {string} result
     */
    const bodyParams = req.body
    const params = {
      resultid: bodyParams.id,
      project: bodyParams.project,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      result: bodyParams.result
    }
    await service.report(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const change = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} project
     * @param {string} id
     * @param {string} result
     */
    const bodyParams = req.body
    const params = {
      resultid: bodyParams.id,
      project: bodyParams.project,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      result: bodyParams.result
    }
    await service.change(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setpro = async (req, res) => {
  try {
    const bodyParams = req.body
    const params = {
      project: bodyParams.project,
      mainid: req.baseInfo.mainid
    }

    await service.setpro(params)
    return res.json({
      success: true
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const project = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      id: bodyParams.project || bodyParams.id || bodyParams.subid,
      mainid: req.baseInfo.mainid,
      type: 1
    }

    const docs = await service.project(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getlist,
  report,
  change,
  setpro,
  project
}
