/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.package.project.getContent({id: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.content.project) {
      userProject.push.apply(userProject,element.content.project.docs)
    }
  })
  return { userBrand: userProject }
}

const paging = async (params) => {
  // 这里不仅要获取用户关联的brand，还要检查白名单
  const checkResult = await checkProject(params)
  const userBrand = checkResult.userBrand

  if (userBrand.length) {
    const query = {
      project: userBrand[0],
      order: Number.parseInt(params.order) || 0,
      skip: (params.page - 1) * params.size,
      size: params.size
    }

    const pageList = await service.item.list.getList(query)
    if (pageList.length) {
      const pageDocs = [0]
      pageList.forEach(pageData => {
        pageDocs.push(pageData.ResultId)
      })

      // 这里要检查已经提交过的信息
      const fblist = await service.feedback.drill.getList({ mainid: params.mainid,project: query.project, id: pageDocs.toString() })

      pageList.forEach(pageData => {
        const fbchecker = fblist.find(t => t.resultId === pageData.ResultId)
        if (fbchecker) {
          pageData.Feedback = 1
          pageData.DiscriminantResult = fbchecker.result
        }
        else pageData.Feedback = 0
      })
    }
    const totalList = await service.feedback.drill.getCount({mainid: params.mainid,project: query.project})
    const data = [
      [{Feedback: totalList.length}],
      await service.item.list.getCount(query),
      pageList
    ]
    return data
  } else return []
}

const report = async (params) => {
  const checkResult = await checkProject(params)
  const userBrand = checkResult.userBrand

  if (userBrand.length) {
    const query = {
      project: userBrand[0],
      resultid: params.resultid,
      userid: params.userid,
      mainid: params.mainid,
      result: params.result,
      status: params.result > 1 ? 0: 1
    }
    // 直接记录
    await service.feedback.drill.addItem(query)
    return params
  } else return params
}

const change = async (params) => {
  const checkResult = await checkProject(params)
  const userBrand = checkResult.userBrand

  if (userBrand.length) {
    const query = {
      project: userBrand[0],
      resultid: params.resultid,
      userid: params.userid,
      mainid: params.mainid,
      result: params.result,
      status: params.result > 1 ? 0: 1
    }
    // 直接记录
    await service.feedback.drill.editItem(query)
    return params
  } else return params
}

const setpro = async (params) => {
  await service.package.project.setProject(params)
  return []
}

const project = async (params) => {
  // // 获取当前项目
  // const proData = await service.package.content.getContent(params)
  // // 获取项目的申请信息
  // const appData = await service.commerce.apply.getContent(params)

  return {
    success: true,
    project: {
      id: params.id,
      status: 3,
      step: 3
    },
    application: []
  }
}

export default {
  paging,
  report,
  change,
  setpro,
  project
}
