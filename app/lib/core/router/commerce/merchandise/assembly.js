/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.commerce.filter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const dashboard = async (params) => {
  const userBrand = await checkProject(params)

  if (userBrand.length) {

    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project
    }
    const productCount = await service.commerce.chart.itemCount(query)
    const productTotal = await service.commerce.chart.itemTotal(query)
    const productRate = (productCount.FakeProductCount * 100 / productTotal.FakeProductCount).toFixed(2) + '%'
    const data = [
      [productCount],
      [{
        name: 'FakeProductRate',
        FakeProductRate: productRate
      }],
      await service.commerce.chart.itemDaily(query),
      await service.commerce.chart.itemChannel(query)
    ]
    return data
  } else return []
}

const filter = async (params) => {
  const userBrand = await checkProject(params)
  const whiteList = []
  if (userBrand.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      rpstatus: params.rpstatus,
      channel: params.channel,
      search: params.search,
      result: params.result,
      level: params.level,
      bucket: params.bucket
    }

    if (whiteList.length) {
      let whiteShop = '"0"'
      whiteList.forEach(element => {
        whiteShop += `,"${element}"`
      })
      if (Number.parseInt(params.authorize) === 0) query.whiteList = ` shopId NOT IN (${whiteShop}) AND `
      if (Number.parseInt(params.authorize) === 1) query.whiteList = ` shopId IN (${whiteShop}) AND `
    }

    const defaultStatus = await service.feedback.list.getStatusList()
    const statusList = await service.commerce.filter.itemfilterStatus(query)
    console.log(0)
    const formatList = []
    defaultStatus.forEach(rpStatus => {
      const defaultDoc = {
        Name: 'CountByRightsProtectionStatus',
        RightsProtectionStatus: rpStatus.index,
        CountNum: 0
      }
      const doc = statusList.find(t => t.RightsProtectionStatus === defaultDoc.RightsProtectionStatus)
      if (doc) formatList.push(doc)
      else formatList.push(defaultDoc)
    })

    const data = [
      formatList,
      await service.commerce.filter.itemfilterChannel(query),
      [{
        id: 1,
        data: '可靠'
      }, {
        id: 0,
        data: '相仿'
      }],
      [{
        id: 1,
        data: '高'
      }, {
        id: 2,
        data: '中'
      }, {
        id: 3,
        data: '低'
      }],
      [{
        id: 1,
        data: '授权'
      }, {
        id: 0,
        data: '未授权'
      }]
    ]
    return data
  } else return []
}

const paging = async (params) => {
  // 这里不仅要获取用户关联的brand，还要检查白名单
  const userBrand = await checkProject(params)
  const whiteList = []

  if (userBrand.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      rpstatus: params.rpstatus,
      channel: params.channel,
      search: params.search,
      result: params.result,
      level: params.level,
      bucket: params.bucket,
      order: Number.parseInt(params.order) || 0,
      skip: (params.page - 1) * params.size,
      size: params.size
    }

    if (whiteList.length) {
      let whiteShop = '"0"'
      whiteList.forEach(element => {
        whiteShop += `,"${element}"`
      })
      if (Number.parseInt(params.authorize) === 0) query.whiteList = ` shopId NOT IN (${whiteShop}) AND `
      if (Number.parseInt(params.authorize) === 1) query.whiteList = ` shopId IN (${whiteShop}) AND `
    }
    const pageList = await service.commerce.list.itempageList(query)
    if (pageList.length) {
      const pageDocs = []
      pageList.forEach(pageData => {
        pageDocs.push(pageData.ResultId)
        pageData.authorize = 0
        if (whiteList && whiteList.length) {
          const shopInfo = whiteList.find(t => t.toString() === pageData.shopId.toString())
          if (shopInfo) pageData.authorize = 1
        }
      })
      const fblist = await service.feedback.specific.getByBoth({ mainid: params.mainid, type: 12, id: pageDocs.toString() })
      pageList.forEach(pageData => {
        const fbchecker = fblist.find(t => t.resultId.toString() === pageData.ResultId.toString())
        if (fbchecker) pageData.Feedback = 1
        else pageData.Feedback = 0
      })
    }

    const data = [
      await service.commerce.list.itempageCount(query),
      pageList
    ]
    return data
  } else return []
}
const feedback = async () => {
  const data = await service.feedback.list.getOpts(1)
  return data
}

const report = async (params) => {
  const userBrand = await checkProject(params)
  if (userBrand.length) {
    const queryDocs = []
    const resultid = params.resultid.toString().split(',')
    const resultSet = params.result.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    resultid.forEach(id => {
      queryDocs.push([
        params.mainid,
        params.userid,
        id,
        params.reportid || 0,
        params.content || '',
        12,
        recordid,
        id + '@12:' + params.mainid,
        params.project
      ])
    })
    // 区分是否需要更新判定结果和确信度
    const bucketLevel = {
      faked: [],
      certified: [],
      record: ''
    }

    resultSet.forEach((value, index) => {
      if (Number.parseInt(value) === 0) {
        bucketLevel.faked.push(resultid[index])
        bucketLevel.record += `(${params.mainid},${params.userid},${resultid[index]},${params.project},0,12),`
      }
      if (Number.parseInt(value) === 1) {
        bucketLevel.certified.push(resultid[index])
        bucketLevel.record += `(${params.mainid},${params.userid},${resultid[index]},${params.project},1,12),`
      }
    })

    // 首先应确认这些条目属于该用户
    const userDocs = await service.commerce.filter.getitemByBoth({ project: userBrand[0], id: params.resultid })
    // 直接进行记录
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      try {
        await service.feedback.specific.subContent(queryDocs)
      } catch (e) {
        throw new Error(`Error happens with the status code: 406, the key is: ${params.resultid}`)
      }
      // 记录完成后执行奖励操作
      service.feedback.specific.subReward({ mainid: params.mainid, userid: params.userid, type: 12, recordid })
      // 修改结果
      if (bucketLevel.faked.length) {
        const changeData = {
          result: 0,
          level: 6,
          resultId: bucketLevel.faked.toString()
        }
        service.feedback.specific.changeResult(changeData)
      }
      if (bucketLevel.certified.length) {
        const changeData = {
          result: 1,
          level: 1,
          resultId: bucketLevel.certified.toString()
        }
        service.feedback.specific.changeResult(changeData)
      }
      if (bucketLevel.record.length) {
        const statusRecord = bucketLevel.record.substring(0,bucketLevel.record.length-1)
        service.feedback.specific.setFeedback(statusRecord)
      }
    }
    return params
  } else return []
}

const complain = async (params) => {
  const userBrand = await checkProject(params)
  if (userBrand.length) {
    const queryDocs = []
    const addDocs = []
    const extDocs = []
    const resultid = params.resultid.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    resultid.forEach(id => {
      queryDocs.push([
        params.mainid,
        params.userid,
        `"${id}"`,
        params.content || '',
        11,
        recordid,
        id + '@11'
      ])
      addDocs.push([
        params.mainid,
        params.userid,
        id,
        2,
        recordid
      ])
      extDocs.push([
        recordid,
        params.mainid,
        `"${id}"`,
        11,
        0,
        'complain'
      ])
    })
    // 首先应确认这些条目属于该用户
    const userDocs = await service.order.item.getitemByBoth({ project: userBrand[0], id: params.resultid })
    // 执行事务依次进行扣款、记录操作、插入状态
    const docs = {
      mainid: params.mainid,
      userid: params.userid,
      recordid,
      cost: queryDocs.length
    }
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      const recordCheck = await service.feedback.specific.checkRecord({projectId: userBrand[0], cost: queryDocs.length})
      if (recordCheck.changedRows) {
        await service.feedback.specific.compItem(docs, queryDocs, addDocs)
        // 修改条目状态
        await service.feedback.specific.changeStatus({resultId: params.resultid.toString()})
        // 插入扣款明细
        service.feedback.specific.compDetail(extDocs)
      }
    }
    /**
     * 生成工单
     */
    // 生成工单内容
    const orderDocs = []
    userDocs.forEach(iteminfo => {
      orderDocs.push({
        id: iteminfo.ResultId,
        email: params.email,
        title: '链接投诉任务',
        comment: `信息：${iteminfo.ProductDescription}，链接：${iteminfo.producturl}`
      })
    })
    // 提交工单
    await service.order.item.sendOrder(orderDocs)

    return params
  } else return []
}

const policy = async (params) => {
  const checkResult = await checkProject(params)
  return checkResult
}

const getrank = async (params) => {
  const data = await service.feedback.specific.getCount(params)
  return data
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  policy,
  getrank
}
