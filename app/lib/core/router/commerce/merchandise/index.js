/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/merchandise/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/commerce/merchandise/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/commerce/merchandise/paging', auth.baseInfo, controller.paging)
router.get(baseUri + '/commerce/merchandise/rank', auth.baseInfo, controller.getrank)
router.get(baseUri + '/commerce/merchandise/feedback', auth.baseInfo, controller.feedback)
router.post(baseUri + '/commerce/merchandise/report', auth.baseInfo, controller.report)
router.put(baseUri + '/commerce/merchandise/complain', auth.baseInfo, controller.complain)
router.get(baseUri + '/commerce/merchandise/policy', auth.baseInfo, controller.policy)

export default router
