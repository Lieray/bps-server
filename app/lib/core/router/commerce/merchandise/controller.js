/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboard(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} rpstatus
     * @param {string} channel
     * @param {number} result
     * @param {number} bucket
     * @param {number} authorize
     * @param {number} project
     */
    const bodyParams = req.query
    const bucketInfo = checkBucket(bodyParams.result, bodyParams.bucket)
    const params = {
      start: bodyParams.start,
      end: bodyParams.end,
      rpstatus: bodyParams.rpstatus,
      project: bodyParams.project || bodyParams.subid,
      channel: bodyParams.channel,
      authorize: bodyParams.authorize,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.filter(Object.assign(params, bucketInfo))
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} search
     * @param {number} page
     * @param {string} rpstatus
     * @param {string} channel
     * @param {number} result
     * @param {number} bucket
     * @param {number} authorize
     */
    const bodyParams = req.query
    const bucketInfo = checkBucket(bodyParams.result, bodyParams.bucket)

    const params = {
      project: bodyParams.project || bodyParams.subid,
      search: bodyParams.search || '',
      rpstatus: bodyParams.rpstatus,
      start: bodyParams.start,
      end: bodyParams.end,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      channel: bodyParams.channel,
      authorize: bodyParams.authorize,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.paging(Object.assign(params, bucketInfo))
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const checkBucket = (result, bucket) => {
  const cbData = {
    result: '',
    level: '',
    bucket: ''
  }
  if (result) {
    switch (Number.parseInt(result)) {
      case 0:
        cbData.result = '0'
        break
      case 1:
        cbData.result = '1'
        break
      default:
        cbData.result = ''
    }
  }
  if (Number.parseInt(bucket)) {
    // 选择真假，并筛选确信度
    if (cbData.result) {
      cbData.level = Number.parseInt(cbData.result) ? Number.parseInt(bucket) : 7 ^ Number.parseInt(bucket)
      cbData.result = ''
    } else {
    // 只筛选确信度
      cbData.bucket = (Number.parseInt(bucket) === 1 && '1,6') || (Number.parseInt(bucket) === 2 && '2,5') ||
                      (Number.parseInt(bucket) === 3 && '3,4') || ''
    }
  }
  return cbData
}

const feedback = async (req, res) => {
  try {
    const docs = await service.feedback()
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const report = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} project
     * @param {string} id
     * @param {string} feedbackOption
     * @param {string} feedbackContent
     * @param {string} feedbackResult
     */
    const bodyParams = req.body
    const params = {
      resultid: bodyParams.id,
      project: bodyParams.project || bodyParams.subid,
      reportid: bodyParams.feedbackOption,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      content: bodyParams.feedbackContent,
      result: bodyParams.feedbackResult
    }
    await service.report(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const complain = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} project
     * @param {string} id
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      project: bodyParams.project || bodyParams.subid,
      resultid: bodyParams.id,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      email: req.baseInfo.email,
      content: bodyParams.feedbackContent
    }
    await service.complain(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const policy = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.policy(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getrank = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid,
      mainid: req.baseInfo.mainid,
      type: 12
    }

    const docs = await service.getrank(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  policy,
  getrank
}
