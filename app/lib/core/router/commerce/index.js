/**
 * 配置可用模块
 */
import apply from './apply'
import drill from './drill'
import merchandise from './merchandise'
import vendor from './vendor'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  merchandise,
  vendor,
  apply,
  drill
}
