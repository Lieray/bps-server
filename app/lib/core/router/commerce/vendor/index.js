/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/vendor/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/commerce/vendor/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/commerce/vendor/paging', auth.baseInfo, controller.paging)
router.get(baseUri + '/commerce/vendor/feedback', auth.baseInfo, controller.feedback)
router.post(baseUri + '/commerce/vendor/report', auth.baseInfo, controller.report)
router.put(baseUri + '/commerce/vendor/complain', auth.baseInfo, controller.complain)
router.get(baseUri + '/commerce/vendor/rank', auth.baseInfo, controller.getrank)

export default router
