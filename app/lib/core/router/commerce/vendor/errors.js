/**
 * 统一的错误处理
 */
const logger = global.logger

const errorCode = {
  400: 'The param is illegal: ',
  403: 'Balance needed: ',
  406: 'The id had already been reported: ',
  500: 'Server Error!'
}
const errorDecoupling = (str) => {
  const cberr = {
    code: 500,
    message: str
  }
  try {
    const [, codeStr, , key] = str.split(/[:,]/)
    const code = Number.parseInt(codeStr)
    if (code && code in errorCode) {
      cberr.code = code
      cberr.message = errorCode[code] + key
    }
    return cberr
  } catch (e) {
    cberr.message = errorCode[500]
    logger.warn(e.stack)
    return cberr
  }
}

const serverErr = (e) => {
  const bcJson = {
    success: false,
    msg: 'Internal server error, please try again later'
  }
  if (e.message && e.message.startsWith('Error happens with the status code:')) {
    logger.info(e.message)
    const eStack = errorDecoupling(e.message)
    bcJson.msg = eStack.message
    return { eStack, bcJson }
  } else {
    logger.warn(e.stack)
    const eStack = {
      code: 500,
      message: errorCode[500]
    }
    return { eStack, bcJson }
  }
}

logger.silly('model: register')
logger.silly('router:')
logger.silly('   post /signup')

export default serverErr
