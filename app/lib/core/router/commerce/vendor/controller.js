/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const feedback = async (req, res) => {
  try {
    const docs = await service.feedback()
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} brand
     * @param {string} category
     * @param {date} start
     * @param {date} end
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid,
      brand: bodyParams.brand,
      category: bodyParams.category || 0,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboard(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} brand
     * @param {string} category
     * @param {string} rpstatus
     * @param {string} search
     * @param {string} shop
     * @param {string} channel
     * @param {number} authorize
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project || bodyParams.subid,
      rpstatus: bodyParams.rpstatus,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      brand: bodyParams.brand || bodyParams.brandids,
      category: bodyParams.category || 0,
      shop: bodyParams.shop || '',
      search: bodyParams.search || '',
      channel: bodyParams.channel || bodyParams.channelids || '',
      authorize: bodyParams.authorize,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} brand
     * @param {string} category
     * @param {string} rpstatus
     * @param {string} search
     * @param {string} shop
     * @param {string} channel
     * @param {number} page
     * @param {number} authorize
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project || bodyParams.subid,
      rpstatus: bodyParams.rpstatus,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      brand: bodyParams.brand || bodyParams.brandids,
      category: bodyParams.category || 0,
      shop: bodyParams.shop || '',
      search: bodyParams.search || '',
      channel: bodyParams.channel || bodyParams.channelids || '',
      authorize: bodyParams.authorize,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const report = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      resultid: bodyParams.id,
      project: bodyParams.project || bodyParams.subid,
      reportid: bodyParams.feedbackOption,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      content: bodyParams.feedbackContent
    }
    await service.report(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const complain = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      resultid: bodyParams.id,
      project: bodyParams.project || bodyParams.subid,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      content: bodyParams.feedbackContent
    }
    await service.complain(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getrank = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid,
      mainid: req.baseInfo.mainid,
      type: 12
    }

    const docs = await service.getrank(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  getrank
}
