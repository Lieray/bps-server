/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.commerce.filter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const dashboard = async (params) => {
  const userBrand = await checkProject(params)
  const whiteList = []
  if (userBrand.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project
    }
    const shopCount = await service.commerce.chart.shopCount(query)
    const shopTotal = await service.commerce.chart.shopTotal(query)
    const shopRate = (shopCount.FakeShopCount * 100 / shopTotal.FakeShopCount).toFixed(2) + '%'

    const whiteCount = []
    const whiteRate = []
    if (whiteList && whiteList.length) {
      const whiteShop = []
      whiteList.forEach(element => {
        whiteShop.push(`"${element}"`)
      })
      const whiteQuery = {
        start: querDate.start,
        end: querDate.end,
        project: queryCheck.project,
        whiteShop: whiteShop.toString()
      }
      const showCount = await service.commerce.chart.shopCount(whiteQuery)
      const showRate = (showCount.FakeShopCount * 100 / shopTotal.FakeShopCount).toFixed(2) + '%'
      whiteCount.push(showCount)
      whiteRate.push({
        name: 'FakeShopRate',
        FakeShopRate: showRate
      })
    }

    const data = [
      [shopCount],
      [{
        name: 'FakeShopRate',
        FakeShopRate: shopRate
      }], whiteCount, whiteRate,
      await service.commerce.chart.shopChannel(query)
    ]
    return data
  } else return []
}

const filter = async (params) => {
  const userBrand = await checkProject(params)
  const whiteList = []
  if (userBrand.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      channel: params.channel,
      search: params.search,
      shop: params.shop,
      project: queryCheck.project
    }

    const statusContent = []
    if (whiteList && whiteList.length) {
      let whiteShop = '"0"'
      whiteList.forEach(element => {
        whiteShop += `,"${element}"`
      })
      query.whiteShop = whiteShop
      if (Number.parseInt(params.authorize) === 0) query.whiteList = ` shopId NOT IN (${whiteShop}) AND `
      if (Number.parseInt(params.authorize) === 1) query.whiteList = ` shopId IN (${whiteShop}) AND `
      const filterContent = await service.commerce.filter.shopfilterStatus(query)

      const defaultStatus = await service.feedback.list.getStatusList()
      defaultStatus.forEach(rpStatus => {
        if (rpStatus.index) {
          const defaultDoc = {
            RightsProtectionStatus: rpStatus.index,
            FakeShopCount: 0
          }
          const doc = filterContent.find(t => t.RightsProtectionStatus === defaultDoc.RightsProtectionStatus)
          if (doc) statusContent.push(doc)
          else statusContent.push(defaultDoc)
        }
      })
    }
    const data = [
      await service.commerce.filter.shopfilterCount(query),
      statusContent,
      await service.commerce.filter.shopfilterChannel(query)
    ]
    return data
  } else return []
}

const paging = async (params) => {
  const userBrand = await checkProject(params)
  const whiteList = []
  if (userBrand.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userBrand[0]
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      search: params.search,
      shop: params.shop,
      skip: (params.page - 1) * params.size,
      size: params.size
    }

    if (whiteList && whiteList.length) {
      query.authList = whiteList
      let whiteShop = '"0"'
      whiteList.forEach(element => {
        whiteShop += `,"${element}"`
      })
      query.whiteShop = whiteShop
      if (Number.parseInt(params.authorize) === 0) query.whiteList = ` shopId NOT IN (${whiteShop}) AND `
      if (Number.parseInt(params.authorize) === 1) query.whiteList = ` shopId IN (${whiteShop}) AND `
    }

    if (whiteList && whiteList.length && params.rpstatus) {
      query.rpstatus = params.rpstatus
      const pageList = await service.commerce.list.shopstatusList(query)
      const pageDocs = []
      if (pageList.length) {
        pageList.forEach(pageData => { pageDocs.push(`"${pageData.ShopId}"`) })
        const fblist = await service.feedback.specific.getByBoth({ mainid: params.mainid, type: 22, id: pageDocs.toString() })
        pageList.forEach(pageData => {
          const fbcheck = fblist.find(t => t.resultId.toString() === pageData.ShopId.toString())
          if (fbcheck) pageData.Feedback = 1
          else pageData.Feedback = 0
        })
      }

      const data = [
        await service.commerce.list.shopstatusCount(query),
        pageList
      ]
      return data
    } else {
      const pageDocs = []
      const pageList = await service.commerce.list.shoppageList(query)
      if (pageList.length) {
        pageList.forEach(pageData => { pageDocs.push(`"${pageData.ShopId}"`) })
        const fblist = await service.feedback.specific.getByBoth({ mainid: params.mainid, type: 22, id: pageDocs.toString() })
        pageList.forEach(pageData => {
          const fbcheck = fblist.find(t => t.resultId.toString() === pageData.ShopId.toString())
          if (fbcheck) pageData.Feedback = 1
          else pageData.Feedback = 0
        })
      }
      const data = [
        await service.commerce.list.shoppageCount(query),
        pageList
      ]
      return data
    }
  } else return []
}
const feedback = async () => {
  const data = await service.feedback.list.getOpts(2)
  return data
}

const report = async (params) => {
  const userBrand = await checkProject(params)

  if (userBrand.length) {
    const queryDocs = []
    const resultid = params.resultid.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    resultid.forEach(id => {
      queryDocs.push([
        params.mainid,
        params.userid,
        id,
        params.reportid || 0,
        params.content || '',
        22,
        recordid,
        id + '@22:' + params.mainid,
        params.project
      ])
    })
    // 首先应确认这些条目属于该用户
    const userDocs = await service.commerce.filter.getShopByBoth({ project: userBrand.toString(), id: resultid.map((ret) => { return `"${ret}"` }) })
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      await service.feedback.specific.subContent(queryDocs)
      // 记录完成后执行奖励操作
      service.feedback.specific.subReward({ mainid: params.mainid, userid: params.userid, type: 12, recordid })
    }
    return params
  } else return []
}

const complain = async (params) => {
  const userBrand = await checkProject(params)

  if (userBrand.length) {
    const queryDocs = []
    const addDocs = []
    const extDocs = []
    const resultid = params.resultid.toString().split(',')
    const token = service.verify.phone.tokenGen()
    const recordid = token.uuid
    let statusItem = ''

    resultid.forEach(id => {
      statusItem += `(${params.project},${params.mainid},${params.userid},"${id}",2,"recordid"),`
      queryDocs.push([
        params.mainid,
        params.userid,
        id,
        params.content || '',
        21,
        recordid,
        id + '@21'
      ])
      addDocs.push([
        params.mainid,
        params.userid,
        id,
        2,
        recordid
      ])
      extDocs.push([
        recordid,
        params.mainid,
        id,
        21,
        0,
        'complain'
      ])
    })
    // 首先应确认这些条目属于该用户
    const userDocs = await service.commerce.filter.getShopByBoth({ project: userBrand.toString(), id: resultid.map((ret) => { return `"${ret}"` }) })
    // 执行事务依次进行扣款、记录操作、插入状态
    const docs = {
      mainid: params.mainid,
      userid: params.userid,
      recordid,
      cost: queryDocs.length
    }
    if (queryDocs.length && queryDocs.length === userDocs.length) {
      const recordCheck = await service.feedback.specific.checkRecord({projectId: params.project, cost: queryDocs.length})
      if (recordCheck.changedRows) {
        await service.feedback.specific.compStore(docs, queryDocs, addDocs)
        statusItem = statusItem.substring(0,statusItem.length-1)

        await service.feedback.specific.addStatus(statusItem)

        // 插入扣款明细
        service.feedback.specific.compDetail(extDocs)
      }
    }

    /**
     * 生成工单
     */
    // 生成工单内容
    const orderDocs = []
    userDocs.forEach(iteminfo => {
      orderDocs.push({
        id: iteminfo.ShopId,
        email: params.email,
        title: '店铺投诉任务',
        comment: `信息：${iteminfo.ShopName}，链接：${iteminfo.ShopURL}`
      })
    })
    // 提交工单
    await service.order.item.sendOrder(orderDocs)


    return params
  } else return []
}

const getrank = async (params) => {
  const data = await service.feedback.specific.getCount(params)
  return data
}

export default {
  dashboard,
  filter,
  paging,
  feedback,
  report,
  complain,
  getrank
}
