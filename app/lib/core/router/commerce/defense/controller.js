/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const categories = async (_req, res) => {
  try {
    const docs = await service.categoryList()
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const topbrand = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      category: Number.parseInt(bodyParams.category) || 1
    }

    const docs = await service.topbrand(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const feedback = async (req, res) => {
  try {
    const docs = await service.feedback()
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const report = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      userid: req.baseInfo.userid,
      content: bodyParams.feedbackContent
    }
    const ret = {
      success: false
    }
    if (!params.content) ret.message = 'FeedbackContent needed'
    else if (params.content.length > 200) ret.message = 'Too long'
    else {
      await service.report(params)
      ret.success = true
      ret.message = 'OK'
    }

    return res.json(ret)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const project = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      id: bodyParams.project || bodyParams.id || bodyParams.subid,
      mainid: req.baseInfo.mainid,
      type: 1
    }

    const docs = await service.project(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  categories,
  topbrand,
  feedback,
  report,
  project
}
