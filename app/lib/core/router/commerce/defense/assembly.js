/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../components'

const categoryList = async () => {
  const data = await service.view.brand.categoryList()
  return data
}

const topbrand = async (docs) => {
  const data = await service.view.brand.topbrand(docs)
  return data
}

const feedback = async () => {
  const data = await service.feedback.list.getOpts()
  return data
}

const report = async (params) => {
  const data = await service.feedback.commom.subContent(params)
  return data
}


const project = async (params) => {
  // 获取当前项目
  const proData = await service.package.content.getContent(params)
  // 获取项目的申请信息
  const appData = await service.package.apply.getContent(params)

  return {
    success: true,
    project: proData,
    application: appData
  }
}

export default {
  categoryList,
  topbrand,
  feedback,
  report,
  project
}
