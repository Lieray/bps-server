/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/category', auth.baseInfo, controller.categories)
router.get(baseUri + '/commerce/topbrand', auth.baseInfo, controller.topbrand)
router.get(baseUri + '/commerce/feedback', auth.baseInfo, controller.feedback)
router.post(baseUri + '/commerce/report', auth.baseInfo, controller.report)
router.get(baseUri + '/commerce/project', auth.baseInfo, controller.project)

export default router
