/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/apply/list', auth.baseInfo, controller.getlist)
router.get(baseUri + '/commerce/apply/view', auth.baseInfo, controller.getview)
router.post(baseUri + '/commerce/apply/info', auth.baseInfo, controller.addinfo)
router.put(baseUri + '/commerce/apply/edit', auth.baseInfo, controller.editinfo)
router.put(baseUri + '/commerce/apply/delete', auth.baseInfo, controller.setinfo)

export default router
