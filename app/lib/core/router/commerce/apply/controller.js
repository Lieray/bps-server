/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const getlist = async (req, res) => {
  try {
    const bodyParams = req.query
    const params = {
      mainid: req.baseInfo.mainid,
      page: Number.parseInt(bodyParams.page) || 1,
      size: 10
    }

    const docs = await service.getlist(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getview = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      id: bodyParams.id,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.getview(params)
    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const addinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      mainid: req.baseInfo.mainid,
      project: bodyParams.project  || bodyParams.subid,
      name: bodyParams.name,
      brand: bodyParams.brand,
      shortname: bodyParams.shortname || '',
      nickname: bodyParams.nickname || '',
      series: bodyParams.series || '',
      article: bodyParams.article || '',
      product: bodyParams.product || '',
      alias: bodyParams.alias || '',
      abbreviation: bodyParams.abbreviation || '',
      channel: bodyParams.channel || ''
    }
    await service.addinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const editinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.id,
      mainid: req.baseInfo.mainid,
      name: bodyParams.name,
      brand: bodyParams.brand,
      shortname: bodyParams.shortname || '',
      nickname: bodyParams.nickname || '',
      series: bodyParams.series || '',
      article: bodyParams.article || '',
      product: bodyParams.product || '',
      alias: bodyParams.alias || '',
      abbreviation: bodyParams.abbreviation || '',
      channel: bodyParams.channel || ''
    }
    await service.editinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setinfo = async (req, res) => {
  try {
    const bodyParams = req.body

    const params = {
      id: bodyParams.id,
      mainid: req.baseInfo.mainid
    }

    await service.setinfo(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getlist,
  getview,
  addinfo,
  setinfo,
  editinfo
}
