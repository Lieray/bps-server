/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const getlist = async (docs) => {

  const query = {
    mainid: docs.mainid,
    skip: (docs.page - 1) * docs.size,
    size: docs.size
  }

  const num = await service.commerce.apply.getCount(query)
  const data = await service.commerce.apply.getList(query)
  return {
    count: num,
    list: data
  }
}

const getview = async (docs) => {
  const data = await service.commerce.apply.getview(docs)
  return data
}

const addinfo = async (docs) => {
  const data = await service.commerce.apply.addinfo(docs)
  return data
}

const editinfo = async (params) => {
  const data = await service.commerce.apply.editinfo(params)
  return data
}


const setinfo = async (params) => {
  const data = await service.commerce.apply.setinfo(params)
  return data
}

export default {
  getlist,
  getview,
  addinfo,
  editinfo,
  setinfo
}
