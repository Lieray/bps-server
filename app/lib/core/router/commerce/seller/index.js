/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/seller/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/commerce/seller/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/commerce/seller/paging', auth.baseInfo, controller.paging)

export default router
