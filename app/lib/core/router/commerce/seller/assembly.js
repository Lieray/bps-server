/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../components'

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }
  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.mall.brandfilter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
  }
  return service.view.brand.dateFormat(paramDate)
}

const dashboard = async (params) => {
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    brand: params.brand
  }

  const shopCount = await service.mall.brandchart.dashboardCount(query)
  const shopTotal = await service.mall.brandchart.dashboardTotal(query)

  const shopRate = (shopCount.FakeShopCount * 100 / shopTotal.FakeShopCount).toFixed(2) + '%'

  const channelData = await service.mall.brandchart.dashboardChannel(query)
  const fakeData = []
  channelData.forEach(element => {
    if (element["FakeShopStatusByChannel_Result"] === 0) {
      const fakeDoc = {
        channelId : element["FakeShopStatusByChannel_ChannelId"],
        channelName : element["FakeShopStatusByChannel_ChannelName"],
        shopCount : element["FakeShopStatusByChannel_ShopCount"],
        shopRate : element["FakeShopStatusByChannel_ShopCount"]/shopCount.FakeShopCount,
      }
      fakeData.push(fakeDoc)
    }
  })

  const data = [
    [shopCount],
    [{
      name: 'FakeShopRate',
      FakeShopRate: shopRate
    }],
    fakeData,
    channelData
  ]
  return data
}

const filter = async (params) => {
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    brand: params.brand
  }

  const data = [
    await service.mall.brandfilter.filterChannel(query)
  ]
  return data
}

const paging = async (params) => {
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    brand: params.brand,
    search: params.search,
    channel: params.channel,
    skip: (params.page - 1) * params.size,
    size: params.size
  }

  const data = [
    await service.mall.brandlist.pageCount(query),
    await service.mall.brandlist.pageList(query)
  ]
  return data
}

export default {
  dashboard,
  filter,
  paging
}
