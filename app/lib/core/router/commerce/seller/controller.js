/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} brand
     * @param {string} category
     * @param {date} start
     * @param {date} end
     */
    const bodyParams = req.query
    const params = {
      brand: bodyParams.brand,
      category: bodyParams.category || 1,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date
    }

    const docs = await service.dashboard(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} brand
     * @param {string} category
     */
    const bodyParams = req.query

    const params = {
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      brand: bodyParams.brand || bodyParams.brandids,
      category: bodyParams.category || 1
    }

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {date} start
     * @param {date} end
     * @param {string} industry
     * @param {string} category
     * @param {string} brand
     * @param {string} series
     * @param {string} model
     * @param {string} search
     * @param {number} page
     * @param {string} channel
     */
    const bodyParams = req.query

    const params = {
      userid: req.baseInfo.userid,
      industry: bodyParams.industry || '',
      category: bodyParams.category || '',
      brand: bodyParams.brand,
      series: bodyParams.series || '',
      model: bodyParams.model || '',
      search: bodyParams.search || '',
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      channel: bodyParams.channel || bodyParams.channelids || ''
    }
    if (bodyParams.rpstatus === 0) params.rpstatus = '0'

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  dashboard,
  filter,
  paging
}
