/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../components'

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }
  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.item.brandfilter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
  }
  return service.view.brand.dateFormat(paramDate)
}

const dashboard = async (params) => {
  /**
   * 检查参数category，若无category，目前临时置为1
   * 则检查brand，若为0，则查询30以外的值
   */
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    brand: params.brand
  }

  const productCount = await service.item.brandchart.dashboardCount(query)
  const productTotal = await service.item.brandchart.dashboardTotal(query)
  const productRate = (productCount.FakeProductCount * 100 / productTotal.FakeProductCount).toFixed(2) + '%'
  const channelDocs =  await service.item.brandchart.dashboardChannel(query)
  const fakeDocs = []
  channelDocs.forEach(element => {
    if (element["FakeProductStatusByChannel_DiscriminantResult"] === 0) {
      const fakedItem = {
        chanelId: element["FakeProductStatusByChannel_ChanelId"],
        chanelName: element["FakeProductStatusByChannel_ChanelName"],
        fakeCount: element["FakeProductStatusByChannel_ProductCount"]
      }
      fakedItem.fakeRate = element["FakeProductStatusByChannel_ProductCount"] / productCount.FakeProductCount
      fakeDocs.push(fakedItem)
    }
  })

  const data = [
    [productCount],
    [{
      name: 'FakeProductRate',
      FakeProductRate: productRate
    }],
    await service.item.brandchart.dashboardDaily(query),
    channelDocs,fakeDocs
  ]

  return data
}

const situation = async (params) => {
  /**
   * 检查参数category，若无category，目前临时置为1
   * 则检查brand，若为0，则查询30以外的值
   */
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)
  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category
  }

  const productTotal = await service.item.brandchart.dashboardTotal(query)
  const topBrand = await service.view.brand.topbrand(query)
  const topInfo = await service.item.brandchart.dashboardTop(query)
  const brandInfo = await service.item.brandchart.dashboardBrand(query)

  const cbDocs = []
  const topData = []
  const topDocs = new Set()

  if (topInfo.length) {
    topInfo.forEach(element => {
      const infoDoc = topBrand.find(t => Number.parseInt(t.id) === Number.parseInt(element.topId))
      topDocs.add(infoDoc.id)
      if(topData.length < 11) topData.push({
        name: 'FakeProductStatusByBrand',
        FakeProductStatusByBrand_BrandId: infoDoc.id,
        FakeProductStatusByBrand_BrandName: infoDoc.name,
        FakeProductStatusByBrand_ProductCount: element.brandCount,
        FakeProductStatusByBrand_FakeCount: element.fakenum,
        FakeProductStatusByBrand_CertCount: element.brandCount - element.fakenum,
        FakeProductStatusByBrand_ProductRate: element.brandCount * 100 / productTotal.FakeProductCount,
        FakeProductStatusByBrand_FakeRate: element.fakenum * 100 / productTotal.FakeProductCount,
        FakeProductStatusByBrand_CertRate: (element.brandCount - element.fakenum)* 100 / productTotal.FakeProductCount
      })
    })
  }

  topBrand.forEach(element => {
    const infoDoc = brandInfo.find(t => Number.parseInt(t.topId) === Number.parseInt(element.id))
    if (infoDoc) {
      cbDocs.push({
        name: 'FakeProductStatusByBrand',
        FakeProductStatusByBrand_BrandId: element.id,
        FakeProductStatusByBrand_BrandName: element.name,
        FakeProductStatusByBrand_ProductCount: infoDoc.brandCount,
        FakeProductStatusByBrand_ProductRate: infoDoc.brandCount * 100 / productTotal.FakeProductCount
      })
    } else {
      cbDocs.push({
        name: 'FakeProductStatusByBrand',
        FakeProductStatusByBrand_BrandId: element.id,
        FakeProductStatusByBrand_BrandName: element.name,
        FakeProductStatusByBrand_ProductCount: 0,
        FakeProductStatusByBrand_ProductRate: 0
      })
    }

    if (topData.length < 11 && !topDocs.has(element.id)) {

      topData.push({
        name: 'FakeProductStatusByBrand',
        FakeProductStatusByBrand_BrandId: element.id,
        FakeProductStatusByBrand_BrandName: element.name,
        FakeProductStatusByBrand_ProductCount: 0,
        FakeProductStatusByBrand_FakeCount: 0,
        FakeProductStatusByBrand_CertCount: 0,
        FakeProductStatusByBrand_ProductRate: 0,
        FakeProductStatusByBrand_FakeRate: 0,
        FakeProductStatusByBrand_CertRate: 0
      })
      topDocs.add(element.id)
    }
  }) 

  return {
    topTen: topData,
    allBrand: cbDocs
  }
}

const filter = async (params) => {
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    rpstatus: params.rpstatus,
    channel: params.channel,
    search: params.search,
    result: params.result,
    level: Number.parseInt(params.level) || 0
  }

  query.brand = params.brand

  /*
  const checkBrand = await service.view.brand.checkBrand(params)
  if (checkBrand.brand) query.brand = checkBrand.brand
  else if (checkBrand.ext) query.ext = checkBrand.ext
  */

  const defaultLevel = {
    type: 1,
    memberid: 0
  }

  if (query.level) {
    defaultLevel.id = params.level
    const levelScore = await service.information.confidence.getByID(defaultLevel)
    if (levelScore.length) {
      query.minscore = levelScore[0].minScore
      query.maxscore = levelScore[0].maxScore
    } else query.level = 0
  }

  const defaultStatus = await service.feedback.list.getStatusList()
  const statusList = await service.item.brandfilter.filterStatus(query)
  const formatList = []
  defaultStatus.forEach(rpStatus => {
    const defaultDoc = {
      Name: 'CountByRightsProtectionStatus',
      RightsProtectionStatus: rpStatus.index,
      'count(0)': 0
    }
    const doc = statusList.find(t => Number.parseInt(t.RightsProtectionStatus) === Number.parseInt(defaultDoc.RightsProtectionStatus))
    if (doc) formatList.push(doc)
    else formatList.push(defaultDoc)
  })

  const confidenceData = await service.information.confidence.getByType(defaultLevel)
  const pageConfidence = []
  confidenceData.forEach(confidenceDoc => {
    pageConfidence.push({
      Name: 'ConfidenceLevelList',
      ConfidenceLevelBucket: confidenceDoc.bucket,
      ConfidenceLevelBucketName: confidenceDoc.bucketName
    })
  })

  const data = [
    formatList,
    await service.item.brandfilter.filterChannel(query),
    [{
      Name: 'DiscriminantResultList',
      DiscriminantResult: 1
    }, {
      Name: 'DiscriminantResultList',
      DiscriminantResult: 0
    }],
    pageConfidence
  ]
  return data
}

const paging = async (params) => {
  const queryCheck = {
    start: params.start,
    end: params.end,
    category: Number.parseInt(params.category) || 1
  }

  const querDate = await checkdate(queryCheck)

  const query = {
    start: querDate.start,
    end: querDate.end,
    category: queryCheck.category,
    rpstatus: params.rpstatus,
    channel: params.channel,
    search: params.channel,
    result: params.result,
    level: Number.parseInt(params.level) || 0,
    order: Number.parseInt(params.order) || 0,
    skip: (params.page - 1) * params.size,
    size: params.size
  }

  query.brand = params.brand

  /*
  const checkBrand = await service.view.brand.checkBrand(params.brand)
  if (checkBrand.brand) query.brand = checkBrand.brand
  else if (checkBrand.ext) query.ext = checkBrand.ext
  */
  // 获取所有Score
  const defaultLevel = {
    type: 1,
    memberid: 0
  }
  const levelList = await service.information.confidence.getByType(defaultLevel)
  if (query.level) {
    const levelScore = levelList.filter(t => Number.parseInt(t.bucket) === Number.parseInt(query.level))
    if (levelScore) {
      query.minscore = levelScore[0].minScore
      query.maxscore = levelScore[0].maxScore
    } else query.level = 0
  }
  // 还要获取所有状态
  const defaultStatus = await service.feedback.list.getStatusList()
  const pageList = await service.item.brandlist.pageList(query)
  pageList.forEach(pageData => {
    if (pageData.Score === 0) pageData.Score += 0.01
    const scoreInfo = levelList.filter(t => Math.abs(pageData.Score) > t.minScore && pageData.Score <= t.maxScore)
    const statusInfo = defaultStatus.filter(t => Number.parseInt(t.index) === Number.parseInt(pageData.RightsProtectionStatus))
    pageData.ConfidenceLevelBucket = scoreInfo.bucket
    pageData.ConfidenceLevelBucketName = scoreInfo.bucketName
    pageData.Feedback = 0
    pageData.RightsProtectionStatusContent = statusInfo.content
  })

  const data = [
    await service.item.brandlist.pageCount(query),
    pageList
  ]
  return data
}

export default {
  dashboard,
  situation,
  filter,
  paging
}
