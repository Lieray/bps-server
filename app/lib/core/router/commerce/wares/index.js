/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/commerce/wares/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/commerce/wares/situation', auth.baseInfo, controller.situation)
router.get(baseUri + '/commerce/wares/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/commerce/wares/paging', auth.baseInfo, controller.paging)

export default router
