/**
 * 配置可用模块
 */
import landing from './landing'
import user from './user'
import commerce from './commerce'
import digital from './digital'
import price from './price'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

const routers = Object.assign(landing, user, commerce, digital,price)

export default routers
