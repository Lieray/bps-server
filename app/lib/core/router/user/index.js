/**
 * 配置可用模块
 */
import info from './info'
import quota from './quota'
import ship from './ship'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  userinfo: info,
  quota,
  ship
}
