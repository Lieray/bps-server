'use strict'
import service from '../../../libraries'

const updateInfo = async (docs) => {
  const user = await service.information.account.checkEmail(docs.email)
  docs.userid = user.userid
  // 直接更新手机状态
  if (docs.phone) {
    docs.type = 2
    const checkedStatus = await service.verify.phone.useSms(docs)
    if (checkedStatus.length && checkedStatus[0]) service.information.baseinfo.updatePhone({ userid: user.userid, phone: docs.phone })
  } else {
    const userInfo = await service.information.baseinfo.updateInfo(docs)
    return userInfo
  }
}

const baseInfo = async (email) => {
  const user = await service.information.account.checkEmail(email)
  const userInfo = await service.information.baseinfo.getByID(user.userid)
  return userInfo
}

const setsms = async (docs) => {
  // 如果验证码已达上限，则返回错误信息
  docs.type = 2
  const smsInfos = await service.verify.phone.getByBoth(docs)
  if (smsInfos) throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  // 生成待激活记录
  const genToken = await service.verify.phone.createSMS(docs)
  // 发送短信
  service.verify.phone.sendSms({
    phone: docs.phone,
    msg: `您的手机验证码为：${genToken.token}，有效期15分钟。如非本人操作，请忽略本短信！`
  })
  return genToken
}

/**
 * 后端发送验证码15分钟内有效
 * @param {string} email
 * @param {string} phone
 * @param {string} msg
 */
const verifysms = async (docs) => {
  // 检查是否存在符合条件的验证码
  docs.type = 2
  const smsInfo = await service.verify.phone.checkSms(docs)
  // 激活验证码
  if (smsInfo) {
    if (smsInfo.status === 'unactivated') {
      await service.verify.phone.putSms(docs)
      return docs
    } else if (smsInfo.status === 'activated') return docs
    else throw new Error(`Error happens with the status code: 403, the key is: ${docs.token}`)
  } else throw new Error(`Error happens with the status code: 403, the key is: ${docs.token}`)
}

export default {
  updateInfo,
  baseInfo,
  setsms,
  verifysms
}
