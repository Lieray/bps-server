/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/users/info', auth.baseInfo, controller.baseInfo)
router.get(baseUri + '/users/sms', auth.baseInfo, controller.setsms)
router.put(baseUri + '/users/sms', auth.baseInfo, controller.verifysms)
router.put(baseUri + '/users/update', auth.baseInfo, controller.updateInfo)

export default router
