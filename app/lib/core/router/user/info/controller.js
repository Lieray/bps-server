/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
import service from './assembly'

const Validater = {
  smsid: (value) => {
    if (!(/^\d{6}$/g.test(value))) return false
    return true
  },
  telnum: (value) => {
    if (!(/^1[3456789]\d{9}$|^\+861[3456789]\d{9}$|^861[3456789]\d{9}/g.test(value))) return false
    return true
  }
}

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (Validater[key]) {
        if (Validater[key](target[key])) return target[key]
        else throw new Error(`Error happens with the status code: 400, the key is: ${key}`)
      }
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const updateInfo = async (req, res) => {
  try {
    /**
     * put方法，读取req.body中的参数
     * @param {string} username
     * @param {string} company
     * @param {string} telnum
     */
    const bodyParams = req.body
    const params = ((
      {
        username,
        company,
        telnum
      }) => (
      {
        email: req.baseInfo.email,
        username,
        company,
        phone: telnum
      }
    ))(bodyParams)

    await service.updateInfo(params)

    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const baseInfo = async (req, res) => {
  try {
    const userInfo = await service.baseInfo(req.baseInfo.email)
    userInfo.email = req.baseInfo.email

    return res.json({
      success: true,
      message: 'OK',
      data: userInfo
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setsms = async (req, res) => {
  try {
    /**
     * get方法，读取req.query中的参数
     * @param {string} telnum
     */
    const bodyParams = req.query
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ telnum }) => ({ phone: telnum, email: req.baseInfo.email }))(bodyParse)
    const serviceResult = await service.setsms(params)
    return res.json({
      success: true,
      message: 'OK',
      data: serviceResult
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const verifysms = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} telnum
     * @param {string} smsid
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ telnum, smsid }) => ({ email: req.baseInfo.email, phone: telnum, token: smsid }))(bodyParse)
    await service.verifysms(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  updateInfo,
  baseInfo,
  setsms,
  verifysms
}
