/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
import service from './assembly'

const getlist = async (req, res) => {
  try {
    const msg = await service.getlist({ userid: req.baseInfo.userid, mainid: req.baseInfo.mainid })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const sendmsg = async (req, res) => {
  try {
    const query = {
      project: req.query.id,
      userid: req.baseInfo.userid, 
      mainid: req.baseInfo.mainid, 
      email: req.baseInfo.email 
    }
    const msg = await service.sendmsg(query)
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getCount = async (req, res) => {
  try {
    const msg = await service.filter({ 
      userid: req.baseInfo.userid, 
      mainid: req.baseInfo.mainid 
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getmsg = async (req, res) => {
  try {
    const msg = await service.paging({ 
      page: Number.parseInt(req.query.page) || 1,
      size: Number.parseInt(req.query.size) || 10,
      type: Number.parseInt(req.query.type) || 0,
      userid: req.baseInfo.userid, 
      mainid: req.baseInfo.mainid 
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setmsg = async (req, res) => {
  try {
    const msg = await service.setmsg({ 
      mainid: req.baseInfo.mainid,
      content: '`status`',
      id: req.body.id
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const delmsg = async (req, res) => {
  try {
    const msg = await service.setmsg({ 
      mainid: req.baseInfo.mainid,
      content: '`isdel`',
      id: req.body.id
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getview = async (req, res) => {
  try {
    const msg = await service.getview({ 
      mainid: req.baseInfo.mainid,
      userid: req.baseInfo.userid
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setview = async (req, res) => {
  try {
    const msg = await service.setview({ 
      mainid: req.baseInfo.mainid,
      userid: req.baseInfo.userid,
      notifier: JSON.stringify(req.body.notifier)
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const setstatus = async (req, res) => {
  try {
    const msg = await service.setstatus({ 
      mainid: req.baseInfo.mainid,
      userid: req.baseInfo.userid,
      status: Number.parseInt(req.body.start) || 0
    })
    return res.json(msg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getlist,
  sendmsg,
  getmsg,
  setmsg,
  delmsg,
  getCount,
  getview,
  setview,
  setstatus
}
