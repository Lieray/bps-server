/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/package/list', auth.baseInfo, controller.getlist)
router.get(baseUri + '/package/upgrade', auth.baseInfo, controller.sendmsg)
router.get(baseUri + '/notification/count', auth.baseInfo, controller.getCount)
router.get(baseUri + '/notification/list', auth.baseInfo, controller.getmsg)
router.put(baseUri + '/notification/read', auth.baseInfo, controller.setmsg)
router.put(baseUri + '/notification/del', auth.baseInfo, controller.delmsg)
router.get(baseUri + '/notification/view', auth.baseInfo, controller.getview)
router.put(baseUri + '/notification/set', auth.baseInfo, controller.setview)
router.put(baseUri + '/notification/status', auth.baseInfo, controller.setstatus)

export default router
