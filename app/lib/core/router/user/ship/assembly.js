/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import service from '../../../libraries'

const getlist = async (docs) => {
  const defaultLevel = {
    balance: 0,
    cost: 0,
    package: []
  }
  if (docs.mainid) {
    const projectList = await service.subscribe.info.getList({mainId: docs.mainid})
    projectList.forEach(muneset => {
      defaultLevel.package.push(muneset)
    })
  }
  return defaultLevel
}

const sendmsg = async (docs) => {
  const defaultMsg = {
    level: '会员',
    demand: '升级'
  }
  if (!docs.project) {
    defaultMsg.level = '用户',
    defaultMsg.demand = '开通'
  }

  // 如果已有待查收的申请邮件，则直接返回成功
  const activationEmails = await service.verify.email.getByEmail({ email: docs.email, type: 0 })
  if (activationEmails.length) return { success: true, message: 'Pending' }

  const accountInfo = await service.information.account.checkEmail(docs.email)

  const userInfo = await service.information.baseinfo.getByID(accountInfo.infoid)
  const msgData = {
    email: docs.email,
    name: userInfo.name,
    phone: userInfo.phone,
    company: userInfo.company
  }

  // 生成申请记录
  await service.verify.email.createMsg({ email: docs.email, type: 0 })

  // 异步发送邮件
  service.verify.email.sendMsg(Object.assign(msgData, defaultMsg))

  return { success: true, message: 'OK' }
}

const paging = async (params) => {

  const query = {
    mainid: params.mainid,
    type: params.type,
    skip: (params.page - 1) * params.size,
    size: params.size
  }

  const pageList = await service.information.notify.pageList(query)
  const data = [
    await service.information.notify.pageCount(query),
    pageList
  ]
  return data
}

const setmsg = async (params) => {
  await service.information.notify.itemSet(params)
  return { success: true, message: 'OK' }
}

const filter = async (params) => {
    const totalItem = await service.information.notify.pageFilter(params)
    const formatList = [
      0,0,0
    ]
    totalItem.forEach(element => {
      formatList[0] += element.Count
      formatList[element.type] += element.Count
    })

    return formatList
}

const getview = async (params) => {
  const cbdata = await service.subscribe.notify.checkemail(params)
  return { success: true, result: cbdata }
}

const setview = async (params) => {
  await service.subscribe.notify.setemail(params)
  return { success: true, message: 'OK' }
}

const setstatus = async (params) => {
  await service.subscribe.notify.setstatus(params)
  return { success: true, message: 'OK' }
}


export default {
  getlist,
  sendmsg,
  filter,
  paging,
  setmsg,
  getview,
  setview,
  setstatus
}
