'use strict'
import service from '../../../libraries'
/**
 * 扣除点数时，分开计算VIP身份，查询时汇总为一个点数内容
 */
const getQuota = async (docs) => {
  const defaultLevel = {
    balance: 0,
    cost: 0
  }
  // 有主账号才可能是会员，非会员不绑定主账号
  // if (docs.mainid) {
  //   // 获取用户余额
  //   const memberQuota = await service.view.balance.getQuota(docs.mainid)
  //   defaultLevel.balance = memberQuota.balance || 0
  //   defaultLevel.cost = memberQuota.cost || 0
  // }
  return defaultLevel
}

const getkf = async (docs) => {
  const cbData = {
    username: docs.email.toLowerCase(),
    time: Number.parseInt(new Date().getTime()/1000)
  }
  cbData.token = await service.identify.commom.genKFToken(cbData)
  return cbData
}

export default {
  getQuota,
  getkf
}
