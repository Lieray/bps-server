/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/users/quota', auth.baseInfo, controller.getQuota)
router.get(baseUri + '/users/kftoken', auth.baseInfo, controller.getkf)

export default router
