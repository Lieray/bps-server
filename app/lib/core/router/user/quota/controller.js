/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
import service from './assembly'

const getQuota = async (req, res) => {
  try {
    const balance = await service.getQuota({ mainid: req.baseInfo.mainid })
    return res.json(balance)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const getkf = async (req, res) => {
  try {
    const info = await service.getkf({ email: req.baseInfo.email })
    return res.json(info)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getQuota,
  getkf
}
