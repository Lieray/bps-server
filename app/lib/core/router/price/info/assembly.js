/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../components'

const checkOrder = (orderType) => {
  const orderList = [
    "series","type","model","updateAt"
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[0] + allTypes[0]
  if(orderType) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const project = async (params) => {
  const data = await service.package.content.getProject(params)
  if (data.length) {
    const docs = []
    data.forEach(element => {
      docs.push(element.id)
    })
    const exid = []
    const exproject = await service.package.project.getProject({id: docs.toString()})
    exproject.forEach(element => {
      exid.push(element.content)
    })
    const exInfo = await service.monitor.info.getItemConfig({id: exid.toString()})
    return {
      success: true,
      list: exInfo
    }
  } else return {
    success: true,
    list: []
  }
}

const filter = async (params) => {
  const data = [
    await service.monitor.filter.filterChannel(params),
    await service.monitor.filter.filterSeries(params),
    await service.monitor.filter.filterMode(params)
  ]
  return data
}

const paging = async (params) => {
  const query = {
    project: params.project,
    skip: (params.page - 1) * params.size,
    channel: params.channel,
    series: params.series,
    model: params.model,
    search: params.search,
    order: checkOrder(params.order),
    size: params.size
  }
  const pageList = await service.monitor.list.pageList(query)

  const data = [
    await service.monitor.list.pageCount(query),
    pageList
  ]
  return data
}

const pageInfo = async (params) => {
  const data = await service.monitor.info.pageInfo(params)
  return data
}

const detailist = async (params) => {
  const query = {
    sku: params.sku,
    channel: params.channel,
    search: params.search,
    authorize: params.authorize,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const pageList = await service.monitor.info.getItemList(query)

  pageList.forEach(element => {
    element.monitorId = element.MonitorId
  })

  const data = [
    await service.monitor.info.getItemCount(query),
    pageList
  ]

  return data
}

const detailInfo = async (params) => {
  const data = await service.monitor.info.getItemInfo(params)
  return data
}

const priceInfo = async (params) => {
  const data = []
  return data
}

export default {
  project,
  filter,
  paging,
  pageInfo,
  detailist,
  detailInfo,
  priceInfo
}
