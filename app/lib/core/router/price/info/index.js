/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/price/monitor/config', auth.baseInfo, controller.project)
router.get(baseUri + '/price/config/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/price/config/brand', auth.baseInfo, controller.paging)
router.get(baseUri + '/price/config/commerce', auth.baseInfo, controller.pageInfo)
router.get(baseUri + '/price/config/list', auth.baseInfo, controller.list)
router.get(baseUri + '/price/config/info', auth.baseInfo, controller.listinfo)
router.get(baseUri + '/price/config/price', auth.baseInfo, controller.detail)

export default router
