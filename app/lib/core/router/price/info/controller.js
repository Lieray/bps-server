/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const project = async (req, res) => {
  try {
    const bodyParams = req.query

    const params = {
      mainid: req.baseInfo.mainid,
      type: 3
    }

    const docs = await service.project(params)
    return res.json(docs)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.id,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {number} page
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.id,
      channel: bodyParams.channel || '',
      series: bodyParams.series || '',
      model: bodyParams.model || '',
      search: bodyParams.search || '',
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const pageInfo = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      sku: bodyParams.sku,
      channel: bodyParams.channel,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.pageInfo(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const list = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     * @param {number} page
     */
    const bodyParams = req.query

    const params = {
      sku: bodyParams.sku,
      channel: bodyParams.channel,
      search: bodyParams.search || '',
      authorize: Number.parseInt(bodyParams.authorize) === 0 ? "0": bodyParams.authorize,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.detailist(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const listinfo = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {number} id
     */
    const bodyParams = req.query
    const params = {
      id: bodyParams.id,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.detailInfo(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const detail = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {number} id
     */
    const bodyParams = req.query

    const params = {
      id: bodyParams.id,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.priceInfo(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  paging,
  filter,
  project,
  pageInfo,
  list,
  listinfo,
  detail
}
