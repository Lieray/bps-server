/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/price/monitor/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/price/monitor/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/price/monitor/paging', auth.baseInfo, controller.paging)
router.get(baseUri + '/price/monitor/list', auth.baseInfo, controller.getlist)
router.get(baseUri + '/price/monitor/project', auth.baseInfo, controller.getconf)
router.get(baseUri + '/price/monitor/charts', auth.baseInfo, controller.charts)

export default router
