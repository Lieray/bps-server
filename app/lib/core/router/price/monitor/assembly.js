/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.monitor.filter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const checkOrder = (orderType) => {
  const orderList = [
    "series","type","model","ISNULL(updateAt), updateAt",
    "ISNULL(Discount), Discount","ISNULL(LinkCount), LinkCount"
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[5] + allTypes[1]
  if(orderType) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const fakeOrder = (orderType) => {
  const orderList = [
    "a.MonitorId","a.CurrentPrice","a.FinalPrice","a.MonitorPrice",
    "a.MarginPrice","a.MarginPercent","d.TotalAmount"
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[0] + allTypes[0]
  if(orderType) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const dashboard = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)
    const query = {
      start: querDate.start,
      end: querDate.end,
      last: querDate.last,
      project: queryCheck.project
    }
    const productCount = await service.monitor.chart.dashboardCount(query)
    const productTotal = await service.monitor.chart.dashboardTotal(query)
    const productRate = (productCount.FakeProductCount * 100 / productTotal.FakeProductCount).toFixed(2) + '%'
    const channelData = await service.monitor.chart.dashboardChannel(query)
    channelData.forEach(element => {
      element.rate = element.Count/productCount.FakeProductCount
    })
    const data = [
      [productCount],
      [{
        name: 'FakeProductRate',
        FakeProductRate: productRate
      }],
      channelData,
      await service.monitor.chart.dashboardDaily(query),
      await service.monitor.filter.getUpdateTime(query)
    ]
    return data
  } else return []
}

const filter = async (params) => {
  const userProject = await checkProject(params)

  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      series: params.series,
      model: params.model,
      search: params.search
    }
    // const totalItem = await service.monitor.filter.totalItem(query)
    // const totalLink = await service.monitor.filter.totalLink(query)
    // const fakeLink = await service.monitor.filter.fakeLink(query)
    const formatList = [
      // totalItem[0].Count,
      // totalLink[0].Count,
      // fakeLink[0].Count,
      0,0,0,0
    ]
    const data = [
      formatList,
      await service.monitor.filter.filterChannel(query),
      await service.monitor.filter.filterSeries(query),
      await service.monitor.filter.filterMode(query)
    ]
    return data
  } else return []
}

const paging = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      series: params.series,
      model: params.model,
      order: checkOrder(params.order),
      search: params.search,
      skip: (params.page - 1) * params.size,
      size: params.size
    }
    const pageList = await service.monitor.list.pageList(query)

    const data = [
      await service.monitor.list.pageCount(query),
      pageList
    ]
    return data
  } else return []
}


const project = async (params) => {
  const imgInfo = await service.monitor.info.currentLink(params)
  return {
    success: true,
    project: data,
    screenshot: imgInfo.length > 0,
    init: initItem 
  }
}

const getlist = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      series: params.series,
      model: params.model,
      search: params.search,
      order: fakeOrder(params.order),
      skip: (params.page - 1) * params.size,
      size: params.size
    }
    const pageList = await service.monitor.list.fakeList(query)
    pageList.forEach(element => {
      const labarray = []
      if(element.hasct) labarray.push('明显假冒')
      if(element.hasbrd) labarray.push('滥用商标词')
      if(element.haspic) labarray.push('盗图')
      element.itemlab = labarray.toString()
    })

    const data = [
      await service.monitor.list.listCount(query),
      pageList
    ]
    return data
  } else return []
}

const charts = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)
    const query = {
      start: querDate.start,
      end: querDate.end,
      last: querDate.last,
      project: queryCheck.project,
      channel: params.channel,
      series: params.series,
      model: params.model,
      search: params.search
    }

    const productCount = await service.monitor.chart.chartCount(query)
    const productTotal = await service.monitor.chart.chartTotal(query)
    const productRate = (productCount.FakeProductCount * 100 / productTotal.FakeProductCount).toFixed(2) + '%'
    const channelData = await service.monitor.chart.chartChannel(query)
    channelData.forEach(element => {
      element.rate = element.Count/productCount.FakeProductCount
    })
    const data = [
      [productCount],
      [{
        name: 'FakeProductRate',
        FakeProductRate: productRate
      }],
      channelData,
      await service.monitor.chart.chartDaily(query),
      await service.monitor.filter.getUpdateTime(query)
    ]
    return data
  } else return []
}

export default {
  dashboard,
  filter,
  paging,
  getlist,
  charts
}
