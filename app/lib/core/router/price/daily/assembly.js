import service from '../../../libraries'
import { date } from 'joi'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  if (userProject.length) {
    const cbData = {
      project: userProject.toString(),
      subname: policy[0].name
    }

    // const defaultData = await service.monitor.daily.deateChecker()
    const defaultDate = await service.monitor.filter.getLastDate(cbData)
    cbData.start = params.start || defaultDate.start
    cbData.end = params.end || defaultDate.end
    return cbData
  } else throw new Error('Can not find the project!')
}


const checkdate = async (params) => {
  const defaultData = await service.monitor.daily.deateChecker()
  const defaultDocs = {
    project: params.project || defaultData.project,
    start: params.start || defaultData.start,
    end: params.end || defaultData.end
  }

  return defaultDocs
}

const tempIP = async () => {
  const data = await service.monitor.daily.serverIP()
  return data
}

const shopStatistics = async (params) => {
  const data = await service.monitor.daily.shopStatistics(params)
  return data
}

const itemStatistics = async (params) => {
  const data = await service.monitor.daily.itemStatistics(params)
  return data
}

const otherStatistics = async (params) => {
  const data = await service.monitor.daily.otherStatistics(params)
  return data
}

const shopTemp = async (params) => {
  const data = await service.price.daily.shopStatistics(params)
  return data
}

const itemTemp = async (params) => {
  const data = await service.price.daily.itemStatistics(params)
  return data
}

const checkTemp = async (params) => {
  const defaultData = await service.price.daily.deateChecker()
  const defaultDocs = {
    project: params.project || defaultData.project,
    start: params.start || defaultData.start,
    end: params.end || defaultData.end
  }

  return defaultDocs
}

const linkInfo = async (params) => {
  const data = await service.monitor.daily.linkInfo(params)
  return data
}

const baseData = async (params) => {
  const cdate = await service.monitor.filter.getUpdateTime(params)
  const lastDateTime = JSON.stringify(cdate[0].CurrentDate).split('T')
  const lastDate = lastDateTime[0].replace('"','').split('-')
  const lastTime = lastDateTime[1].split(':')
  return params.subname + lastDate[0]+ lastDate[1]+ lastDate[2]+ lastTime[0]+ lastTime[1]
}

export default {
  tempIP,
  checkdate,
  shopStatistics,
  itemStatistics,
  otherStatistics,
  shopTemp,
  itemTemp,
  checkTemp,
  checkProject,
  linkInfo,
  baseData
}