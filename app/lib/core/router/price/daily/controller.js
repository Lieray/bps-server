import service from './assembly'
import XLSX from 'xlsx'
import stream from 'stream'
const tempIP = async (_req, res) => {
  try {
    /**
     * get方法，读取req.headers中的Authorization
     * @param {string} Authorization
     */
    const docs = await service.tempIP()

    return res.json({
      success: true,
      results: docs
    })
  } catch (e) {
    return res.json("出错了")
  }
}

const shopStatistics = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end,
      mainid: req.baseInfo? req.baseInfo.mainid : NULL
    }
    const query = req.baseInfo? await service.checkProject(params) : await service.checkdate(params)
    const docs = await service.shopStatistics(query)
    const xlsxObj = [
      ['平台','店铺','是否授权','当天商品链接数','当天异常链接数','当天异常次数','当天最低折扣']
    ]
    docs.forEach(element => {
      xlsxObj.push([
        element['平台'],
        element['店铺'],
        element['是否授权'],
        element['当天商品链接数'],
        element['当天异常链接数'],
        element['当天异常次数'],
        element['当天最低折扣']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, query.start)
    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`店铺统计${query.start}.xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

const itemStatistics = async (req, res) => {
  console.log(1)
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end,
      mainid: req.baseInfo? req.baseInfo.mainid : 0
    }
    const query = req.baseInfo? await service.checkProject(params) : await service.checkdate(params)
    console.log(query)
    const docs = await service.itemStatistics(query)
    const xlsxObj = [
      ['编码','系列','产品名称','参数','产品链接','渠道','店铺名称','型号','授权','页面价格',
      '券后价格','检测价格','价差','价差百分比','最近低价时间','近期异常次数','近期异常天数']
    ]
    docs.forEach(element => {
      xlsxObj.push([
        element['编码'],
        element['系列'],
        element['产品名称'],
        element['参数'],
        element['产品链接'],
        element['渠道'],
        element['店铺名称'],
        element['型号'],
        element['授权'],
        element['页面价格'],
        element['券后价格'],
        element['检测价格'],
        element['价差'],
        element['价差百分比'],
        element['最近低价时间'],
        element['近期异常次数'],
        element['近期异常天数']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, '低价商品统计')

    const otherdocs = await service.otherStatistics(query)
    const otherxlsxObj = [
      ['编码','系列','产品名称','参数','产品链接','渠道','店铺名称','型号','授权',
      '最近页面价格','最近券后价格','检测价格','价差','价差百分比','最近监测时间']
    ]
    otherdocs.forEach(element => {
      otherxlsxObj.push([
        element['编码'],
        element['系列'],
        element['产品名称'],
        element['参数'],
        element['产品链接'],
        element['渠道'],
        element['店铺名称'],
        element['型号'],
        element['授权'],
        element['页面价格'],
        element['券后价格'],
        element['检测价格'],
        element['价差'],
        element['价差百分比'],
        element['最近低价时间']
      ])
    })
    const othersheet = XLSX.utils.aoa_to_sheet(otherxlsxObj)
    XLSX.utils.book_append_sheet(book, othersheet, '一般商品统计')

    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`商品统计(${query.start}-${query.end}).xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

const shopTemp = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end
    }
    const query = await service.checkTemp(params)
    const docs = await service.shopTemp(query)
    const xlsxObj = [
      ['平台','店铺','是否授权','当天商品链接数','当天异常链接数','当天异常次数','当天最低折扣']
    ]
    docs.forEach(element => {
      xlsxObj.push([
        element['平台'],
        element['店铺'],
        element['是否授权'],
        element['当天商品链接数'],
        element['当天异常链接数'],
        element['当天异常次数'],
        element['当天最低折扣']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, query.start)
    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`店铺统计${query.start}.xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

const itemTemp = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end
    }
    const query = await service.checkTemp(params)
  
    const docs = await service.itemTemp(query)
    const xlsxObj = [
      ['AssetID','系列/Series','产品名称/Product Name','参数/Spec','产品链接/Product_URL','渠道/Platform',
      '店铺名称/StoreName','型号/Model','授权/Authorization','页面价格/PagePrice',
      '券后价格/DiscountedPrice','检测价格/SetPrice','价差/PriceDiff','折扣率/Discount%', '降价率/%Off',
      '最近低价时间/Last Price Break Time','近期异常次数/Issue times','近期异常天数/Issue days']
      ]
    docs.forEach(element => {
      xlsxObj.push([
        element['编码'],
        element['系列'],
        element['产品名称'],
        element['参数'],
        element['产品链接'],
        element['渠道'],
        element['店铺名称'],
        element['型号'],
        element['授权'],
        element['页面价格'],
        element['券后价格'],
        element['检测价格'],
        element['价差'],
        Math.ceil(element['券后价格']/element['检测价格']*100) + '%',
        Math.ceil(-element['价差']/element['检测价格']*100) + '%',
        element['最近低价时间'],
        element['近期异常次数'],
        element['近期异常天数']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, query.start)
    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`商品统计${query.start}.xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

const linkStatistics = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid
    }
    const docs = await service.linkInfo(params)
    const xlsxObj = [
      ['项目编号','检测序列','商品名称','系列','型号','参数','链接','渠道','卖家名称','卖家链接','是否授权'
      ,'页面价格','券后价格','检测价格','差价','折扣','最近更新时间','30天内违规次数','30天内违规天数']
    ]
    docs.forEach(element => {
      xlsxObj.push([
        element['项目编号'],
        element['检测序列'],
        element['商品名称'],
        element['系列'],
        element['型号'],
        element['参数'],
        element['链接'],
        element['渠道'],
        element['卖家名称'],
        element['卖家链接'],
        element['是否授权'],
        element['页面价格'],
        element['券后价格'],
        element['检测价格'],
        element['差价'],
        element['折扣'],
        element['最近更新时间'],
        element['30天内违规次数'],
        element['30天内违规天数']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, '链接统计')
    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`链接统计.xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

const itemInfo = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {string} channel
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project || bodyParams.subid || 91,
      start: bodyParams.start,
      end: bodyParams.end,
      mainid: req.baseInfo? req.baseInfo.mainid : 6
    }
    const query = await service.checkProject(params)
    const fileCurrentName = await service.baseData(query)
    const docs = await service.itemStatistics(query)
    const xlsxObj = [
      ['AssetID','系列/Series','产品名称/Product Name','参数/Spec','产品链接/Product_URL','渠道/Platform',
      '店铺名称/StoreName','型号/Model','授权/Authorization','页面价格/PagePrice',
      '券后价格/DiscountedPrice','检测价格/SetPrice','价差/PriceDiff','折扣率/Discount%', '降价率/%Off',
      '最近低价时间/Last Price Break Time','近期异常次数/Issue times','近期异常天数/Issue days']
      ]
    docs.forEach(element => {
      xlsxObj.push([
        element['编码'],
        element['系列'],
        element['产品名称'],
        element['参数'],
        element['产品链接'],
        element['渠道'],
        element['店铺名称'],
        element['型号'],
        element['授权'],
        element['页面价格'],
        element['券后价格'],
        element['检测价格'],
        element['价差'],
        Math.ceil(element['券后价格']/element['检测价格']*100) + '%',
        (100 - Math.ceil(element['券后价格']/element['检测价格']*100))  + '%',
        element['最近低价时间'],
        element['近期异常次数'],
        element['近期异常天数']
      ])
    })

    const book = XLSX.utils.book_new()
    const sheet = XLSX.utils.aoa_to_sheet(xlsxObj)
    XLSX.utils.book_append_sheet(book, sheet, '商品统计')
    const fileContents = XLSX.write(book, { type: 'buffer', bookType: 'xlsx', bookSST: false })
    const readStream = new stream.PassThrough()
    readStream.end(fileContents)
 
    const fileName = encodeURIComponent(`${fileCurrentName}.xlsx`)
    res.set('Content-disposition', 'attachment; filename=' + fileName)
    res.set('Content-Type', 'text/plain;charset=utf-8')
    readStream.pipe(res)
  } catch (e) {
    return res.json("出错了")
  }
}

export default {
  tempIP,
  shopStatistics,
  itemStatistics,
  shopTemp,
  itemTemp,
  linkStatistics,
  itemInfo
}
