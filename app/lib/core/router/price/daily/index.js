/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V0'
const tempUri = '/V1'
const currentUri = '/V3'


const router = express.Router()

router.get(baseUri + '/daily/statistics/ip', controller.tempIP)
router.get(baseUri + '/daily/statistics/shop', controller.shopStatistics)
router.get(baseUri + '/daily/statistics/item', controller.itemStatistics)
// router.get(tempUri + '/daily/statistics/shop', controller.shopTemp)
// router.get(tempUri + '/daily/statistics/item', controller.itemTemp)
router.get(tempUri + '/daily/statistics/link', controller.linkStatistics)
router.get(currentUri + '/price/statistics/item', controller.itemInfo)

export default router
