/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const checkOrder = (orderType) => {
  const orderList = [
    "a.MonitorId","a.CurrentPrice","a.FinalPrice","a.MonitorPrice",
    "a.MarginPrice","a.MarginPercent","a.statisticsCount","a.statisticsDay",
    "d.TotalAmount"
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[0] + allTypes[0]
  if(orderType) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.monitor.info.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const filter = async (params) => {
  const cbData = [0,0,0,0,0,0]
  const totalLink = await service.monitor.info.getItemFilter(params)

  totalLink.forEach(element => {
    cbData[0] += element.Count
    if(Number.parseInt(element.Status) > 0 ) cbData[Number.parseInt(element.Status)] += element.Count
  })
  return [
    cbData,
    await service.monitor.info.filterChannel(params)
  ]
}

const paging = async (params) => {
  const query = {
    skuid: params.skuid,
    search: params.search,
    channel: params.channel,
    authorize: params.authorize,
    order: checkOrder(params.order),
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  
  const dataid = []

  if (Number.parseInt(params.status) >0) query.status = params.status
  const cbData = [0,0,0,0,0,0]
  const totalLink = await service.monitor.info.getItemFilter(params)
  totalLink.forEach(element => {
    cbData[0] += element.Count
    if(Number.parseInt(element.Status) > 0 ) cbData[Number.parseInt(element.Status)] += element.Count
  })
  const datalist = await service.monitor.info.getItemList(query)
  datalist.forEach(element => {
    const labarray = []
    if(element.hasct) labarray.push('明显假冒')
    if(element.hasbrd) labarray.push('滥用商标词')
    if(element.haspic) labarray.push('盗图')
    element.itemlab = labarray.toString()
    dataid.push(element.MonitorId)
  })
  return [
    [{Count: params.status? cbData[Number.parseInt(params.status)] : cbData[0]}],
    datalist
  ]
}

const getDaily = async (params) => {
  // const querDate = await checkdate(params)
  // const query = {
  //   start: querDate.start,
  //   end: querDate.end,
  //   last: querDate.last,
  //   skuid: params.skuid
  // }

  const data = await service.monitor.info.getStatusLevel(params)
  return data
}

const dashboard = async (params) => {
  const itemInfo = await service.monitor.info.getItemInfo(params)
  const detailDocs = await service.monitor.info.currentList(params)
  const linkDocs = await service.monitor.info.currentLink(params)
  const linkNum = linkDocs.length
  detailDocs.forEach((currentValue, index) => {
    currentValue.ProductName = itemInfo[0].ProductName
    if (linkNum)
    currentValue.snapshot = linkNum < index ? linkDocs[linkNum-1]: linkDocs[index]
  })
  return {
    info: itemInfo,
    result:[
      await service.monitor.info.currentDetail(params),
      await service.monitor.info.weekDetail(params),
      detailDocs
    ]
  }
}

const complain = async (params) => {
  const userBrand = await checkProject(params)
  if (userBrand.length) {
    const resultid = params.resultid.toString().split(',')
    // 首先应确认这些条目属于该用户
    const userDocs = await service.order.submit.getByBoth({ project: userBrand[0], id: params.resultid })
    // 生成操作记录
    const queryDocs = []
    // 生成工单内容
    userDocs.forEach(iteminfo => {
      queryDocs.push({
        project: userBrand[0],
        mainid: params.mainid,
        userid: params.userid,
        id: iteminfo.MonitorId,
        content: params.content || '',
        email: params.email,
        title: '低价链接投诉',
        comment: `信息：${iteminfo.ProductName}，链接：${iteminfo.URL}`
      })
    })
    // 提交工单
    await service.order.submit.sendOrder(queryDocs)

    return params
  } else return []
}

export default {
  paging,
  getDaily,
  filter,
  dashboard,
  complain
}
