/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/price/statistics/daily', auth.baseInfo, controller.recently)
router.get(baseUri + '/price/statistics/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/price/statistics/paging', auth.baseInfo, controller.paging)
router.get(baseUri + '/price/statistics/dashboard', auth.baseInfo, controller.dashboard)
router.put(baseUri + '/price/statistics/complain', auth.baseInfo, controller.complain)

export default router
