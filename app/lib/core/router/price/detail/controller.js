/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const recently = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      skuid: bodyParams.skuid,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.getDaily(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      skuid: bodyParams.skuid,
      channel: bodyParams.channel,
      mainid: req.baseInfo.mainid,
      search: bodyParams.search || '',
      authorize: Number.parseInt(bodyParams.authorize) === 0 ? "0": bodyParams.authorize
    }

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     * @param {number} page
     */
    const bodyParams = req.query

    const params = {
      skuid: bodyParams.skuid,
      page: Number.parseInt(bodyParams.page) || 1,
      status: Number.parseInt(bodyParams.rpstatus) === 0 ? "0": bodyParams.rpstatus,
      search: bodyParams.search || '',
      channel: bodyParams.channel,
      order: bodyParams.order,
      authorize: Number.parseInt(bodyParams.authorize) === 0 ? "0": bodyParams.authorize,
      size: Number.parseInt(bodyParams.size) || 10,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {number} id
     */
    const bodyParams = req.query
    const params = {
      id: bodyParams.assetid,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboard(params)
    return res.json(Object.assign({
      success: true,
      query: req.query
    },docs))
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const complain = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} project
     * @param {string} id
     * @param {string} feedbackContent
     */
    const bodyParams = req.body
    const params = {
      project: bodyParams.project || bodyParams.subid,
      resultid: bodyParams.id,
      userid: req.baseInfo.userid,
      mainid: req.baseInfo.mainid,
      email: req.baseInfo.email,
      content: bodyParams.feedbackContent
    }
    await service.complain(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  paging,
  recently,
  filter,
  dashboard,
  complain
}
