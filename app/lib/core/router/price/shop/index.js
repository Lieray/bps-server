/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/price/shop/dashboard', auth.baseInfo, controller.dashboard)
router.get(baseUri + '/price/shop/filter', auth.baseInfo, controller.filter)
router.get(baseUri + '/price/shop/paging', auth.baseInfo, controller.paging)
router.get(baseUri + '/price/shop/list', auth.baseInfo, controller.list)

export default router
