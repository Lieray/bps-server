/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const checkdate = async (params) => {
  const paramDate = {
    start: new Date(params.start),
    end: new Date(params.end)
  }

  if (paramDate.start.getTime() && paramDate.end.getTime()) {
    if (paramDate.start > paramDate.end) {
      paramDate.start = new Date(params.end)
      paramDate.end = new Date(params.start)
    }
  } else {
    const defaultDate = await service.monitor.filter.getLastDate(params)
    paramDate.start = defaultDate.start
    paramDate.end = defaultDate.end
    paramDate.last = defaultDate.last
  }
  return service.identify.commom.dateCheck(paramDate)
}

const checkOrder = (orderType) => {
  const orderList = [
    "a.InfoId","a.FakeLink","a.FakeDay",
    "a.FakeCount","a.FakeItem","a.Discount",
    "d.SkuTotalAmount"
  ]
  const allTypes = [" ASC"," DESC"]
  let orderContent = orderList[0] + allTypes[0]
  if(orderType) {
    const orderValue = (orderType/10) > orderList.length ? 0 : Math.floor(orderType/10)
    const typeValue = (orderType%10) > (allTypes.length - 1) ?  0 : orderType%10
    orderContent = orderList[orderValue] + allTypes[typeValue]
  }
  return orderContent
}

const dashboard = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)
    const query = {
      start: querDate.start,
      end: querDate.end,
      last: querDate.last,
      project: queryCheck.project
    }
    const productCount = await service.monitor.vendor.dashboardCount(query)
    const productTotal = await service.monitor.vendor.dashboardTotal(query)
    const productRate = (productCount.FakeShopCount * 100 / productTotal.ShopCount).toFixed(2) + '%'
    const shopCount = await service.monitor.vendor.dashboardAuth(query)
    const whiteCount = [shopCount]
    const whiteRate = [{
      name: 'FakeShopRate',
      FakeShopRate: (shopCount.FakeShopCount * 100 / productTotal.ShopCount).toFixed(2) + '%'
    }]

    const data = [
      [productCount],
      [{
        name: 'FakeShopRate',
        FakeShopRate: productRate
      }],whiteCount, whiteRate,
      await service.monitor.vendor.getStatusLevel(query),
      await service.monitor.vendor.dailyChannel(query),
      await service.monitor.filter.getUpdateTime(query)
    ]
    return data
  } else return []
}

const filter = async (params) => {
  const userProject = await checkProject(params)

  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      search: params.search
    }
    // const totalItem = await service.price.filter.totalItem(query)
    // const totalLink = await service.price.filter.totalLink(query)
    // const fakeLink = await service.price.filter.fakeLink(query)
    // const formatList = [
    //   totalItem[0].Count,
    //   totalLink[0].Count,
    //   fakeLink[0].Count,
    //   0,0,0,0
    // ]
    const data = [
      await service.monitor.filter.filterChannel(query),
      await service.monitor.filter.filterSeries(query),
      await service.monitor.filter.filterMode(query)
    ]
    return data
  } else return []
}

const paging = async (params) => {
  const userProject = await checkProject(params)
  if (userProject.length) {
    const queryCheck = {
      start: params.start,
      end: params.end,
      project: userProject.toString()
    }

    const querDate = await checkdate(queryCheck)

    const query = {
      start: querDate.start,
      end: querDate.end,
      project: queryCheck.project,
      channel: params.channel,
      authorize: params.authorize,
      search: params.search,
      order: checkOrder(params.order),
      skip: (params.page - 1) * params.size,
      size: params.size
    }
    const pageList = await service.monitor.vendor.pageList(query)

    const data = [
      await service.monitor.vendor.pageCount(query),
      pageList
    ]
    return data
  } else return []
}

const list = async (params) => {
  const userProject = await checkProject(params)

  const query = {
    project: userProject.toString(),
    shopId: params.shopId,
    skip: (params.page - 1) * params.size,
    size: params.size
  }
  const datalist = await service.monitor.vendor.getDetail(query)
  datalist.forEach(element => {
    const labarray = []
    if(element.hasct) labarray.push('明显假冒')
    if(element.hasbrd) labarray.push('滥用商标词')
    if(element.haspic) labarray.push('盗图')
    element.itemlab = labarray.toString()
  })

  return datalist
}

export default {
  dashboard,
  filter,
  paging,
  list
}
