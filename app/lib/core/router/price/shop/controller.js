/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const dashboard = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboard(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const filter = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {date} start
     * @param {date} end
     * @param {string} series
     * @param {string} model
     * @param {string} channel
     * @param {string} search
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start || bodyParams.start_date,
      end: bodyParams.end || bodyParams.end_date,
      channel: bodyParams.channel || '',
      authorize: Number.parseInt(bodyParams.authorize) === 0 ? '0': bodyParams.authorize,
      search: bodyParams.search || '',
      mainid: req.baseInfo.mainid
    }

    const docs = await service.filter(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
const paging = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {number} page
     * @param {date} start
     * @param {date} end
     * @param {string} series
     * @param {string} model
     * @param {string} channel
     * @param {string} search
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      start: bodyParams.start,
      end: bodyParams.end,
      channel: bodyParams.channel || '',
      authorize: Number.parseInt(bodyParams.authorize) === 0 ? '0': bodyParams.authorize,
      search: bodyParams.search || '',
      order: bodyParams.order,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.paging(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const list = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} project
     * @param {number} page
     * @param {number} shopId
     */
    const bodyParams = req.query

    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      page: Number.parseInt(bodyParams.page) || 1,
      size: Number.parseInt(bodyParams.size) || 10,
      shopId: bodyParams.shopid,
      mainid: req.baseInfo.mainid
    }
    const docs = await service.list(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  dashboard,
  filter,
  paging,
  list
}
