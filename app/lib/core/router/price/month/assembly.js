/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
import service from '../../../libraries'
import phone from '../../../libraries/verify/phone'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  return userProject
}

const getlist = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  const docs = await service.monitor.month.monthFilter(query)
  return docs
}

const dashboardItem = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return cbData
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month

  const cbData = {
    board: {
      CurrentLink: {
        Count: 0,
        Rate: 0
      },
      FakeLink: {
        Count: 0,
        Rate: 0
      },
      FakeCount: {
        Count: 0,
        Rate: 0
      }   
    }, chart: [[],[],[],[]]
  }

  const itemTitle = await service.monitor.month.dashboardItem(query)
  if (itemTitle.length) cbData.board = {
    CurrentLink: {
      Count: itemTitle[1].CurrentLink || 0,
      Rate: itemTitle[0].CurrentLink? (itemTitle[1].CurrentLink - itemTitle[0].CurrentLink)/itemTitle[0].CurrentLink : 1
    },
    FakeLink: {
      Count: itemTitle[1].FakeLink || 0,
      Rate: itemTitle[0].FakeLink? (itemTitle[1].FakeLink - itemTitle[0].FakeLink)/itemTitle[0].FakeLink : 1
    },
    FakeCount: {
      Count: itemTitle[1].FakeCount || 0,
      Rate: itemTitle[0].FakeCount? (itemTitle[1].FakeCount - itemTitle[0].FakeCount)/itemTitle[0].FakeCount : 1
    }   
  }
  cbData.chart[0] = await service.monitor.month.dashboardDaily(query)
  cbData.chart[1] = await service.monitor.month.channelItem(query)

  return cbData
}
const dashboardShop = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return cbData
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month
  const cbData = {
    board: {
      FakeShop: {
        Count: 0,
        Rate: 0
      },
      CurrentShop: {
        Count: 0,
        Rate: 0
      },
      AuthShop: {
        Count: 0,
        Rate: 0
      }   
    }, chart: [[],[],[],[]]
  }

  const itemTitle = await service.monitor.month.dashboardShop(query)
  if (itemTitle.length) cbData.board = {
    FakeShop: {
      Count: itemTitle[1].FakeShop || 0,
      Rate: itemTitle[0].FakeShop? (itemTitle[1].FakeShop - itemTitle[0].FakeShop)/itemTitle[0].FakeShop : 1
    },
    CurrentShop: {
      Count: itemTitle[1].CurrentShop || 0,
      Rate: itemTitle[0].CurrentShop? (itemTitle[1].CurrentShop - itemTitle[0].CurrentShop)/itemTitle[0].CurrentShop : 1
    },
    AuthShop: {
      Count: itemTitle[1].AuthShop || 0,
      Rate: itemTitle[0].AuthShop? (itemTitle[1].AuthShop - itemTitle[0].AuthShop)/itemTitle[0].AuthShop : 1
    }   
  }
  cbData.chart[0] = await service.monitor.month.dashboardDaily(query)
  cbData.chart[1] = await service.monitor.month.channelShop(query)
  cbData.chart[2] = await service.monitor.month.shopDetail(query)
  cbData.chart[3] = await service.monitor.month.getShopDiscount(query)

  return cbData
}

const itemList = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return []
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month
  const docs = await service.monitor.month.itemList(query)
  return docs
}
const discountList = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return []
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month
  const docs = await service.monitor.month.linkList(query)
  return docs
}
const shopList = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return []
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month
  const orderlist = ['FakeLink DESC', 'FakeCount DESC', 'Discount ASC']
  if (parseInt(params.order) && parseInt(params.order) < 3) query.order = orderlist[parseInt(params.order)]
  else query.order = orderlist[0]
  const docs = await service.monitor.month.shopList(query)
  return docs
}

const skuinfo = async (params) => {
  const userProject = await checkProject(params)
  const query = {
    project: userProject.toString(),
    mainid: params.mainid
  }
  if (!params.month) {
    const contentList = await getlist(params)
    if(!contentList.length) return []
    else query.month = contentList[0].CurrentMonth
  } else query.month = params.month
  if (!params.skuid) {
    const docs = await service.monitor.month.itemList(query)
    if (docs.length) query.sku = docs[0].skuid
    else query.sku = 0
  } else query.sku = params.skuid

  const cbData = [
    await service.monitor.month.getSkuDiscount({sku:query.sku,querymonth:query.month}),
    await service.monitor.month.getSkuDiscount({sku:query.sku,month:query.month})
  ]

  return cbData
}

export default {
  getlist,
  dashboardItem,
  dashboardShop,
  itemList,
  discountList,
  shopList,
  skuinfo
}
