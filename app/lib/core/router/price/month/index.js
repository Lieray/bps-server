/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
import auth from '../../auth/controller'
const baseUri = '/V3'

const router = express.Router()

router.get(baseUri + '/price/statistics/detail', auth.baseInfo, controller.getlist)
router.get(baseUri + '/price/statistics/iteminfo', auth.baseInfo, controller.dashboardItem)
router.get(baseUri + '/price/statistics/shopinfo', auth.baseInfo, controller.dashboardShop)
router.get(baseUri + '/price/statistics/itemlist', auth.baseInfo, controller.itemList)
router.get(baseUri + '/price/statistics/discount', auth.baseInfo, controller.discountList)
router.get(baseUri + '/price/statistics/shoplist', auth.baseInfo, controller.shopList)
router.get(baseUri + '/price/statistics/skuinfo', auth.baseInfo, controller.skuinfo)

export default router
