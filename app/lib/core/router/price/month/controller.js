/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import service from './assembly'

const getlist = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} subid
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.getlist(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const dashboardItem = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboardItem(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const dashboardShop = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.dashboardShop(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const itemList = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.itemList(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const discountList = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.discountList(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const shopList = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      order: bodyParams.order,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.shopList(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const skuinfo = async (req, res) => {
  try {
    /**
     * 参数校验
     * @param {string} sku
     */
    const bodyParams = req.query
    const params = {
      project: bodyParams.project ||  bodyParams.subid,
      month: bodyParams.month,
      skuid: bodyParams.skuid,
      mainid: req.baseInfo.mainid
    }

    const docs = await service.skuinfo(params)
    return res.json({
      success: true,
      query: req.query,
      results: docs
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  getlist,
  dashboardItem,
  dashboardShop,
  itemList,
  discountList,
  shopList,
  skuinfo
}
