/**
 * 配置可用模块
 */
import monitor from './monitor'
import detail from './detail'
import priceshop from './shop'
import pricemonth from './month'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  price: monitor,
  itemdetail: detail,
  priceshop,
  pricemonth
}
