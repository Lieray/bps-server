/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
const baseUri = '/V3'

const router = express.Router()

router.post(baseUri + '/users/signup', controller.singup)
router.put(baseUri + '/verify/activate', controller.activate)

export default router
