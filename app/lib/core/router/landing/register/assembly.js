/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import moment from 'moment'
import service from '../../../libraries'

/**
 * 生成激活信息，submitted为用户提交的数据，其内容为：
 * @param {string} company
 * @param {string} email
 * @param {string} password
 * @param {string} phone
 * @param {string} username
 */

const singup = async (docs) => {
  const cbData = {
    success: true,
    message: 'OK'
  }
  docs.phoneStatus = false
  docs.type = 1
  // 直接更新手机状态
  // if (docs.phone) {
  //   const checkedStatus = await service.verify.phone.useSms(docs)
  //   if (checkedStatus.length && checkedStatus[0]) docs.phoneStatus = true
  //   else throw new Error('Error happens with the status code: 400, the key is: Code')
  // } else throw new Error('Error happens with the status code: 400, the key is: phone')
  // 如果邮箱已存在，则返回错误信息
  const user = await service.information.account.getByEmail(docs.email)
  if (user) {
    cbData.success = false,
    cbData.message = 'existed'
    return cbData
  }

  const activationEmails = await service.verify.email.getByEmail({ email: docs.email, type: 1 })
  if (activationEmails.length) {
    cbData.success = false,
    cbData.message = 'unactivate'
    return cbData
  }
    // throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  const tokenInfo = await service.identify.password.genPwd(docs.password)
  // 生成待激活记录
  const result = await service.verify.activate.createInfo(docs, tokenInfo)
  // 异步发送邮件
  service.verify.email.activateInfo({
    email: result.email,
    uuid: result.uuid,
    token: result.token
  })
  return cbData
}

/**
 * 账号激活
 * @param {string} token
 * @param {string} uuid
 */

const activate = async ({ token, uuid }) => {
  // 先查询相关参数是否正确
  const userInfo = await service.verify.email.getByCode({ uuid, token, type: 1 })
  if (!userInfo) throw new Error('Error happens with the status code: 400, the key is: token')
  const cbMsg = {
    success: true,
    message: 'OK'
  }
  // 对未激活且未过期的账号进行激活，并创建新账号，已激活则跳过此操作
  if (userInfo.status === 'unactivated') {
    if (userInfo.expire && moment(userInfo.expire).isAfter()) {
      await service.verify.activate.createUser(userInfo)
    } else throw new Error('Error happens with the status code: 403, the key is: Token expired')
  } else cbMsg.message = '已激活'
  return cbMsg
}

export default {
  singup,
  activate
}
