/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 * 返回数据的格式化放在这里进行
 */
import serverErr from './errors'
// import { formatted } from '../../../utils'
import service from './assembly'

const Validater = {
  // 邮箱必须包含@符号，@前长度50，@字符后长度50
  email: (value) => {
    if (typeof (value) !== 'string') return false
    const arr = value.split('@')
    if (arr.length !== 2) return false
    if (arr[0].length > 50 || arr[1].length > 50) return false
    return true
  },
  // 账户名格式为中英文+数字,5-20个字符，一个中文字为两个字符
  username: (value) => {
    if (typeof (value) !== 'string') return false
    // const valueInfo = formatted.strChecker(value)
    // if (valueInfo.others || valueInfo.spaces || valueInfo.punctuations) return false
    // const valueLength = valueInfo.characters * 2 + valueInfo.lowercases + valueInfo.uppercases + valueInfo.number
    // if (valueLength < 5 || valueLength > 20) return false
    return true
  },
  // 密码要求包含5-25个字符，只可以是字母、数字和符号，其中至少包含两种
  password: (value) => {
    if (typeof (value) !== 'string') return false
    // const valueInfo = formatted.strChecker(value)
    // if (valueInfo.others || valueInfo.spaces || valueInfo.characters) return false
    // let valueType = 0
    // let valueLength = 0
    // if (valueInfo.punctuations) {
    //   valueLength += valueInfo.punctuations
    //   valueType++
    // }
    // if (valueInfo.number) {
    //   valueLength += valueInfo.number
    //   valueType++
    // }
    // if (valueInfo.lowercases || valueInfo.uppercases) {
    //   valueLength += valueInfo.lowercases + valueInfo.uppercases
    //   valueType++
    // }
    // if (valueType < 2) return false
    // if (valueLength < 5 || valueLength > 20) return false
    return true
  },
  company: (value) => {
    if (typeof (value) !== 'string') return false
    return true
  },
  telnum: (value) => {
    if (value && value.length > 11) return false
    // if (!(/^1[3456789]\d{9}$|^\+861[3456789]\d{9}$|^861[3456789]\d{9}/g.test(value))) return false
    return true
  },
  uuid: (value) => {
    if (typeof (value) !== 'string') return false
    return true
  },
  activateToken: (value) => {
    if (typeof (value) !== 'string') return false
    return true
  }
}

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (Validater[key]) {
        if (Validater[key](target[key])) return target[key]
        else throw new Error(`Error happens with the status code: 400, the key is: ${key}`)
      }
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

// 注册
const singup = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} username
     * @param {string} email
     * @param {string} password
     * @param {string} company
     * @param {string} telnum
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = ((
      {
        email,
        username,
        password,
        company,
        telnum
      }) => (
      {
        email,
        username,
        password,
        company,
        phone: telnum
      }
    ))(bodyParse)
    const serviceResult = await service.singup(params)
    return res.json(serviceResult)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}
// 激活
const activate = async (req, res) => {
  try {
    /**
     * put方法，读取req.body中的参数
     * @param {string} activateToken
     * @param {string} uuid
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ activateToken: token, uuid }) => ({ token, uuid }))(bodyParse)

    const resMsg = await service.activate(params)
    return res.json(resMsg)
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  singup,
  activate
}
