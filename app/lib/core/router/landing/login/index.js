/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
const baseUri = '/V3'

const router = express.Router()

router.post(baseUri + '/users/login', controller.login)
router.get(baseUri + '/users/logout', controller.logout)

export default router
