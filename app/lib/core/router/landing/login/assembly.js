/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import moment from 'moment'
import service from '../../../libraries'

const login = async (docs) => {
  // 查找账号
  const accountInfo = await service.information.account.checkEmail(docs.email)
  // 如果不存在，则查看待激活记录
  if (!accountInfo) {
    const activationEmails = await service.verify.email.getByEmail({ email: docs.email, type: 1 })
    if (activationEmails.length) {
      // 判断最后一条是否已过期
      const currentInfo = activationEmails.pop()
      if (currentInfo.expire && moment(currentInfo.expire).isAfter()) {
        // 如果未过期，则重新发送邮件，并提示
        service.verify.email.activateInfo({
          email: currentInfo.email,
          uuid: currentInfo.uuid,
          token: currentInfo.token
        })
        return {
          success: false,
          login_status: 1,
          message: 'unactivate'
        }
      }
      // 哪里都没查到，则报错
    } else throw new Error(`Error happens with the status code: 400, the key is: ${docs.email}`)
  }

  // 验证密码
  const pwdInfo = await service.identify.password.checkPwd({ userid: accountInfo.userid, password: docs.password })

  if (!pwdInfo) throw new Error(`Error happens with the status code: 400, the key is: ${docs.password}`)
  const defaultLevel = {
    balance: 0,
    package: {
      commmerce: [],
      digital: [],
      price: []
    }
  }
  // 有主账号才可能是会员，非会员不绑定主账号
  // 现在默认都有mainid
  if (accountInfo.mainid) {
    // 获取订阅
    const projectList = await service.subscribe.info.getList({mainId: accountInfo.mainid})

    // 根据权限渲染菜单
    projectList.forEach(muneset => {
      if((Number.parseInt(muneset.type) & 1) === 1) {
        defaultLevel.package.price.push(muneset)
      }
      if((Number.parseInt(muneset.type) & 2) === 2) {
        defaultLevel.package.commmerce.push(muneset)
      }
      if((Number.parseInt(muneset.type) & 4) === 4) {
        defaultLevel.package.digital.push(muneset)
      }
    })
  }

  // 获取用户信息
  const userInfo = await service.information.baseinfo.getByID(accountInfo.userid)
  userInfo.email = docs.email

  const query = {
    ip: docs.ip,
    email: docs.email,
    userid: accountInfo.userid,
    mainid: accountInfo.mainid
  }
  // 登录
  const tokenInfo = await service.identify.acl.createJWT(query)

  return {
    success: true,
    code: 0,
    user: userInfo,
    package: defaultLevel.package,
    balance: defaultLevel.balance,
    token: tokenInfo.token
  }
}

const logout = async (submitted) => {
  return submitted
}

export default {
  login,
  logout
}
