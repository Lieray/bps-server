/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
const baseUri = '/V3'

const router = express.Router()

router.post(baseUri + '/verify/sms', controller.prequest)
router.put(baseUri + '/verify/sms', controller.verifysms)
router.get(baseUri + '/verify/email', controller.checkUser)
router.put(baseUri + '/verify/email', controller.resetPwd)

export default router
