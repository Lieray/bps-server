/**
 * controllers 中负责与 http 的连接工作
 * 只做简单参数的验证，传参和调起 services
 * 禁止把 rep, res 对像做为参数传递到 services 层
 */
import serverErr from './errors'
import { formatted } from '../../../utils'
import service from './assembly'

const Validater = {
  // 邮箱必须包含@符号，@前长度50，@字符后长度50
  email: (value) => {
    if (typeof (value) !== 'string') return false
    const arr = value.split('@')
    if (arr.length !== 2) return false
    if (arr[0].length > 50 || arr[1].length > 50) return false
    return true
  },
  telnum: (value) => {
    if (!(/^1[3456789]\d{9}$|^\+861[3456789]\d{9}$|^861[3456789]\d{9}/g.test(value))) return false
    return true
  },
  smsid: (value) => {
    if (!(/^\d{6}$/g.test(value))) return false
    return true
  },
  passwordtoken: (value) => {
    if (typeof (value) !== 'string' || value.length < 1) return false
    return true
  },
  uuid: (value) => {
    if (typeof (value) !== 'string' || value.length < 1) return false
    return true
  },
  // 密码要求包含5-25个字符，只可以是字母、数字和符号，其中至少包含两种
  password: (value) => {
    if (typeof (value) !== 'string') return false
    const valueInfo = formatted.strChecker(value)
    if (valueInfo.others || valueInfo.spaces || valueInfo.characters) return false
    let valueType = 0
    let valueLength = 0
    if (valueInfo.punctuations) {
      valueLength += valueInfo.punctuations
      valueType++
    }
    if (valueInfo.number) {
      valueLength += valueInfo.number
      valueType++
    }
    if (valueInfo.lowercases || valueInfo.uppercases) {
      valueLength += valueInfo.lowercases + valueInfo.uppercases
      valueType++
    }
    if (valueType < 2) return false
    if (valueLength < 5 || valueLength > 20) return false
    return true
  }
}

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (Validater[key]) {
        if (Validater[key](target[key])) return target[key]
        else throw new Error(`Error happens with the status code: 400, the key is: ${key}`)
      }
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const prequest = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} email
     * @param {string} telnum
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ email, telnum }) => ({ email, phone: telnum }))(bodyParse)
    const serviceResult = await service.prequest(params)
    return res.json({
      success: true,
      message: 'OK',
      data: serviceResult
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

const verifysms = async (req, res) => {
  try {
    /**
     * post方法，读取req.body中的参数
     * @param {string} email
     * @param {string} telnum
     * @param {string} smsid
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ email, telnum, smsid }) => ({ email, phone: telnum, token: smsid }))(bodyParse)
    await service.verifysms(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

// 发送认证邮件
const checkUser = async (req, res) => {
  try {
    /**
     * get方法，读取req.query中的参数
     * @param {string} email
     */
    const bodyParams = req.query
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ email }) => ({ email }))(bodyParse)

    const serviceResult = await service.checkUser(params)
    return res.json({
      success: true,
      message: 'OK',
      data: serviceResult

    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

// // 重置密码
const resetPwd = async (req, res) => {
  try {
    /**
     * put方法，读取req.body中的参数
     * @param {string} passwordtoken
     * @param {string} uuid
     * @param {string} password
     */
    const bodyParams = req.body
    const bodyParse = paramsFormat(bodyParams)
    const params = (({ passwordtoken: token, uuid, password }) => ({ token, uuid, password }))(bodyParse)
    await service.resetPwd(params)
    return res.json({
      success: true,
      message: 'OK'
    })
  } catch (e) {
    const { eStack, bcJson } = serverErr(e)
    res.statusCode = eStack.code
    return res.json(bcJson)
  }
}

export default {
  prequest,
  verifysms,
  checkUser,
  resetPwd
}
