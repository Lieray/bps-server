/**
 * 数据分发，仅在本文件中进行与其他模块的业务连通
 */
'use strict'
import moment from 'moment'
import service from '../../../libraries'

/**
 * 注册时的手机号验证
 * 验证码为4位数，验证码24小时内5条上限，超过条数页面提示“今日验证码已达上限”
 * 在注册阶段，同一邮箱或同一手机号均视为同一账户，24小时内上限5条
 * @param {string} email
 * @param {string} phone
 */

const prequest = async (docs) => {
  // 如果邮箱已存在，则返回错误信息
  const user = await service.information.account.getByEmail(docs.email)
  if (user) throw new Error(`Error happens with the status code: 403, the key is: ${docs.email}`)
  // 如果验证码已达上限，则返回错误信息
  docs.type = 1
  const smsInfos = await service.verify.phone.getByBoth(docs)
  if (smsInfos) throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  // 生成待激活记录
  const genToken = await service.verify.phone.createSMS(docs)
  // 发送短信
  service.verify.phone.sendSms({
    phone: docs.phone,
    msg: `您的手机验证码为：${genToken.token}，有效期15分钟。如非本人操作，请忽略本短信！`
  })

  return genToken
}

/**
 * 后端发送验证码15分钟内有效
 * @param {string} email
 * @param {string} phone
 * @param {string} msg
 */
const verifysms = async (docs) => {
  // 检查是否存在符合条件的验证码
  docs.type = 1
  const smsInfo = await service.verify.phone.checkSms(docs)
  // 激活验证码
  if (smsInfo) {
    if (smsInfo.status === 'unactivated') {
      await service.verify.phone.putSms(docs)
      return docs
    } else if (smsInfo.status === 'activated') return docs
    else throw new Error(`Error happens with the status code: 403, the key is: ${docs.token}`)
  } else throw new Error(`Error happens with the status code: 400, the key is: ${docs.token}`)
}

// /**
//  * 发送邮件，24小时有效
//  * @param {string} email
//  * @param {string} phone
//  * @param {string} msg
//  */
const checkUser = async (docs) => {
  // 如果邮箱不存在，或未处于激活状态，则返回错误信息
  const user = await service.information.account.checkEmail(docs.email)
  if (!user) throw new Error(`Error happens with the status code: 401, the key is: ${docs.email}`)
  // 如果已有待激活邮件，则返回错误信息？？？
  docs.type = 2
  // const emailInfos = await service.verify.email.getByEmail(docs)
  // if (emailInfos.length) throw new Error(`Error happens with the status code: 406, the key is: ${docs.email}`)
  // 生成激活码
  const genToken = await service.verify.email.createToken(docs)
  // 发送邮件
  service.verify.email.verifyInfo({
    email: docs.email,
    uuid: genToken.uuid,
    token: genToken.token
  })
  return genToken
}

// /**
//  * 重置密码
//  * @param {string} token
//  * @param {string} uuid
//  */
const resetPwd = async (docs) => {
  // 检查是否存在符合条件的验证码
  docs.type = 2
  const smsInfo = await service.verify.email.getByCode(docs)
  // 激活验证码
  if (smsInfo) {
    if (smsInfo.status === 'unactivated') {
      // 先检查激活码是否已过期
      if (smsInfo.expire && moment(smsInfo.expire).isAfter()) {
        docs.email = smsInfo.email
        const user = await service.information.account.checkEmail(docs.email)
        docs.userid = user.userid
        await service.verify.email.useToken(docs)
        await service.identify.password.putPwd(docs)
        return docs
      } else throw new Error('Error happens with the status code: 403, the key is: Token expired')
    } else throw new Error('Error happens with the status code: 403, the key is: Token Used')
  } else throw new Error(`Error happens with the status code: 400, the key is: ${docs.token}`)
}

export default {
  prequest,
  verifysms,
  checkUser,
  resetPwd
}
