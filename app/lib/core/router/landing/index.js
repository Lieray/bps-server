/**
 * 配置可用模块
 */
import login from './login'
import register from './register'
import verify from './verify'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  login,
  register,
  verify
}
