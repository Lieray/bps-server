/**
 * 基础字符表
 */
const bsUnicode = {
  space: '\u0020', // 空格
  punctuations: [ // 标点符号
    ['\u0021', '\u002f'],
    ['\u003a', '\u0040'],
    ['\u005b', '\u0060'],
    ['\u007b', '\u007e']
  ],
  numbers: ['\u0030', '\u0039'],
  uppercases: ['\u0041', '\u005a'],
  lowercases: ['\u0061', '\u007a']
}
/**
 * 中文字符表
 */
const exUnicode = {
  space: '\u3000',
  punctuations: [ // 标点符号
    ['\uff21', '\uff2f'],
    ['\uff3a', '\uff40'],
    ['\uff5b', '\uff60'],
    ['\uff7b', '\uff7e']
  ],
  numbers: ['\uff30', '\uff39'],
  uppercases: ['\uff41', '\uff5a'],
  lowercases: ['\uff61', '\uff7a'],
  characters: [
    ['\u4E00', '\u9FA5'], // 基本汉字
    ['\u9FA6', '\u9FEF'], // 基本汉字补充
    ['\uF900', '\uFAD9'], // 兼容汉字
    ['\u3400', '\u4DB5'], // 扩展A
    ['\u{20000}', '\u{2A6D6}'], // 扩展B
    ['\u{2A700}', '\u{2B734}'], // 扩展C
    ['\u{2B740}', '\u{2B81D}'], // 扩展D
    ['\u{2B820}', '\u{2CEA1}'], // 扩展E
    ['\u{2CEB0}', '\u{2EBE0}'], // 扩展F
    ['\u{2F800}', '\u{2FA1D}'] // 兼容扩展
  ],
  radicals: [
    ['\u2F00', '\u2FD5'], // 康熙部首
    ['\u2E80', '\u2EF3'], // 部首扩展
    ['\u31C0', '\u31E3'], // 汉字笔画
    ['\u2FF0', '\u2FFB'], // 汉字结构
    ['\u3105', '\u312F'], // 汉语注音
    ['\u31A0', '\u31BA'], // 注音扩展
    ['\uE815', '\uE86F'], // PUA(GBK)部件
    ['\uE400', '\uE5E8'], // 部件扩展
    ['\uE600', '\uE6CF'] // PUA增补
  ],
  punctuationsEX: [
    ['\u3001', '\u3003'],
    ['\u3005', '\u3011'],
    ['\u3014', '\u301F'],
    ['\uFE30', '\uFE4F']
  ]
}

class StrChecker {
  constructor (str) {
    this.str = str
  }

  space () {
    const pattern = `[${bsUnicode.space}${exUnicode.space}]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || 0
    return detail.length
  }

  punctuation () {
    const pattern = `[
      ${bsUnicode.punctuations[0][0]}-${bsUnicode.punctuations[0][1]}
      ${bsUnicode.punctuations[1][0]}-${bsUnicode.punctuations[1][1]}
      ${bsUnicode.punctuations[2][0]}-${bsUnicode.punctuations[2][1]}
      ${bsUnicode.punctuations[3][0]}-${bsUnicode.punctuations[3][1]}
    ]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || []
    return detail.length
  }

  number () {
    const pattern = `[${bsUnicode.numbers[0]}-${bsUnicode.numbers[1]}]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || []
    return detail.length
  }

  uppercase () {
    const pattern = `[${bsUnicode.uppercases[0]}-${bsUnicode.uppercases[1]}]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || []
    return detail.length
  }

  lowercases () {
    const pattern = `[${bsUnicode.lowercases[0]}-${bsUnicode.lowercases[1]}]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || []
    return detail.length
  }

  character () {
    const pattern = `[${exUnicode.characters[0]}-${exUnicode.characters[1]}]`
    const rgp = new RegExp(pattern, 'g')
    const detail = this.str.match(rgp) || []
    return detail.length
  }
}

export default (str) => {
  const checkStr = new StrChecker(str)
  const strInfo = {
    spaces: checkStr.space(),
    punctuations: checkStr.punctuation(),
    number: checkStr.number(),
    uppercases: checkStr.uppercase(),
    lowercases: checkStr.lowercases(),
    characters: checkStr.character()
  }
  const defaultLen = Object.values(strInfo).reduce((acc, cur) => {
    return acc + cur
  }, 0)
  const others = str.length - defaultLen
  strInfo.others = others
  return strInfo
}
