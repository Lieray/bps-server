import strChecker from './validate'
import strEncode from './base64unicode'

export default {
  strChecker,
  strEncode
}
