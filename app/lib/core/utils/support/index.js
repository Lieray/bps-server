import email from './email'
import sms from './sms'
import order from './order'

export default {
  email,
  sms,
  order
}
