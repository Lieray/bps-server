import fetch from 'node-fetch'
import nconf from 'nconf'

const createOrder = async (params) => {
  const kfInfo = await nconf.get('kfInfo')
  const APIKey = kfInfo.APIKey
  const code = new Buffer.from(`${params.email}/token:${APIKey}`).toString('base64')

  return new Promise((resolve, reject) => {
    fetch('https://simplybrand.kf5.com/apiv2/requests.json', { 
      method: 'post',
      credentials: 'include', 
      body: JSON.stringify({
        request:{
          title: params.title,
          comment: { content: params.comment }
        }
      }),
      headers: { 
        'Authorization': `Basic ${code}`,
        'Content-Type': 'application/json; charset=UTF-8'
      } 
    }).then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

const replyOrder = async (params) => {
  const kfInfo = await nconf.get('kfInfo')
  const APIKey = kfInfo.APIKey
  const code = new Buffer.from(`${params.email}/token:${APIKey}`).toString('base64')

  return new Promise((resolve, reject) => {
    fetch(`https://simplybrand.kf5.com/apiv2/requests/${params.id}.json`, { 
      method: 'put',
      credentials: 'include', 
      body: JSON.stringify({
        request:{
          comment: { 
            content: `${params.content}` 
          }
        }
      }),
      headers: { 
        'Authorization': `Basic ${code}`,
        'Content-Type': 'application/json; charset=UTF-8'
      } 
    }).then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

export default {
  createOrder,
  replyOrder
}