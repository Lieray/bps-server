import uuidv4 from 'uuid/v4'
import bcrypt from 'bcrypt'
import crypto from 'crypto'
/**
 * 生成手机验证码
 */
const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}
const genSMS = () => {
  return {
    uuid: uuidv4(),
    token: getRandomInt(123456, 987654)
  }
}

const genToken = () => {
  return {
    uuid: uuidv4(),
    token: crypto.randomBytes(128).toString('hex')
  }
}

const genPassword = (password, str) => {
  const salt = bcrypt.genSaltSync(str)
  return {
    salt,
    uuid: uuidv4(),
    password: bcrypt.hashSync(password, salt),
    token: crypto.randomBytes(128).toString('hex')
  }
}

const md5 = (somestr) => {
  return crypto
    .createHash('md5')
    .update(somestr)
    .digest('hex')
}

const checkPwd = async (input, record) => {
  return bcrypt.compareSync(input, record)
}

export default {
  genSMS,
  genToken,
  genPassword,
  checkPwd,
  md5
}
