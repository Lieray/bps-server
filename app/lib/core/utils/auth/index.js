import jwt from 'jsonwebtoken'
import uuidv4 from 'uuid/v4'

const genJwtToken = async (email, authConfig) => {
  const uuid = uuidv4()
  const token = jwt.sign({ id: uuid, email, expire: authConfig.expiresIn }, authConfig.secretKey, {
    expiresIn: authConfig.expiresIn
  })
  return {
    uuid,
    token
  }
}

const verifyJWT = async (token, authConfig) => {
  const jwtInfo = jwt.verify(token, authConfig.secretKey)
  return { uuid: jwtInfo.id, email: jwtInfo.email, expire: jwtInfo.expire }
}

export default {
  genJwtToken,
  verifyJWT
}
