'use strict'

class Dal {
  constructor (dbName, dbModels) {
    this.dbName = dbName
    this.dbModels = dbModels
  }

  getModel () {
    return async () => {
      const models = await this.dbModels
      const db = models[this.dbName]
      if (!db) throw new Error(`Error happens with the status code: 506, the key is: ${this.dbName}`)
      return db
    }
  }

  add () {
    return async (data) => {
      const model = this.getModel()
      const db = await model()
      const dbInfo = await db.create(data)
      return dbInfo
    }
  }

  put () {
    return async (query, data) => {
      const model = this.getModel()
      const db = await model()
      const smsInfos = await db.update(data, {
        where: query
      })
      return smsInfos
    }
  }

  get () {
    return async (query) => {
      const model = this.getModel()
      const db = await model()
      const dbInfo = await db.findOne({
        where: query
      })
      return dbInfo
    }
  }

  find () {
    return async (query) => {
      const model = this.getModel()
      const db = await model()
      const dbInfos = await db.findAll({
        where: query
      })
      return dbInfos
    }
  }
}

export default (dbName, dbModels) => {
  const db = new Dal(dbName, dbModels)
  return {
    add: db.add(),
    put: db.put(),
    get: db.get(),
    find: db.find()
  }
}
