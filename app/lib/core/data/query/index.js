import dbquery from './sequelize/methods'
import sqlquery from './mysql'
import msquery from './mssql'

export {
  dbquery,
  sqlquery,
  msquery
}
