import localClient from './local'
import dataClient from './base'
import sucClient from './client'

const logger = global.logger

const localData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    localClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const viewData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    dataClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const subData = (sql) => {
  logger.silly(sql)
  return new Promise((resolve, reject) => {
    sucClient.query(sql, (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

export default {
  localData,
  viewData,
  sucClient,
  subData
}
