
import db from '../query/sequelize'
/**
 * 注册可用数据库
 */
const defaultModels = {
  userModels: {
    Account: 'user/consumer/account',
    Baseinfo: 'user/consumer/baseinfo',
    Password: 'user/consumer/password',
    ACL: 'user/identification/acl',
    Verification: 'user/identification/verification'
  }
}

const dbModels = db(defaultModels.userModels)

export default dbModels
