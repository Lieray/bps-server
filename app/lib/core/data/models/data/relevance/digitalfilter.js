'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'digitalfilter',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      dataId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      type: { type: DataTypes.STRING(20), allowNull: false },
      level: { type: DataTypes.STRING(20), allowNull: false },
      title: { type: DataTypes.STRING(200), allowNull: false },
      analysis: { type: DataTypes.STRING(20), allowNull: false },
      discriminant: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      result: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
