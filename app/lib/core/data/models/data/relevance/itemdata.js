'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'itemdata',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      ResultId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      ShopId: { type: DataTypes.STRING(50), allowNull: true },
      producturl: { type: DataTypes.STRING(300), allowNull: true },
      ShopURL: { type: DataTypes.STRING(200), allowNull: true },
      DiscriminantReason: { type: DataTypes.STRING(500), allowNull: true },
      DiscriminantTime: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
