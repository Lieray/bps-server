'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'digitaldata',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      DataId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      ProjectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      url: { type: DataTypes.STRING(800), allowNull: true },
      content: { type: DataTypes.STRING(800), allowNull: true },
      Poster: { type: DataTypes.STRING(50), allowNull: true },
      PostTime: { type: DataTypes.DATE, allowNull: true },
      SiteName: { type: DataTypes.STRING(200), allowNull: true },
      DiscriminantTime: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
