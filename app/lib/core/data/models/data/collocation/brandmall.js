'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'mall',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      resultId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      topId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      category: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      link: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      shopName: { type: DataTypes.STRING(50), allowNull: false },
      shopId: { type: DataTypes.STRING(50), allowNull: false },
      result: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      discriminant: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
