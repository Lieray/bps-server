'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define('top', {
    // ID用于用户查询
    id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
    name: { type: DataTypes.STRING(50), allowNull: false },
    category: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
    index: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true }
  },
  {
    timestamps: false,
    freezeTableName: true
  })
}