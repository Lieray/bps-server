'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define('category', {
    id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
    categoryId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
    name: { type: DataTypes.STRING(50), allowNull: false },
    index: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true }
  },
  {
    timestamps: false,
    freezeTableName: true
  })
}
