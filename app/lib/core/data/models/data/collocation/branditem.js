'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'item',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      resultId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      topId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // brand为BrandCheck的ID
      brand: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      category: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      brandId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      result: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      score: { type: DataTypes.DOUBLE, allowNull: false },
      description: { type: DataTypes.STRING(200), allowNull: false },
      discriminant: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
