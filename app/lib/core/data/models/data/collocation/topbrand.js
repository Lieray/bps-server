'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define('topbrand', {
    id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
    topId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
    // brand为BrandCheckId
    brand: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false }
  },
  {
    timestamps: false,
    freezeTableName: true
  })
}