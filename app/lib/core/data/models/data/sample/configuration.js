'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'configuration',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      content: { type: DataTypes.STRING(64), allowNull: false },
      /**
       * 目前只有两类，feedback和rpstatus
       * feedback分4级：0为系统反馈，1为商品分析，2为卖家分析，3为数字防线
       */
      name: { type: DataTypes.STRING(50), allowNull: false },
      index: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
