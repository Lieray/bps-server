'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'confidence',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      memberId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      type: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      minScore: { type: DataTypes.DOUBLE, allowNull: false },
      maxScore: { type: DataTypes.DOUBLE, allowNull: false },
      bucket: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      bucketName: { type: DataTypes.STRING(64), allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
