'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'store',
    {
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      channelName: { type: DataTypes.STRING(50), allowNull: false },
      shopName: { type: DataTypes.STRING(50), allowNull: false },
      shopLink: { type: DataTypes.STRING(50), allowNull: true },
      shopId: { type: DataTypes.STRING(50), allowNull: false, unique: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}