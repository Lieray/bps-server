'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'channellist',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // brand为BrandCheck的ID
      channelName: { type: DataTypes.STRING(50), allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
