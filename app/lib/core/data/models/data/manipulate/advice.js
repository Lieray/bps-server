'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'advice',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      type: { type: DataTypes.STRING(32), allowNull: false },
      content: { type: DataTypes.STRING(32), allowNull: false },
      createBy: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      createAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
