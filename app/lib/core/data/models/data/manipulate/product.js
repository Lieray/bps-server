'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'product',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      resultId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      status: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      createBy: { type: DataTypes.STRING(64), allowNull: false },
      updateBy: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      createAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      updateAt: { type: DataTypes.DATE, allowNull: true },
      protectAt: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
