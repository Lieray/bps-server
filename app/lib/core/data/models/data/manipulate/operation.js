'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'operation',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      resultId: { type: DataTypes.STRING(64), allowNull: false },
      // 反馈选项的ID
      reportId: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      content: { type: DataTypes.STRING(200), allowNull: true },
      // 组合数，十位：商品为1，卖家为2，数字为3；个位：投诉为1，反馈为2
      type: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      createBy: { type: DataTypes.STRING(64), allowNull: false },
      updateBy: { type: DataTypes.STRING(64), allowNull: true },
      createAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      updateAt: { type: DataTypes.DATE, allowNull: true },
      // 投诉为resultId+'@'+type，反馈为resultId+'@'+type+':'+projectId
      operationId: { type: DataTypes.STRING(128), unique: true, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
