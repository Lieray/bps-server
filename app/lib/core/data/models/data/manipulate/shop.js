export default (sequelize, DataTypes) => {
  return sequelize.define(
    'shop',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      shopId: { type: DataTypes.STRING(64), allowNull: false },
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      status: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      createBy: { type: DataTypes.STRING(64), allowNull: false },
      updateBy: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      createAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      updateAt: { type: DataTypes.DATE, allowNull: true },
      protectAt: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
