'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'verification',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      class: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      type: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      init: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      email: {
        type: DataTypes.STRING(120),
        allowNull: false,
        notEmpty: true
      },
      telephone: {
        type: DataTypes.STRING(20),
        allowNull: true
      },
      uuid: {
        type: DataTypes.UUID,
        defaultValue: null,
        allowNull: true,
        notEmpty: false,
        validate: { isUUID: 4 },
        unique: true
      },
      token: { type: DataTypes.STRING(256), allowNull: false },
      status: { type: DataTypes.ENUM, values: ['used', 'activated', 'unactivated'], defaultValue: 'unactivated', allowNull: false },
      expire: { type: DataTypes.DATE, allowNull: false },
      updateTime: { type: DataTypes.DATE, defaultValue: null, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}