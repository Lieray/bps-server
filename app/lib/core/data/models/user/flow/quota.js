'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'quota',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      recordId: { type: DataTypes.STRING(64), allowNull: false },
      memberId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      resultId: { type: DataTypes.STRING(64), allowNull: false },
      // 模块组合：十位：商品为1，卖家为2，数字为3；个位：系统扣款为0，投诉为1
      type: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      amount: { type: DataTypes.DOUBLE, allowNull: true },
      reason: { type: DataTypes.STRING(64), allowNull: true },
      recordTime: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
