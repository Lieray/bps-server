'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'recharge',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      memberId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      amount: { type: DataTypes.DOUBLE, allowNull: true },
      reason: { type: DataTypes.STRING(64), allowNull: true },
      recordId: { type: DataTypes.STRING(64), allowNull: true },
      recordTime: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
