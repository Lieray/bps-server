'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'account',
    {
      userid: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      infoId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // 主账号的userId
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      email: {
        type: DataTypes.STRING(120),
        allowNull: false,
        notEmpty: true,
        unique: true
      },
      status: { type: DataTypes.ENUM, values: ['active', 'pending'], defaultValue: 'pending', allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
