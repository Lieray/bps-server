'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'baseinfo',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      infoId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      userId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      username: {
        type: DataTypes.STRING(50),
        allowNull: false,
        notEmpty: true
      },
      telephone: {
        type: DataTypes.STRING(20),
        allowNull: false,
        notEmpty: true
      },
      company: {
        type: DataTypes.STRING(30),
        allowNull: true,
        notEmpty: true
      },
      url: {
        type: DataTypes.STRING(120),
        allowNull: true,
        notEmpty: true
      }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
