'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'project',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      packageId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // 子模块权限
      type: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      name: { type: DataTypes.STRING(32), allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
