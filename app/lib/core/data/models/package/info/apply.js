'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'project',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      packageId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      name: { type: DataTypes.STRING(32), allowNull: true },
      brand: { type: DataTypes.STRING(32), allowNull: true },
      shortname: { type: DataTypes.STRING(32), allowNull: true },
      nickname: { type: DataTypes.STRING(32), allowNull: true },
      series: { type: DataTypes.STRING(32), allowNull: true },
      article: { type: DataTypes.STRING(32), allowNull: true },
      product: { type: DataTypes.STRING(32), allowNull: true },
      alias: { type: DataTypes.STRING(32), allowNull: true },
      abbreviation: { type: DataTypes.STRING(32), allowNull: true },
      channel: { type: DataTypes.STRING(32), allowNull: true },
      result: { type: DataTypes.STRING(32), allowNull: true },
      status: { type: DataTypes.STRING(32), allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
