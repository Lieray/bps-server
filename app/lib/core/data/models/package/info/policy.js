'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'policy',
    {
      // 对于每个模块，主账号都可以有一条记录，即packageId
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      // 主账号的userId
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      // 权限组
      type: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      // 套餐名称
      name: { type: DataTypes.STRING(32), allowNull: false },
      // 开始时间
      init: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      // 到期时间
      expire: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
