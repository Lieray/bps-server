'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'empower',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      channelId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // 等级权限名称
      name: { type: DataTypes.STRING(32), allowNull: false },
      // 等级权限名称
      url: { type: DataTypes.STRING(1024), allowNull: false },
      // 描述
      desc: { type: DataTypes.STRING(32), allowNull: true },
      // 开始时间
      init: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      // 到期时间
      expire: { type: DataTypes.DATE, allowNull: true },
      // 是否删除
      status: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
