'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'content',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      // 同一组projectId为一类项目
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      name: { type: DataTypes.STRING(32), allowNull: false },
      content: { type: DataTypes.STRING(32), allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
