'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'stage',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      // 同一组projectId为一类项目
      projectId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      amount: { type: DataTypes.DOUBLE, allowNull: true },
      cost: { type: DataTypes.DOUBLE, allowNull: true },
      init: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      expire: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
