'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'level',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      // 主账号的userId
      mainId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      // 项目等级
      status: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      // 配置情况，由2进制转为10进制，管理单位为子模块
      type: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      name: { type: DataTypes.STRING(32), allowNull: false },
      init: { type: DataTypes.DATE, defaultValue: DataTypes.NOW, allowNull: false },
      expire: { type: DataTypes.DATE, allowNull: true }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
