'use strict'
/**
 * charset and collate are set at database level.
 * "charset": "utf8mb4"
 * "collate": "utf8mb4_unicode_520_ci"
 */
export default (sequelize, DataTypes) => {
  return sequelize.define(
    'authority',
    {
      id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER.UNSIGNED },
      // 这里为每个等级配置相应的权限，一个用户可以有多个权限
      memberId: { type: DataTypes.INTEGER.UNSIGNED, allowNull: false },
      // 模块类型，目前有2类：电商防线和数字防线
      class: { type: DataTypes.INTEGER.UNSIGNED, allowNull: true },
      // 子模块类型，对于电商防线，目前有2类：商品分析和卖家分析
      status: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false },
      // 用户类型，目前有1计点扣费和2包年包月两种
      type: { type: DataTypes.INTEGER.UNSIGNED, defaultValue: 0, allowNull: false }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  )
}
