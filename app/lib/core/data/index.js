'use strict'
/**
 * 模块访问的入口
 */
import dbModels from './models'
import { dbquery, sqlquery,msquery } from './query'
const localDB = {
  db: dbModels,
  dbquery
}
const dataBases = sqlquery.localData
const dataView = sqlquery.viewData
const dataAuth = sqlquery.subData
const sqlClient = sqlquery.sucClient
const msClient = msquery.localData

export {
  localDB,
  dataBases,
  dataView,
  sqlClient,
  msClient,
  dataAuth
}
