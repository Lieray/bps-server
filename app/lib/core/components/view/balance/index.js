import service from './dal'

const getQuota = async (mainid) => {
  const query = { mainid }

  const data = await service.getQuota(query)
  return data
}

export default {
  getQuota
}
