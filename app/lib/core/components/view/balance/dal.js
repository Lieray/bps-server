import { dataBases } from '../../../data'

const getQuota = async (params) => {
  const sql = `SELECT balance, cost FROM point WHERE mainId = ${params.mainid}`
  const sqlResult = await dataBases(sql)
  return sqlResult.length ? sqlResult[0] : { balance: 0 }
}

export default {
  getQuota
}
