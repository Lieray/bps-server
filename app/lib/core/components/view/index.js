/**
 * 配置可用模块
 */
import brand from './brand'
import balance from './balance'
import project from './project'


export default {
  brand,
  balance,
  project
}
