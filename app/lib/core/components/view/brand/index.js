import service from './dal'
import moment from 'moment'

const categoryList = async () => {
  const data = await service.categoryList()
  return data
}

const categoryView = async () => {
  const data = await service.categoryView()
  return data
}

const topbrand = async (docs) => {
  const query = (({
    category,
    start,
    end
  }) => ({
    category: category || 1,
    start,
    end
  }))(docs)

  const data = await service.topBrand(query)
  return data
}

const dateFormat = (docs) => {
  return {
    start: moment(docs.start).format('YYYY-MM-DD HH:mm:ss'),
    end: moment(docs.end).format('YYYY-MM-DD HH:mm:ss')
  }
}

const dateCheck = (docs) => {
  return {
    start: moment(docs.start).format('YYYY-MM-DD'),
    last: docs.last,
    end: moment(docs.end).format('YYYY-MM-DD HH:mm:ss')
  }
}

const checkBrand = async (docs) => {
  const brands = docs.brand
  const query = (({
    category
  }) => ({
    category: Number.parseInt(category) || 1
  }))(docs)

  if (!brands && brands !== 0) return ''
  let hasZero = brands === 0
  let brandList = []
  if (typeof brands === 'string') {
    brandList = brands.split(',')
    if (brandList.includes('0')) hasZero = true
  }
  if (hasZero) {
    const topbrands = await service.brandList(query)
    const topids = topbrands.map(item => item.id)
    let extids = topids
    if (brandList.length) extids = topids.filter(item => brandList.includes(item + ''))
    return { ext: extids.toString() }
  }
  return { brands }
}

export default {
  categoryList,
  topbrand,
  categoryView,
  dateFormat,
  checkBrand,
  dateCheck
}
