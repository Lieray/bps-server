import { dataBases } from '../../../data'

const categoryList = async () => {
  const sql = 'SELECT * FROM category'
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const categoryView = async () => {
  const sql = `SELECT * FROM category WHERE id = ${params.category}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const topBrand = async (params) => {
  const sql = `SELECT * FROM brand WHERE category = ${params.category}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}
export default {
  categoryList,
  topBrand,
  categoryView
}
