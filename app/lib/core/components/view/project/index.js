import service from './dal'

const getById = async (memberId) => {
  const query = { memberId }

  const data = await service.getById(query)
  return data
}

export default {
  getById
}
