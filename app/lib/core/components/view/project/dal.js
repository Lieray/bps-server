import { dataBases } from '../../../data'

const getById = async (params) => {
  let sql = 'SELECT a.id AS projectId, a.`type` AS projectType, a.`name` AS projectName, '
  sql += 'b.`name`, b.content, b.id FROM project a LEFT JOIN content b ON a.id = b.projectId '
  sql += `WHERE memberId = ${params.memberId} `
  const sqlResult = await dataBases(sql)
  const retDocs = []
  const retRecord = []
  sqlResult.forEach(element => {
    const rcdIndex = retRecord.findIndex(n => n === element.projectId)
    if (rcdIndex > -1) {
      if (!retDocs[rcdIndex].content[element.name]) {
        retDocs[rcdIndex].content[element.name] = {
          id: [element.id],
          docs: [element.content]
        }
      } else {
        retDocs[rcdIndex].content[element.name].id.push(element.id)
        retDocs[rcdIndex].content[element.name].docs.push(element.content)
      }
    } else {
      retRecord.push(element.projectId)
      const content = {}
      content[element.name] = {
        id: [element.id],
        docs: [element.content]
      }
      retDocs.push({
        projectId: element.projectId,
        projectName: element.projectName,
        projectType: element.projectType,
        content
      })
    }
  });
  return retDocs
}

export default {
  getById
}
