import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT count(shopId) AS Total FROM ( 
    SELECT shopId FROM brandmall WHERE `
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = `SELECT m.shopId, count(m.resultId) AS fakenum, SUM(m.link) AS linknum, 
            IFNULL(count(p.resultId),0) AS rpnum FROM brandmall m `
  sql += 'LEFT JOIN product p ON p.`status` = 4 AND m.resultId = p.resultId '
  sql += `WHERE m.category = ${params.category} AND `
  if (params.brand) sql += `m.topId IN (${params.brand}) AND `
  if (params.channel) sql += `m.channelId = ${params.channel} And `
  if (params.search) sql += `m.shopName like "%${params.search}%" And `
  sql += `m.discriminant >= "${params.start}" AND m.discriminant <= "${params.end}"
          GROUP BY m.shopId LIMIT ${params.skip}, ${params.size}`

  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
    })
    const particulars = `SELECT * FROM store WHERE shopId IN (${retDocs.toString()})`
    const viewResult = await dataBases(particulars)

    const channelDocs = new Set()
    viewResult.forEach(element => {
      channelDocs.add(element.channelId)
    })
    const channelSql = `SELECT ChannelId as channelId, ChannelName AS channelName FROM channellist 
                        WHERE channelId IN (${[...channelDocs].toString()})`
    const channelResult = await dataBases(channelSql)

    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.shopId) === Number.parseInt(element.shopId))
      const channeldoc = channelResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(extdoc.channelId))
      cbDocs.push({
        ShopName: extdoc.shopName,
        ShopURL: extdoc.shopLink,
        ChannelName: channeldoc.channelName,
        ChannelId: channeldoc.channelId,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      })
    })
  }
  return cbDocs
}

export default {
  getFakeList,
  getFakeCount
}
