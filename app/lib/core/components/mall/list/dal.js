import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT COUNT(DISTINCT shopId) AS FakeShopCount FROM ( 
    SELECT shopId FROM itemfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByStatus = async (params) => {
  let sql = 'SELECT COUNT(DISTINCT c.shopId) AS FakeShopCount FROM ( SELECT a.shopId FROM itemfilter a '
  if (Number.parseInt(params.rpstatus) === 1) sql += 'LEFT JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE b.shopId IS NULL AND '
  else sql += 'INNER JOIN shop b ON a.shopId = b.shopId  AND a.projectId = b.projectId AND `b`.`status` = ' + `${params.rpstatus} WHERE `
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `
  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `
  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" ) c`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = `SELECT shopId, COUNT(IF(result = 0,1,null)) AS fakenum,
            COUNT(IF(result = 0 AND link = 1,1,null)) AS linknum, `
  sql += 'COUNT(IF(`status`=4,1,null)) AS rpnum FROM `listfilter`'
  
  sql += `WHERE projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList

  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `

  sql += `(discriminant BETWEEN "${params.start}" AND "${params.end}") 
          GROUP BY shopId LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    const channelDocs = new Set()

    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
      channelDocs.add(element.channelId)
    })
    const statusSql = 'SELECT shopId, `status`, createAt FROM shop ' + `WHERE shopId IN (${retDocs.toString()})`
    const statusDocs = await dataBases(statusSql)

    const particulars = `SELECT * FROM store WHERE shopId IN (${retDocs.toString()})`
    const viewResult = await dataBases(particulars)

    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.shopId === element.shopId)

      const formatDoc = {
        ShopId: element.shopId,
        ShopName: extdoc? extdoc.shopName : null,
        ShopURL: extdoc? extdoc.shopLink : null,
        ChannelName: extdoc? extdoc.channelName : null,
        ChannelId: extdoc? extdoc.channelId : null,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      }

      if (params.authList) {
        const rpResult = statusDocs.find(ret => ret.shopId === element.shopId)
        const shopInfo = params.authList.find(t => t === element.shopId)
        if (shopInfo) {
          formatDoc.Authorize = 1
          formatDoc.RightsProtectionStatus = 0
          formatDoc.RightsProtectionTime = ''
        } else {
          formatDoc.Authorize = 0
          formatDoc.RightsProtectionStatus = rpResult ? rpResult.status : 1
          formatDoc.RightsProtectionTime = rpResult ? rpResult.createAt : ''
        }
      }

      cbDocs.push(formatDoc)
    })
  }
  return cbDocs
}

const getStatusList = async (params) => {
  let sql = `SELECT shopId, COUNT(IF(result = 0,1,null)) AS fakenum,
            COUNT(IF(result = 0 AND link = 1,1,null)) AS linknum, `
  sql += 'COUNT(IF(`status`=4,1,null)) AS rpnum FROM `listfilter` a'

  if (Number.parseInt(params.rpstatus) === 1) sql += 'LEFT JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE c.shopId IS NULL AND '
  else sql += 'INNER JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId AND `b`.`status` = ' + `${params.rpstatus} WHERE `
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `

  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `

  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY a.shopId LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
    })
    const statusSql = 'SELECT shopId, `status`, createAt FROM shop ' + `WHERE shopId IN (${retDocs.toString()})`
    const statusDocs = await dataBases(statusSql)

    const particulars = `SELECT * FROM store WHERE ShopId IN (${retDocs.toString()})`
    const viewResult = await dataBases(particulars)
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.shopId === element.shopId)
      const formatDoc = {
        ShopId: extdoc.shopId,
        ShopName: extdoc.shopName,
        ShopURL: extdoc.shopLink,
        ChannelName: extdoc.channelName,
        ChannelId: extdoc.channelId,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      }

      const rpResult = statusDocs.find(ret => ret.shopId === element.shopId)
      formatDoc.Authorize = 0
      formatDoc.RightsProtectionStatus = rpResult ? rpResult.status : 1
      formatDoc.RightsProtectionTime = rpResult ? rpResult.createAt : ''

      cbDocs.push(formatDoc)
    })
  }
  return cbDocs
}

export default {
  getFakeList,
  getByStatus,
  getFakeCount,
  getStatusList
}
