import { dataBases } from '../../../data'
const getByChannel = async (params) => {
  let sql = 'SELECT channelId FROM brandmall WHERE '
  sql += `category = ${params.category} `
  if (params.brand) sql += `AND topId IN (${params.brand}) `
  sql += 'GROUP BY channelId ORDER BY channelId '
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const sqlDocs = []
    sqlResult.forEach(element => {
      sqlDocs.push(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${sqlDocs.toString()})`
    const viewResult = await dataBases(particulars)
    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(element.channelId))
      cbDocs.push({
        name: 'ChannelList',
        ChannelId: element.channelId,
        ChannelName: extdoc ? extdoc.channelName : 'Undefined'
      })
    })
  }
  return cbDocs
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM brandmall 
            WHERE category = ${params.category} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getLastDate,
  getByChannel
}
