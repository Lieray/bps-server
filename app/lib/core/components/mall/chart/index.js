import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 1
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 1
  return data
}

const dashboardChannel = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  params.dataRange = []
  for (const day of range.by('days')) {
    params.dataRange.push(day.format('YYYY-MM-DD'))
  }
  const data = await service.getByChannel(params)
  return data
}

export default {
  dashboardCount,
  dashboardTotal,
  dashboardChannel
}
