import { dataBases } from '../../../data'
import Moment from 'moment'

const getFakeCount = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM itemfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteShop) sql += `shopId NOT IN (${params.whiteShop}) AND `
  else sql += 'result = 0 AND '
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM itemfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, discriminant, count(shopId) AS shopCount FROM itemfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY channelId, discriminant ORDER BY discriminant ASC`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    params.dataRange.forEach(element => {
      for(let listdoc of sqlDocs.keys()){
        const extdoc = viewResult.find(ret => ret.channelId === listdoc)
        const baseData = sqlResult.find(ret => ret.channelId === listdoc && Moment(ret.discriminant).isSame(element) )
        cbDocs.push({
          name: 'FakeShopStatusByChannel',
          FakeShopStatusByChannel_ChannelId: listdoc,
          FakeShopStatusByChannel_ChannelName: extdoc ? extdoc.channelName : 'Undefined',
          FakeShopStatusByChannel_ShopCount: baseData? baseData.shopCount : 0,
          FakeShopStatusByChannel_DiscriminantTime: element
        })
      }
    })
  }
  return cbDocs
}

export default {
  getFakeCount,
  getTotalCount,
  getByChannel
}
