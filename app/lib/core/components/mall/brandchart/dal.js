import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM brandmall WHERE result = 0 AND `
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM brandmall WHERE `
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, result, count(DISTINCT shopId) AS shopCount FROM brandmall WHERE '
  sql += `category = ${params.category} AND channelId IS NOT NULL AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY channelId, result ORDER BY channelId ASC, result DESC`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(element.channelId))
      cbDocs.push({
        name: 'FakeShopStatusByChannel',
        FakeShopStatusByChannel_ChannelId: element.channelId,
        FakeShopStatusByChannel_ChannelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeShopStatusByChannel_ShopCount: element.shopCount,
        FakeShopStatusByChannel_Result: element.result
      })
    })
  }
  return cbDocs
}

const getByDay = async (params) => {
  let sql = 'SELECT channelId, discriminant, count(shopId) AS shopCount FROM brandmall WHERE '
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY channelId, discriminant ORDER BY discriminant ASC`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(element.channelId))
      cbDocs.push({
        name: 'FakeShopStatusByChannel',
        FakeShopStatusByChannel_ChannelId: element.channelId,
        FakeShopStatusByChannel_ChannelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeShopStatusByChannel_ShopCount: element.shopCount,
        FakeShopStatusByChannel_DiscriminantTime: element.discriminant
      })
    })
  }
  return cbDocs
}

export default {
  getFakeCount,
  getTotalCount,
  getByChannel,
  getByDay
}
