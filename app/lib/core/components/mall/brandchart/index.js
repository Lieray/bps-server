import service from './dal'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 1
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 1
  return data
}

const dashboardChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

export default {
  dashboardCount,
  dashboardTotal,
  dashboardChannel
}
