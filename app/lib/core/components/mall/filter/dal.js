import { dataBases } from '../../../data'

const getCount = async (params) => {
  let sql = `SELECT COUNT(DISTINCT shopId) AS FakeShopCount FROM ( 
    SELECT shopId FROM itemfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByStatus = async (params) => {
  let sql = 'SELECT COUNT(DISTINCT c.shopId) AS FakeShopCount, c.RightsProtectionStatus FROM ( '
  sql += 'SELECT a.shopId, ifnull(b.`status`,1) AS RightsProtectionStatus FROM itemfilter a '
  sql += 'LEFT JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE '
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `
  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `
  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += ') c GROUP BY c.RightsProtectionStatus'
  const sqlResult = await dataBases(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId FROM itemfilter WHERE '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `projectId =  ${params.project} `
  sql += 'GROUP BY channelId ORDER BY channelId '
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const sqlDocs = []
    sqlResult.forEach(element => {
      sqlDocs.push(element.channelId)
    })
    const particulars = `SELECT ChannelId as channelId, ChannelName AS channelName FROM channellist 
                        WHERE channelId IN (${sqlDocs.toString()})`
    const viewResult = await dataBases(particulars)
    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.channelId === element.channelId)
      cbDocs.push({
        name: 'ChannelList',
        ChannelId: element.channelId,
        ChannelName: extdoc ? extdoc.channelName : 'Undefined'
      })
    })
  }
  return cbDocs
}

const getByBoth = async (params) => {
  const sql = `SELECT shopId FROM itemfilter WHERE projectId =  ${params.project} AND shopId IN (${params.id}) GROUP BY shopId`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM itemfilter 
            WHERE projectId =  ${params.project} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getLastDate,
  getCount,
  getByStatus,
  getByChannel,
  getByBoth
}
