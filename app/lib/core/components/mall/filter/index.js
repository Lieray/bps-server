import service from './dal'
import moment from 'moment'

const filterCount = async (params) => {
  const data = await service.getCount(params)
  return data
}

const filterStatus = async (params) => {
  const data = await service.getByStatus(params)
  return data
}

const filterChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

const getByBoth = async (params) => {
  const data = await service.getByBoth(params)
  return data
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: moment().endOf('day').toDate()
  }
  const data = await service.getLastDate(params)
  if (data.length && moment(data[0].discriminant)) {
    defaultDate.end = moment(data[0].discriminant).endOf('day').toDate()
  }
  defaultDate.start = moment(defaultDate.end).subtract(6, 'days').startOf('day').toDate()
  return defaultDate
}

export default {
  getLastDate,
  getByBoth,
  filterCount,
  filterStatus,
  filterChannel
}
