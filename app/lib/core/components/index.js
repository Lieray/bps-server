/**
 * 配置可用模块
 */

import view from './view'
import item from './item'
import mall from './mall'
import right from './right'
import pack from './package'
import price from './price'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

export default {
  view,
  item,
  mall,
  right,
  package: pack,
  price
}
