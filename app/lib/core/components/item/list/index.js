import service from './dal'

const pageCount = async (params) => {
  const data = await service.getFakeCount(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}
const getCount = async (params) => {
  const data = await service.getCount(params)
  return data
}
const getList = async (params) => {
  const data = await service.getList(params)
  return data
}

export default {
  pageCount,
  pageList,
  getCount,
  getList
}
