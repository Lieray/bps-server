import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM itemfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = `SELECT * FROM itemfilter WHERE projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  if (params.order % 10) sql += 'ORDER BY discriminant ASC, '
  else sql += 'ORDER BY discriminant DESC, '
  if (params.order > 9) sql += '`level` ASC '
  else sql += '`level` DESC '

  sql += `LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brand)
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })
    const brandSql = 'SELECT brandId, brandName FROM brandlist WHERE brandId IN ' +
                      `(${[...brandDocs].toString()})`

    const channelSql = `SELECT channelId as ChannelId, channelName as ChannelName FROM channellist 
                        WHERE channelId IN (${[...channelDocs].toString()})`

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`

    const statusInfo = `SELECT * FROM product WHERE resultId IN (${retDocs.toString()})`

    const viewResult = await dataBases(particulars)
    const brandResult = await dataBases(brandSql)
    const channelResult = await dataBases(channelSql)
    const statusResult = await dataBases(statusInfo)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.ResultId === element.resultId) || {}
      const branddoc = brandResult.find(ret => ret.brandId === element.brand)
      const brandCheck = brandResult.find(ret => ret.brandId === element.brandId)
      const channeldoc = channelResult.find(ret => ret.ChannelId === element.channelId)
      const statusdoc = statusResult.find(ret => ret.resultId === element.resultId) || {}

      const basedoc = {
        ResultId: element.resultId,
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        BrandNameList: branddoc? branddoc.brandName : null,
        BrandCheckList: brandCheck? brandCheck.name : null,
        ConfidenceLevelBucket: element.level,
        RightsProtectionStatus: Number.parseInt(element.status),
        RightsProtectionTime: element.createAt,
        RightsProtectionStartTime: statusdoc ? statusdoc.createAt : null
      }
      cbDocs.push(Object.assign(extdoc, branddoc, channeldoc, basedoc))
    })
  }
  return cbDocs
}

const getCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM listfilter WHERE '
  sql += `projectId = ${params.project} `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getList = async (params) => {
  let sql = `SELECT * FROM itemfilter WHERE projectId = ${params.project} `
  sql += 'ORDER BY discriminant ASC '
  sql += `LIMIT ${params.skip}, ${params.size}`

  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brand)
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })
    const brandSql = 'SELECT brandId, brandName FROM brandlist WHERE brandId IN ' +
                      `(${[...brandDocs].toString()})`

    const channelSql = `SELECT channelId as ChannelId, channelName as ChannelName FROM channellist 
                        WHERE channelId IN (${[...channelDocs].toString()})`

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`
    const statusInfo = `SELECT * FROM product WHERE resultId IN (${retDocs.toString()})`

    const viewResult = await dataBases(particulars)
    const brandResult = await dataBases(brandSql)
    const channelResult = await dataBases(channelSql)
    const statusResult = await dataBases(statusInfo)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.ResultId === element.resultId) || {}
      const branddoc = brandResult.find(ret => ret.brandId === element.brand)
      const brandCheck = brandResult.find(ret => ret.brandId === element.brandId)
      const channeldoc = channelResult.find(ret => ret.ChannelId === element.channelId)
      const statusdoc = statusResult.find(ret => ret.resultId === element.resultId) || {}

      const basedoc = {
        ResultId: element.resultId,
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        BrandNameList: branddoc.brandName,
        BrandCheckList: brandCheck.name,
        ConfidenceLevelBucket: element.level,
        RightsProtectionStatus: element.status,
        RightsProtectionTime: element.createAt,
        RightsProtectionStartTime: statusdoc ? statusdoc.createAt : null
      }
      cbDocs.push(Object.assign(extdoc, branddoc, channeldoc, basedoc))
    })
  }
  return cbDocs
}

const getLog = async (params) => {
  let sql = `SELECT * FROM datalog ORDER BY createdAt DESC LIMIT 1 `
  const sqlResult = await dataBases(sql)

  return sqlResult
}

const addLog = async (params) => {
  let sql = 'CALL `anti_counterfeiting_test`.`view_data_update`() '
  const sqlResult = await dataView(sql)

  return sqlResult
}

export default {
  getFakeList,
  getFakeCount,
  getCount,
  getList,
  getLog,
  addLog
}
