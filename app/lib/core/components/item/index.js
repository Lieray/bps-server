/**
 * 配置可用模块
 */
import chart from './chart'
import filter from './filter'
import list from './list'
import brandchart from './brandchart'
import brandfilter from './brandfilter'
import brandlist from './brandlist'

export default {
  chart,
  filter,
  list,
  brandchart,
  brandfilter,
  brandlist
}
