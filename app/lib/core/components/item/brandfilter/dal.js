import { dataBases } from '../../../data'

const getByRPStatus = async (params) => {
  let sql = 'SELECT ifnull(`p`.`status`,(1 - `b`.`result`)) AS RightsProtectionStatus, '
  sql += `"CountByRightsProtectionStatus" AS Name, count(0) AS CountNum FROM branditem b
          LEFT JOIN product p ON b.resultId = p.resultId WHERE `
  sql += `b.category = ${params.category} AND `
  if (params.brand) sql += `b.topId IN (${params.brand}) AND `
  if (params.rpstatus) sql += 'p.`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `b.channelId = ${params.channel} AND `
  if (params.result) sql += `b.result = ${params.result} AND `
  if (params.search) sql += `b.description like "%${params.search}%" AND `
  if (params.level) sql += `ABS(b.score) > ${params.minscore} AND b.score <= ${params.maxscore} AND `
  sql += `b.discriminant >= "${params.start}" AND b.discriminant <= "${params.end}" 
          GROUP BY RightsProtectionStatus`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId FROM brandmall WHERE '
  sql += `category = ${params.category} `
  if (params.brand) sql += `AND topId IN (${params.brand}) `
  sql += 'GROUP BY ChannelID ORDER BY ChannelID '
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const sqlDocs = []
    sqlResult.forEach(element => {
      sqlDocs.push(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${sqlDocs.toString()})`
    const viewResult = await dataBases(particulars)
    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(element.channelId))
      cbDocs.push({
        name: 'ChannelList',
        ChannelId: element.channelId,
        ChannelName: extdoc ? extdoc.channelName : 'Undefined'
      })
    })
  }
  return cbDocs
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM branditem 
            WHERE category = ${params.category} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getByRPStatus,
  getByChannel,
  getLastDate
}
