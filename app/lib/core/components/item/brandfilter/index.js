import service from './dal'
import moment from 'moment'

const filterStatus = async (params) => {
  const data = await service.getByRPStatus(params)
  return data
}
const filterChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: moment().endOf('day').toDate()
  }
  const data = await service.getLastDate(params)
  if (data.length && moment(data[0].discriminant)) {
    defaultDate.end = moment(data[0].discriminant).endOf('day').toDate()
  }
  defaultDate.start = moment(defaultDate.end).subtract(6, 'days').startOf('day').toDate()
  return defaultDate
}

export default {
  filterStatus,
  filterChannel,
  getLastDate
}
