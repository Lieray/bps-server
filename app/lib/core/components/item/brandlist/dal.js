import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM branditem b LEFT JOIN product p ON b.resultId = p.resultId WHERE '
  sql += `b.category = ${params.category} AND `
  if (params.brand) sql += `b.topId IN (${params.brand}) AND `
  if (params.rpstatus) sql += 'p.`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `b.channelId = ${params.channel} AND `
  if (params.result) sql += `b.result = ${params.result} AND `
  if (params.search) sql += `b.description like "%${params.search}%" AND `
  if (params.level) sql += `ABS(b.score) > ${params.minscore} AND b.score <= ${params.maxscore} AND `
  sql += `b.discriminant >= "${params.start}" AND b.discriminant <= "${params.end}" `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = 'SELECT b.*, ifnull(`p`.`status`,(1 - `b`.`result`)) AS `status`, p.createBy FROM branditem b LEFT JOIN product p '

  sql += `ON b.resultId = p.resultId WHERE b.category = ${params.category} AND `
  if (params.brand) sql += `b.topId IN (${params.brand}) AND `
  if (params.rpstatus) sql += 'p.`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `b.channelId = ${params.channel} AND `
  if (params.result) sql += `b.result = ${params.result} AND `
  if (params.search) sql += `b.description like "%${params.search}%" AND `
  if (params.level) sql += `ABS(b.score) > ${params.minscore} AND b.score <= ${params.maxscore} AND `
  sql += `b.discriminant >= "${params.start}" AND b.discriminant <= "${params.end}" `
  if (params.order % 10) sql += 'ORDER BY b.discriminant ASC, '
  else sql += 'ORDER BY b.discriminant DESC, '
  if (params.order > 9) sql += 'b.score ASC '
  else sql += 'b.score DESC '
  sql += `LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })
    const brandSql = 'SELECT brandId, brandName FROM brandlist WHERE brandId IN ' +
                      `(${[...brandDocs].toString()})`

    const channelSql = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...channelDocs].toString()})`

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`

    const viewResult = await dataBases(particulars)
    const brandResult = await dataBases(brandSql)
    const channelResult = await dataBases(channelSql)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.ResultId) === Number.parseInt(element.resultId)) || {}
      const branddoc = brandResult.find(ret => Number.parseInt(ret.brandId) === Number.parseInt(element.brandId))
      const channeldoc = channelResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(element.channelId))
      const basedoc = {
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        BrandNameList: branddoc.brandName,
        BrandCheckList: branddoc.brandName,
        ChannelId: channeldoc.channelId,
        ChannelName: channeldoc.channelName,
        RightsProtectionStatus: element.status,
        RightsProtectionTime: element.createAt
      }
      cbDocs.push(Object.assign(extdoc, basedoc))
    })
  }
  return cbDocs
}

export default {
  getFakeList,
  getFakeCount
}
