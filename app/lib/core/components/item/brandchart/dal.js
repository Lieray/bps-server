import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT count(0) AS FakeProductCount 
            FROM brandmall WHERE result = 0 AND `
  /* 品牌限定为指定的30个，不再有brandId
  if (params.category) sql += `categoryId = ${params.category} AND `
  if (params.brand) sql += `brandId IN (${params.brand}) AND `
  else if (params.ext) sql += `brandId NOT IN (${params.ext}) AND `
  */
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM brandmall WHERE '
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getDailyCount = async (params) => {
  let sql = `SELECT 'FakeProductValueTrade' AS name, discriminant AS FakeProductValueTradeData, 
              count(0) AS FakeProductValueTradeValue FROM brandmall WHERE result = 0 AND `
  sql += `category = ${params.category} AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY discriminant ORDER BY discriminant ASC`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByBrand = async (params) => {
  let sql = 'SELECT topId, count(0) AS brandCount FROM brandmall '
  sql += `WHERE result = 0 AND category = ${params.category} AND 
            discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += 'GROUP BY topId ORDER BY brandCount DESC'
  const sqlResult = await dataBases(sql)

  return sqlResult
}

const getByTotal = async (params) => {
  let sql = 'SELECT topId, count(0) AS brandCount, COUNT(IF(result = 0,1,null)) AS fakenum '
  sql += `FROM brandmall WHERE category = ${params.category} AND 
            discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += 'GROUP BY topId ORDER BY brandCount DESC'
  const sqlResult = await dataBases(sql)

  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, result, count(0) as channelCount FROM brandmall WHERE '
  sql += `category = ${params.category} AND channelId IS NOT NULL AND `
  if (params.brand) sql += `topId IN (${params.brand}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY channelId, result `
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    for (const keyId of sqlDocs.keys()) {
      const extdoc = viewResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(keyId))
      const topdoc = sqlResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(keyId) && Number.parseInt(ret.result) === 1)
      const btmdoc = sqlResult.find(ret => Number.parseInt(ret.channelId) === Number.parseInt(keyId) && Number.parseInt(ret.result) === 0)
      // 实际数据应该按照channelId来进一步分组
      cbDocs.push({
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 1,
        FakeProductStatusByChannel_ProductCount: topdoc ? topdoc.channelCount : 0
      }, {
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 0,
        FakeProductStatusByChannel_ProductCount: btmdoc ? btmdoc.channelCount : 0
      })
    }
  }
  return cbDocs
}

export default {
  getFakeCount,
  getTotalCount,
  getDailyCount,
  getByBrand,
  getByTotal,
  getByChannel
}
