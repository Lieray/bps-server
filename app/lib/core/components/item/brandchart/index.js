import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 1
  return data
}

const dashboardDaily = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)

  const data = await service.getDailyCount(params)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeProductValueTrade',
      FakeProductValueTradeData: day.format('YYYY-MM-DD'),
      FakeProductValueTradeValue: 0
    }
    const doc = data.find(t => moment(t.FakeProductValueTradeData).isSame(defaultDoc.FakeProductValueTradeData, 'day'))
    if (doc) formatData.FakeProductValueTradeValue = doc.FakeProductValueTradeValue
    formatData.push(defaultDoc)
  }
  return formatData
}

const dashboardChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

const dashboardBrand = async (params) => {
  const data = await service.getByBrand(params)
  return data
}

const dashboardTop = async (params) => {
  const data = await service.getByTotal(params)
  return data
}

export default {
  dashboardCount,
  dashboardTotal,
  dashboardDaily,
  dashboardBrand,
  dashboardTop,
  dashboardChannel,
}
