import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT count(0) AS FakeProductCount 
            FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getDailyCount = async (params) => {
  let sql = `SELECT 'FakeProductValueTrade' AS name, discriminant AS FakeProductValueTradeData, 
              count(0) AS FakeProductValueTradeValue FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += 'GROUP BY discriminant ORDER BY discriminant ASC'
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, result, count(0) as channelCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" GROUP BY channelId, result`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    for (const keyId of sqlDocs.keys()) {
      const extdoc = viewResult.find(ret => ret.channelId === keyId)
      const topdoc = sqlResult.find(ret => ret.channelId === keyId && ret.result === 1)
      const btmdoc = sqlResult.find(ret => ret.channelId === keyId && ret.result === 0)
      // 实际数据应该按照channelId来进一步分组
      cbDocs.push({
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 1,
        FakeProductStatusByChannel_ProductCount: topdoc ? topdoc.channelCount : 0
      }, {
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 0,
        FakeProductStatusByChannel_ProductCount: btmdoc ? btmdoc.channelCount : 0
      })
    }
  }
  return cbDocs
}

export default {
  getFakeCount,
  getTotalCount,
  getDailyCount,
  getByChannel
}
