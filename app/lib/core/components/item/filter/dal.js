import { dataBases } from '../../../data'

const getByRPStatus = async (params) => {
  let sql = 'SELECT `status` AS RightsProtectionStatus, count(0) AS CountNum, '
  sql += '"CountByRightsProtectionStatus" AS Name FROM itemfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"
          GROUP BY RightsProtectionStatus`
  const sqlResult = await dataBases(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId FROM listfilter WHERE '
  sql += `projectId =  ${params.project} `
  sql += 'GROUP BY channelId ORDER BY channelId'
  const sqlResult = await dataBases(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const sqlDocs = []
    sqlResult.forEach(element => {
      sqlDocs.push(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${sqlDocs.toString()})`
    const viewResult = await dataBases(particulars)
    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.channelId === element.channelId)
      cbDocs.push({
        name: 'ChannelList',
        ChannelId: element.channelId,
        ChannelName: extdoc ? extdoc.channelName : 'Undefined'
      })
    })
  }
  return cbDocs
}

const getByBoth = async (params) => {
  const sql = `SELECT resultId FROM listfilter WHERE projectId =  ${params.project} AND resultId IN (${params.id})`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM listfilter 
            WHERE projectId = ${params.project} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getLastDate,
  getByRPStatus,
  getByChannel,
  getByBoth
}
