/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { dataBases } from '../../../data'

const getByBoth = async (docs) => {
  const sql = `SELECT * FROM policy WHERE mainId = ${docs.mainid} ` +
              'AND `type` = ' + docs.type
  const sqlResult = await dataBases(sql)
  if (sqlResult.length) {
    return {
      memberid: sqlResult[0].id,
      type: sqlResult[0].type
    }
  } else return null
}

export default {
  getByBoth
}
