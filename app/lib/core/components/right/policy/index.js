import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key] || target[key] === 0) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getByBoth = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainid,
    type
  }) => ({
    mainid,
    type
  }))(params)

  const userInfo = await service.getByBoth(query)
  return userInfo
}

export default {
  getByBoth
}
