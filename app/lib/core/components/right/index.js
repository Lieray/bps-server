/**
 * 配置可用模块
 */
import authority from './authority'
import policy from './policy'

export default {
  authority,
  policy
}
