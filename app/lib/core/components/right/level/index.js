import service from './dal'
/*
const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}
*/
const getByID = async (mainid) => {
  if (!Number.isSafeInteger(mainid)) throw new Error('Error happens with the status code: 502, the key is: ID')
  const query = { mainid }
  const userInfo = await service.getByID(query)
  return userInfo
}

export default {
  getByID
}
