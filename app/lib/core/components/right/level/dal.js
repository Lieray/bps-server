/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
// import Sequelize from 'sequelize'
import { dataBases } from '../../../data'

const getByID = async (docs) => {
  const sql = `SELECT * FROM level WHERE mainId = ${docs.mainid} `
  const sqlResult = await dataBases(sql)

  const userDocs = []
  sqlResult.forEach(item => {
    userDocs.push({
      status: item.status,
      type: item.type,
      init: item.init,
      expire: item.expire
    })
  })
  if (userDocs.length) return userDocs[0]
  return userDocs.length
}

export default {
  getByID
}
