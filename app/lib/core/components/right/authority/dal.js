/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */

import { dataBases } from '../../../data'

const getByBoth = async (docs) => {
  const sql = `SELECT * FROM authority WHERE memberId = ${docs.memberid} ` +
              'AND `class` = ' + docs.type
  const sqlResult = await dataBases(sql)

  const userDocs = []
  sqlResult.forEach(element => {
    userDocs.push({
      memberid: element.memberId,
      status: element.status,
      type: element.type,
      class: element.class
    })
  })

  return userDocs
}

export default {
  getByBoth
}
