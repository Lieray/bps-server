import { dataBases } from '../../../data'

const getList = async (params) => {
  let sql =  'SELECT a.id, a.init, a.expire, a.`name`, b.type, b.`name` AS "level", '
  sql += 'c.amount, c.cost, (c.amount - c.cost) AS balance FROM policy a '
  sql += 'LEFT JOIN `level` b ON a.type = b.id LEFT JOIN `stage` c '
  sql += 'ON a.id = c.packageId AND CURDATE() BETWEEN c.init AND c.expire '
  sql += `WHERE a.mainId = ${params.mainId}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getStore = async (params) => {
  // 检查套餐权限
  let sql =  'SELECT b.id, a.`name`,b.`status` AS authority, b.type, a.init,a.expire FROM policy a '
  sql += `LEFT JOIN project b ON a.id = b.packageId WHERE a.mainId = ${params.mainId}`
  const authResult = await dataBases(sql)
  const cbData = []
  authResult.forEach(element => {
    if ((Number.parseInt(element.type) === 1) && ((Number.parseInt(element.authority) & 2) === 2))
    cbData.push({
      id: element.id,
      name: element.name,
      authority: element.authority,
      init: element.init,
      expire: element.expire
    })
  })
  return cbData
}

const getContent = async (params) => {

  // 检查项目状态
  let sql = 'SELECT id,`status`,step FROM project WHERE '
  sql += `packageId = ${params.id} AND type = ${params.type}`
  const sqlResult = await dataBases(sql)
  const cbData = sqlResult[0]

  if(sqlResult.length) {
    // 对于type=1的检查权限，有2的权限再检查是否配置白名单
    if( (Number.parseInt(params.type) === 1) && ((Number.parseInt(params.status) & 2) === 2)) {
      let countSql = 'SELECT COUNT(id) AS "count" FROM '
      countSql += `empower WHERE projectid = ${params.id}`
      const sqlNum = await dataBases(countSql)
      cbData.whitelist = sqlNum[0].count
    }
  }

  return cbData
}

const getProject = async (params) => {

  // 检查项目状态
  let sql = 'SELECT id,`status`,step FROM project WHERE '
  sql += `mainId = ${params.mainId} AND type = ${params.type}`
  const sqlResult = await dataBases(sql)
  const cbData = sqlResult
  return cbData
}

export default {
  getList,
  getStore,
  getContent,
  getProject
}
