import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getList = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainId
  }) => ({
    mainId
  }))(params)
  const dataInfo = await service.getList(query)
  return dataInfo
}

const getStore = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainId
  }) => ({
    mainId
  }))(params)
  const dataInfo = await service.getStore(query)
  return dataInfo
}

const getContent = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    type
  }) => ({
    id,
    type
  }))(params)
  const dataInfo = await service.getContent(query)
  return dataInfo
}

const getProject = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainid,
    type
  }) => ({
    mainId:mainid,
    type
  }))(params)
  const dataInfo = await service.getProject(query)
  return dataInfo
}

export default {
    getList,
    getStore,
    getContent,
    getProject
}
