import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getCount = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainid: mainId
  }) => ({
    mainId
  }))(params)
  const dataInfo = await service.getCount(query)
  return dataInfo
}

const getList = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainid: mainId,
    size
  }) => ({
    mainId,
    size,
    skip:docs.skip || 0
  }))(params)
  const dataInfo = await service.getList(query)
  return dataInfo
}

const getview = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    mainid: mainId
  }) => ({
    id,
    mainId
  }))(params)
  const dataInfo = await service.getview(query)
  return dataInfo
}

const getContent = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    mainid: mainId
  }) => ({
    id,
    mainId
  }))(params)
  const dataInfo = await service.getContent(query)
  return dataInfo
}

const addinfo = async (docs) => {
  const dataInfo = await service.addinfo(docs)
  return dataInfo
}

const editinfo = async (docs) => {
  const dataInfo = await service.editinfo(docs)
  return dataInfo
}

const setinfo = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    mainid: mainId
  }) => ({
    id,
    mainId
  }))(params)
  const dataInfo = await service.setinfo(query)
  return dataInfo
}

export default {
    getList,
    getCount,
    getview,
    getContent,
    addinfo,
    editinfo,
    setinfo
}
