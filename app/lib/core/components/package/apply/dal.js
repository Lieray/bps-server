import { dataBases } from '../../../data'

const getCount = async (params) => {
    let sql = 'SELECT COUNT(id) AS "COUNT" FROM `apply` WHERE `status` > 0 AND '
    sql += `mainId = ${params.mainId} GROUP BY packageId`

    const sqlResult = await dataBases(sql)
    return sqlResult.length? sqlResult[0].COUNT : 0
}

const getList = async (params) => {
    let sql = 'SELECT * FROM `apply` WHERE id IN ( SELECT MAX(id) FROM `apply` WHERE '
    sql += ` mainId = ${params.mainId} GROUP BY packageId ) `
    sql += 'AND `status` > 0 '
    sql += ` LIMIT ${params.skip}, ${params.size}`
    const sqlResult = await dataBases(sql)
    return sqlResult
}

const getContent = async (params) => {
    let sql = 'SELECT * FROM `apply` WHERE `status` > 0 AND '
    sql += `mainId = ${params.mainId} AND packageId = ${params.id} ORDER BY id DESC`
    const sqlResult = await dataBases(sql)
    return sqlResult
}

const getview = async (params) => {
    let sql = 'SELECT * FROM `apply` WHERE '
    sql += `mainId = ${params.mainId} AND id = ${params.id}`

    const sqlResult = await dataBases(sql)
    return sqlResult
}

const addinfo = async (params) => {
    let sql = 'INSERT INTO apply (`mainid`,`packageId`,`name`, `brand`, `shortname`, `nickname`, `series`, `article`, `product`, `alias`, `abbreviation`, `channel`, `status`) VALUES '
    sql += `("${params.mainid}","${params.project}","${params.name}","${params.brand}","${params.shortname}","${params.nickname}","${params.series}","${params.article}","${params.product}","${params.alias}","${params.abbreviation}","${params.channel}",1 )`

    const sqlResult = await dataBases(sql)
    return sqlResult
}

const editinfo = async (params) => {
    let sql = 'UPDATE apply set name ='
    sql += `"${params.name}",brand="${params.brand}",shortname="${params.shortname}",nickname="${params.nickname}",series="${params.series}",article="${params.article}",product="${params.product}",alias="${params.alias}",abbreviation="${params.abbreviation}",channel="${params.channel}" `
    sql += ', `status` = 2 '
    sql += ` WHERE mainId = ${params.mainid} AND id = ${params.id}`
    const sqlResult = await dataBases(sql)
    return sqlResult
}

const setinfo = async (params) => {
    let sql = 'UPDATE apply set `status` = 0 '
    sql += `WHERE mainId = ${params.mainId} AND id = ${params.id}`

    const sqlResult = await dataBases(sql)
    return sqlResult
}

export default {
    getList,
    getCount,
    getContent,
    getview,
    addinfo,
    editinfo,
    setinfo
}
