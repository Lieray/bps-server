/**
 * 配置可用模块
 */
import project from './project'
import content from './content'
import empower from './empower'

export default {
  project,
  content,
  empower
}