import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getCount = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    project,
    mainid: mainId
  }) => ({
    project,
    mainId,
  }))(params)
  const dataInfo = await service.getCount(query)
  return dataInfo
}

const getproject = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    project,
    mainid: mainId
  }) => ({
    project,
    mainId
  }))(params)
  const dataInfo = await service.getproject(query)
  return dataInfo
}

const getList = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    project,
    mainid: mainId,
    size
  }) => ({
    project,
    mainId,
    size,
    skip:docs.skip || 0
  }))(params)
  const dataInfo = await service.getproject(query)
  return dataInfo
}

const addlist = async (docs) => {
  const dataInfo = await service.addList(docs)
  return dataInfo
}

const editlist = async (docs) => {
  const dataInfo = await service.setItem(docs)
  return dataInfo
}

const setlist = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    project,
    mainid: mainId
  }) => ({
    id,
    project,
    mainId
  }))(params)
  const dataInfo = await service.delItem(query)
  return dataInfo
}

export default {
  getCount,
  getproject,
  getList,
  addlist,
  editlist,
  setlist
}
