import { dataBases } from '../../../data'

const getCount = async (params) => {
  let sql = 'SELECT COUNT(*) AS "COUNT" FROM `empower` WHERE status > 0 AND '
  sql += `projectId = ${params.project} AND mainId = ${params.mainId}`
  const sqlResult = await dataBases(sql)
  return sqlResult.length? sqlResult[0].COUNT : 0
}

const getproject = async (params) => {
  let sql = 'SELECT * FROM `empower` WHERE status > 0 AND '
  sql += `projectId = ${params.project} AND mainId = ${params.mainId}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getList = async (params) => {
  let sql = 'SELECT * FROM `empower` WHERE status > 0 AND '
  sql += `projectId = ${params.project} AND mainId = ${params.mainId} 
          LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const addList = async (params) => {
    let sql = 'INSERT INTO empower(`mainId`,`projectId`, `shopId`, `channelId`, `name`, `url`, `desc`, `init`, `expire`) VALUES '
    sql += params
    const sqlResult = await dataBases(sql)
    return sqlResult
}

const setItem = async (params) => {
  let sql = `UPDATE empower set shopId = md5(concat("${params.name}","-","${params.channelId}")), `
  sql += '`name` = "' + params.name + '", url = '
  sql += params.url? `"${params.url}"`: 'null'
  sql += ', `desc` = '
  sql += params.desc? `"${params.desc}"`: 'null'
  sql += ',`init` = '
  sql += params.init? `"${params.init}"`: 'null'
  sql += ',`expire` = '
  sql += params.expire? `"${params.expire}"`: 'null'
  sql += ` WHERE projectId = ${params.project} AND mainId = ${params.mainid} AND id= ${params.id}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const delItem = async (params) => {
    let sql = 'UPDATE empower set `status` = 0 '
    sql += `WHERE projectId = ${params.project} AND mainId = ${params.mainId} AND id= ${params.id}`
    const sqlResult = await dataBases(sql)
    return sqlResult
}

export default {
  getproject,
  getList,
  getCount,
  addList,
  setItem,
  delItem
}
