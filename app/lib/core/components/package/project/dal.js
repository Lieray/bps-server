import { dataBases } from '../../../data'

const getList = async (params) => {
    let sql = 'SELECT * FROM `level` WHERE '
    sql += `mainId = ${params.mainId} `
    const sqlResult = await dataBases(sql)
    return sqlResult
}

const getProject = async (params) => {
  let sql = 'SELECT * FROM `content` WHERE `name` = "project" '
  sql += `AND projectId in (${params.id}) `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getContent = async (params) => {
    let sql = 'SELECT * FROM `content` WHERE '
    sql += `projectId = ${params.id}`
    const sqlResult = await dataBases(sql)
    const retDocs = []
    const retRecord = []
    sqlResult.forEach(element => {
      const rcdIndex = retRecord.findIndex(n => n === element.projectId)
      if (rcdIndex > -1) {
        if (!retDocs[rcdIndex].content[element.name]) {
          retDocs[rcdIndex].content[element.name] = {
            id: [element.id],
            docs: [element.content]
          }
        } else {
          retDocs[rcdIndex].content[element.name].id.push(element.id)
          retDocs[rcdIndex].content[element.name].docs.push(element.content)
        }
      } else {
        retRecord.push(element.projectId)
        const content = {}
        content[element.name] = {
          id: [element.id],
          docs: [element.content]
        }
        retDocs.push({
          projectId: element.projectId,
          projectName: element.projectName,
          projectType: element.projectType,
          content
        })
      }
    });
    return retDocs
}
const getImg = async (params) => {
  let sql = 'SELECT * FROM `content` WHERE `name` = "screenshot" AND '
  sql += `projectId = ${params.id}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const setProject = async (params) => {
  let sql = 'UPDATE `project` SET step = 2 WHERE '
  sql += `packageId = ${params.project} AND mainId = ${params.mainId} `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

  export default {
    getList,
    getImg,
    getProject,
    getContent,
    setProject
  }
