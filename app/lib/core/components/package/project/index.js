import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getList = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainId
  }) => ({
    mainId
  }))(params)
  const dataInfo = await service.getList(query)
  return dataInfo
}

const getContent = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    mainId
  }) => ({
    id,
    mainId
  }))(params)
  const dataInfo = await service.getContent(query)
  return dataInfo
}

const getImg = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id
  }) => ({
    id
  }))(params)
  const dataInfo = await service.getImg(query)
  return dataInfo
}

const getProject = async (docs) => {
  const dataInfo = await service.getProject(docs)
  return dataInfo
}

const setProject = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainid:mainId,
    project
  }) => ({
    mainId,
    project
  }))(params)
  const dataInfo = await service.setProject(query)
  return dataInfo
}

export default {
  getList,
  getImg,
  getContent,
  getProject,
  setProject
}
