import { msClient } from '../../../data'

const getFakeCount = async (params) => {
  const sql = `SELECT COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT a.C_ProductMonitor_ShopId FROM [Monitor_C_ProductMonitor] a 
    LEFT JOIN Monitor_D_ProductMonitorData b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY a.C_ProductMonitor_ShopId ) c`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getTotalCount = async (params) => {
  const sql = `SELECT COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT C_ProductMonitor_ShopId FROM [Monitor_C_ProductMonitor] 
    WHERE C_ProductConfigId IN (${params.project})
    GROUP BY C_ProductMonitor_ShopId ) a`

  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getAuthCount = async (params) => {
  const sql = `SELECT COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT C_ProductMonitor_ShopId FROM [Monitor_C_ProductMonitor_Shop]  
    WHERE C_ProductConfigId IN (${params.project}) AND [IsAuthor] = N'1'
    GROUP BY C_ProductMonitor_ShopId ) a`

  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDailyChannel = async (params) => {
  const sql = `SELECT Channel, dateString, COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT DISTINCT a.C_ProductMonitor_ShopId,a.Channel,
    convert(varchar(10),b.CurrentDate,120) AS dateString
    FROM Monitor_C_ProductMonitor a 
    LEFT JOIN Monitor_D_ProductMonitorData b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    ) c GROUP BY Channel, dateString`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getChannelCount = async (params) => {
  const sql = `SELECT Channel, COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT a.C_ProductMonitor_ShopId,a.Channel FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY C_ProductMonitor_ShopId,Channel ) c GROUP BY Channel`

  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDailyCount = async (params) => {
  const sql = `SELECT dateString, COUNT(C_ProductMonitor_ShopId) AS 'Count' FROM (
    SELECT DISTINCT a.C_ProductMonitor_ShopId,a.Channel,
    convert(varchar(10),b.CurrentDate,120) AS dateString
    FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    ) c GROUP BY dateString`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getTotalItem = async (params) => {
  let sql = `SELECT COUNT( DISTINCT C_ProductMonitor_ShopId) AS 'Count'
  FROM V_Item_Key WHERE C_ProductMonitor_ShopId IN ( SELECT 
  C_ProductMonitor_ShopId FROM Monitor_C_ProductMonitor_Shop WHERE `
  if (params.channel) sql += `Channel = '${params.channel}' AND `
  if (params.authorize) sql += `IsAuthor  = '${params.authorize}' AND `
  if (params.search) sql += `ShopName LIKE '%${params.search}%' AND `
  sql += `C_ProductConfigId IN (${params.project}))`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFakeList = async (params) => {
  let sql = `SELECT * FROM ( SELECT d.ShopName, d.ShopUrl, d.Channel, d.IsAuthor,
    c.*, ROW_NUMBER() OVER(Order by c.${params.order} ) AS 'RowId'
    FROM ( SELECT a.C_ProductMonitor_ShopId AS N'ShopId', 
    COUNT(DISTINCT (CASE WHEN b.[Status] = N'2' THEN  a.C_ProductMonitor_Id  ELSE NULL END)) AS FakeLink,
    COUNT(DISTINCT (CASE WHEN b.[Status] = N'2' THEN  convert(varchar(10),b.[CurrentDate],120)  ELSE NULL END)) AS FakeDay,
    COUNT(CASE WHEN b.[Status] = N'2' THEN  a.C_ProductMonitor_Id  ELSE NULL END) AS FakeCount,
    COUNT(DISTINCT (CASE WHEN b.[Status] = N'2' THEN  a.UniqueKey  ELSE NULL END)) AS FakeItem, 
    '0' AS ReportLink FROM V_Item_Key a
    JOIN Monitor_D_ProductMonitorData b ON
    a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id] 
    AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    WHERE a.C_ProductMonitor_ShopId IN ( SELECT C_ProductMonitor_ShopId
    FROM Monitor_C_ProductMonitor_Shop WHERE `
  if (params.channel) sql += `Channel = '${params.channel}' AND `
  if (params.authorize) sql += `IsAuthor  = '${params.authorize}' AND `
  if (params.search) sql += `ShopName LIKE '%${params.search}%' AND `
  sql += ` C_ProductConfigId IN (${params.project}) ) GROUP BY 
  a.C_ProductMonitor_ShopId ) c JOIN Monitor_C_ProductMonitor_Shop d 
  ON c.[ShopId] = d.[C_ProductMonitor_ShopId]) e
  WHERE RowId BETWEEN ${params.skip + 1} AND ${params.skip + params.size}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const fakeStatistics = async (params) => {
  let sql = `SELECT a.C_ProductMonitor_ShopId AS ShopId,
  COUNT(DISTINCT a.C_ProductMonitor_Id) AS FakeLink,
  COUNT(DISTINCT convert(varchar(10),b.[CurrentDate],120)) AS FakeDay,
  COUNT(a.C_ProductMonitor_Id) AS FakeCount,
  COUNT(DISTINCT a.UniqueKey) AS FakeItem FROM V_Item_Key a 
  JOIN Monitor_D_ProductMonitorData b ON 
  a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id] WHERE
  a.C_ProductMonitor_ShopId IN (${params.id}) AND  b.[Status] = N'2'
  AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
  GROUP BY a.C_ProductMonitor_ShopId`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDetail = async (params) => {
  let sql = `SELECT * FROM (SELECT a.monitorId AS 'MonitorId',
  a.series AS 'Series', a.model AS 'Model', a.type AS 'ConfigValue',
  a.[name] AS ProductName,a.URL, ROW_NUMBER() OVER(Order by a.monitorId ) AS 'RowId',
  b.CurrentPrice,b.FinalPrice,b.MonitorPrice,b.MarginPrice,b.MarginPercent
  FROM V_Item_Value a JOIN Monitor_D_ProductMonitorData_RealTime b
  ON a.[monitorId] = b.[C_ProductMonitor_Id] WHERE `
  sql += `a.[shopId] = '${params.shopId}' ) d `
  // WHERE RowId between ${params.skip + 1} and ${params.skip + params.size}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

export default {
  getFakeCount,
  getAuthCount,
  getTotalCount,
  getDailyCount,
  getChannelCount,
  getDailyChannel,
  getTotalItem,
  getFakeList,
  fakeStatistics,
  getDetail
}