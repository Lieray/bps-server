import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].Count || 0
  return data
}

const dashboardAuth = async (params) => {
  const result = await service.getAuthCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].Count || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'ShopCount',
    ShopCount: 1
  }
  if (result.length) data.ShopCount = result[0].Count || 1
  return data
}

const dashboardDaily = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.last || params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  const data = await service.getDailyCount(params)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeData',
      FakeData: day.format('YYYY-MM-DD'),
      FakeValue: 0
    }
    const doc = data.find(t => moment(t.dateString).isSame(defaultDoc.FakeData, 'day'))
    if (doc) defaultDoc.FakeValue = doc.Count
    formatData.push(defaultDoc)
  }
  return formatData
}

const dashboardChannel = async (params) => {
  const data = await service.getChannelCount(params)
  return data
}

const dailyChannel = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.last || params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  const data = await service.getDailyChannel(params)
  const formatData = {
    xdatetime:[],
    yvalue:[]
  }
  const channelDocs = new Set()
  data.forEach(element => {
    channelDocs.add(element.Channel)
  });
  for(let item of channelDocs.keys()) {
    formatData.yvalue.push({
      name: item,
      data:[]
    })
  }

  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    formatData.xdatetime.push(day.format('YYYY-MM-DD'))
    const doc = data.filter(t => moment(t.dateString).isSame(day, 'day'))

    formatData.yvalue.forEach(element => {
      const currentData = doc.find(t => t.Channel === element.name) || {}
      element.data.push(currentData.Count || 0)
    })
  }
  return formatData
}

const pageCount = async (params) => {
  const data = await service.getTotalItem(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}
const fakeStatistics = async (params) => {
  const data = await service.fakeStatistics(params)
  return data
}

const getDetail = async (params) => {
  const data = await service.getDetail(params)
  return data
}

export default {
  dashboardCount,
  dashboardAuth,
  dashboardTotal,
  dashboardDaily,
  dashboardChannel,
  dailyChannel,
  pageCount,
  pageList,
  fakeStatistics,
  getDetail
}