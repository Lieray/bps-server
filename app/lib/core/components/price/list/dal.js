import { msClient } from '../../../data'

const getTotalItem = async (params) => {
  let sql = `SELECT COUNT(UniqueKey) AS 'Count' FROM (
    SELECT UniqueKey FROM V_Item_Key WHERE `
    if (params.channel) sql += `Channel = '${params.channel}' AND `
    if (params.series) sql += `Series = '${params.series}' AND `
    if (params.model) sql += `Model = '${params.model}' AND `
    if (params.search) sql += `ProductName LIKE '%${params.search}%' AND `
  sql += `C_ProductConfigId IN (${params.project}) 
  GROUP BY UniqueKey ) a`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFakeList = async (params) => {
  let sql = `SELECT b.*, c.Img AS 'trend' FROM (
    SELECT MAX(dataId) AS 'dataId',
    ROW_NUMBER() OVER(Order by sku ) AS 'RowId'
    FROM V_Item_Value WHERE `
    if (params.channel) sql += `channel = '${params.channel}' AND `
    if (params.series) sql += `series = '${params.series}' AND `
    if (params.model) sql += `model = '${params.model}' AND `
    if (params.search) sql += `[name] LIKE '%${params.search}%' AND `
    sql += `project IN (${params.project}) GROUP BY sku
    ) a LEFT JOIN V_Item_Value b ON a.dataId = b.dataId
    LEFT JOIN V_Item_Img c ON b.sku = c.UniqueKey
    WHERE a.RowId between ${params.skip + 1} and ${params.skip + params.size}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getInfo = async (params) => {
  let sql = `SELECT TOP 1 * FROM V_Item_Value WHERE sku = '${params.sku}' AND channel = '${params.channel}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getItemConfig = async (params) => {
  let sql = `SELECT C_ProductConfigId AS 'configId', Industry,
  BrandName,Channel,Series,Model,InsertDate,UpdateDate,Class FROM
  Monitor_C_ProductConfig WHERE C_ProductConfigId IN (${params.id})`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getItemCount = async (params) => {
  let sql = `SELECT COUNT(a.C_ProductMonitor_Id) AS 'Count'
	FROM V_Item_Key a LEFT JOIN Monitor_C_ProductMonitor_Shop c
  ON a.[C_ProductMonitor_ShopId] = c.[C_ProductMonitor_ShopId] WHERE `
  if (params.authorize) sql += `c.IsAuthor = '${params.authorize}' AND `
  if (params.search) sql += `c.ShopName LIKE '%${params.search}%' AND `
  sql += `a.UniqueKey = '${params.sku}' AND a.Channel = '${params.channel}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}
const getItemList = async (params) => {
  let sql = `SELECT * FROM (SELECT a.monitorId,a.[name] AS ProductName,
    a.URL,c.ShopName, c.ShopUrl,c.IsAuthor, a.[updateAt] AS UpdateDate,
    ROW_NUMBER() OVER(Order by a.monitorId ) AS 'RowId'
    FROM V_Item_Value a LEFT JOIN Monitor_C_ProductMonitor_Shop c
    ON a.[shopId] = c.[C_ProductMonitor_ShopId] WHERE `
    if (params.authorize) sql += `c.IsAuthor = '${params.authorize}' AND `
    if (params.search) sql += `c.ShopName LIKE '%${params.search}%' AND `
    sql += `a.sku = '${params.sku}' AND a.channel = '${params.channel}'
    )b  WHERE b.RowId BETWEEN ${params.skip + 1} and ${params.skip + params.size}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getItemSet = async (params) => {
  let sql = `SELECT * FROM Monitor_C_ProductMonitor_Price WHERE C_ProductMonitor_Id = ${params.id}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getItemInfo = async (params) => {
  let sql = `SELECT C_ProductMonitor_Id AS 'monitorId',* FROM 
  Monitor_C_ProductMonitor WHERE C_ProductMonitor_Id = ${params.id}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDetail = async (params) => {
  let sql = `SELECT * FROM (SELECT a.C_ProductMonitor_Id AS 'MonitorId',
  a.ProductName,a.URL,c.ShopName, c.ShopUrl,c.Channel, c.IsAuthor,
     (CASE
       WHEN a.[Status] = N'2' THEN 1
       ELSE 0
     END
     ) AS 'Status', ROW_NUMBER() OVER(Order by ${params.order} ) AS 'RowId',
   a.CurrentPrice,a.FinalPrice,a.MonitorPrice,a.MarginPrice,a.MarginPercent,
	  b.statisticsCount, b.statisticsDay
   FROM V_Recent_Data a LEFT JOIN V_30Day b ON a.C_ProductMonitor_Id = b.C_ProductMonitor_Id
	 LEFT JOIN Monitor_C_ProductMonitor_Shop c
   ON a.[C_ProductMonitor_ShopId] = c.[C_ProductMonitor_ShopId] WHERE `
  if (params.authorize) sql += `c.IsAuthor = '${params.authorize}' AND `
  if (params.status) sql += `a.Status = '${params.status}' AND `
  if (params.channel) sql += `c.Channel = '${params.channel}' AND `
  if (params.search) sql += `c.ShopName LIKE '%${params.search}%' AND `
  sql += `a.UniqueKey = '${params.sku}' ) d WHERE RowId
  between ${params.skip + 1} and ${params.skip + params.size}`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

export default {
  getFakeList,
  getTotalItem,
  getInfo,
  getItemConfig,
  getItemCount,
  getItemList,
  getItemInfo,
  getItemSet,
  getDetail
}
