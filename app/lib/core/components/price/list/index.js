import service from './dal'

const pageCount = async (params) => {
  const data = await service.getTotalItem(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}

const pageInfo = async (params) => {
  const data = await service.getInfo(params)
  return data
}

const getItemConfig = async (params) => {
  const data = await service.getItemConfig(params)
  return data
}
const getItemCount = async (params) => {
  const data = await service.getItemCount(params)
  return data
}
const getItemList = async (params) => {
  const data = await service.getItemList(params)
  return data
}
const getItemInfo = async (params) => {
  const data = await service.getItemInfo(params)
  return data
}
const getItemSet = async (params) => {
  const data = await service.getItemSet(params)
  return data
}

const getDetail = async (params) => {
  const data = await service.getDetail(params)
  return data
}

export default {
  pageCount,
  pageList,
  pageInfo,
  getItemConfig,
  getItemCount,
  getItemList,
  getItemInfo,
  getItemSet,
  getDetail
}
