/**
 * 配置可用模块
 */
import chart from './chart'
import filter from './filter'
import list from './list'
import daily from './daily'
import vendor from './vendor'

export default {
  chart,
  filter,
  list,
  daily,
  vendor
}
