import { msClient } from '../../../data'

const getTotalItem = async (params) => {
  let sql = `SELECT COUNT(UniqueKey) AS 'Count' FROM (
    SELECT UniqueKey,Channel FROM V_Item_Key WHERE `
  if (params.channel) sql += `Channel = '${params.channel}' AND `
  if (params.series) sql += `Series = '${params.series}' AND `
  if (params.model) sql += `Model = '${params.model}' AND `
  if (params.search) sql += `ProductName = '${params.search}' AND `
  sql += `C_ProductConfigId IN (${params.project}) 
  GROUP BY UniqueKey,Channel ) a`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getTotalLink = async (params) => {
  let sql = `SELECT COUNT(a.C_ProductConfigId) AS 'Count' FROM
  Monitor_C_ProductMonitor a LEFT JOIN
  Monitor_D_ProductMonitorData_RealTime b ON
  a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id] WHERE
  a.C_ProductConfigId IN (${params.project}) AND a.Isdel = '0' AND `
  if (params.channel) sql += `a.Channel = '${params.channel}' AND `
  if (params.series) sql += `a.Series = '${params.series}' AND `
  if (params.model) sql += `a.Model = '${params.model}' AND `
  if (params.search) sql += `ProductName = '${params.search}' AND `
  sql += `b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFakeLink = async (params) => {
  let sql = `SELECT COUNT(a.C_ProductConfigId) AS 'Count' FROM
  Monitor_C_ProductMonitor a LEFT JOIN
  Monitor_D_ProductMonitorData_RealTime b
  ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
  WHERE a.C_ProductConfigId IN (${params.project}) 
  AND a.Isdel = '0' AND b.Status = 2 AND `
  if (params.channel) sql += `a.Channel = '${params.channel}' AND `
  if (params.series) sql += `a.Series = '${params.series}' AND `
  if (params.model) sql += `a.Model = '${params.model}' AND `
  if (params.search) sql += `ProductName = '${params.search}' AND `
  sql += `b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getByChannel = async (params) => {
  const sql = `SELECT Channel FROM Monitor_C_ProductMonitor
  WHERE C_ProductConfigId IN (${params.project}) GROUP BY Channel`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getBySeries = async (params) => {
  const sql = `SELECT Series FROM Monitor_C_ProductMonitor
  WHERE C_ProductConfigId IN (${params.project}) GROUP BY Series`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getByModel = async (params) => {
  const sql = `SELECT Model FROM Monitor_C_ProductMonitor
  WHERE C_ProductConfigId IN (${params.project}) GROUP BY Model`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getLastDate = async (params) => {
  const sql = `SELECT TOP 1 b.CurrentDate FROM Monitor_C_ProductMonitor a
  LEFT JOIN Monitor_D_ProductMonitorData_RealTime b
  ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
  WHERE a.C_ProductConfigId IN (${params.project}) AND a.Isdel = '0'
  ORDER BY  b.CurrentDate DESC`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFirstDate = async (params) => {
  const sql = `SELECT TOP 1 InsertDate AS 'CurrentDate' FROM Monitor_C_ProductMonitor
  WHERE C_ProductConfigId IN (${params.project}) AND Isdel = '0'
  ORDER BY  InsertDate ASC`
  const sqlResult = await msClient(sql)
  return sqlResult.recordset
}

const getSkuStatus = async (params) => {
  let sql = `SELECT a.[Status], COUNT(DISTINCT a.[C_ProductMonitor_Id])
  AS 'Count' FROM V_Recent_Data a
  LEFT JOIN Monitor_C_ProductMonitor_Shop c
  ON a.[C_ProductMonitor_ShopId] = c.[C_ProductMonitor_ShopId] WHERE `
  if (params.authorize) sql += `c.IsAuthor = '${params.authorize}' AND `
  if (params.channel) sql += `c.Channel = '${params.channel}' AND `
  if (params.search) sql += `c.ShopName LIKE '%${params.search}%' AND `
  sql += `a.UniqueKey = '${params.sku}' GROUP BY a.[Status]`
  const sqlResult = await msClient(sql)
  return sqlResult.recordset
}

const getSkuChannel = async (params) => {
  const sql = `SELECT Channel FROM V_Recent_Data
  WHERE UniqueKey = '${params.sku}' GROUP BY Channel`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFakeStatistics = async (params) => {
  const sql = `SELECT C_ProductMonitor_Id AS 'MonitorId', 
  SUM(CASE 
    WHEN [Status] = N'2' THEN 1
    ELSE 0
  END
  ) AS 'Count',
  COUNT(DISTINCT (
  CASE 
    WHEN [Status] = N'2' THEN convert(varchar(10),CurrentDate,120)
    ELSE NULL
  END
  ) )AS 'Day'
  FROM Monitor_D_ProductMonitorData
  WHERE C_ProductMonitor_Id IN (${params.id}) AND CurrentDate > '${params.last}'
  GROUP BY C_ProductMonitor_Id`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

export default {
  getTotalItem,
  getTotalLink,
  getFakeLink,
  getFirstDate,
  getLastDate,
  getByChannel,
  getBySeries,
  getByModel,
  getSkuStatus,
  getSkuChannel,
  getFakeStatistics
}
