import { msClient } from '../../../data'

const dailyShop = async (params) => {
  const sql = `	SELECT DISTINCT * FROM (
    SELECT b.Channel AS N'平台', b.shopname AS N'店铺',
    (CASE 
      WHEN b.IsAuthor = N'1' THEN N'是'
      ELSE N'否'
    END) AS N'是否授权',
    a.[当天商品链接数],a.[当天异常链接数],a.[当天异常次数],a.[当天最低折扣]
    FROM (
    SELECT [C_ProductMonitor_ShopId],
    COUNT(DISTINCT [C_ProductMonitor_Id]) AS N'当天商品链接数',
    COUNT(DISTINCT (CASE WHEN [Status] = N'2' THEN [C_ProductMonitor_Id] ELSE NULL END)) AS N'当天异常链接数',
    SUM(CASE WHEN [Status] = N'2' THEN 1 ELSE 0 END) AS N'当天异常次数',
    MIN(FinalPrice/MonitorPrice) AS N'当天最低折扣'
    FROM [dbo].[V_Monitor_ShopProduct_MonitorData] 
    WHERE MonitorPrice > 0 AND [C_ProductConfigId] = N'${params.project}'
    AND [CurrentDate] BETWEEN N'${params.start}' AND N'${params.end}'
    GROUP BY [C_ProductMonitor_ShopId]
    ) AS a LEFT JOIN [V_Monitor_ShopProduct_List] AS b ON a.[C_ProductMonitor_ShopId] = b.[C_ProductMonitor_ShopId]
    ) AS c `
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}
  
const dailyInfo = async (params) => {
const sql = `SELECT 
  c.[C_ProductMonitor_Id] AS N'编码',
  d.[Series] AS N'系列',
  d.[ProductName] AS N'产品名称',
  e.[ConfigValue] AS N'参数',
  d.[URL] AS N'产品链接',
  f.[Channel] AS N'渠道',
  f.[ShopName] AS N'店铺名称',
  d.[Model] AS N'型号',
  (CASE 
    WHEN f.[IsAuthor] = N'1' THEN N'是'
    ELSE N'否'
  END) AS N'授权',
  g.[CurrentPrice] AS N'页面价格',
  g.[FinalPrice] AS N'券后价格',
  g.[MonitorPrice] AS N'检测价格',
  g.[MarginPrice] AS N'价差',
  g.[MarginPercent] AS N'价差百分比',
  CONVERT(varchar(100), g.[CurrentDate], 120) AS N'最近低价时间',
  c.[近期异常次数],c.[近期异常天数]
  FROM (
    SELECT 
  a.[C_ProductMonitor_Id], a.[C_ProductMonitor_ShopId],
      COUNT(CASE WHEN b.[Status] = N'2' THEN b.[C_ProductMonitor_Id] ELSE NULL END) AS N'近期异常次数',
      COUNT(DISTINCT (CASE WHEN b.[Status] = N'2' THEN convert(varchar(10),b.[CurrentDate],120)  ELSE NULL END)) AS N'近期异常天数'
  FROM 
  [dbo].[Monitor_C_ProductMonitor] a
  LEFT JOIN
  [dbo].[Monitor_D_ProductMonitorData] b
  ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
  WHERE a.[C_ProductConfigId] = N'${params.project}'
  AND b.[CurrentDate] BETWEEN N'${params.start}' AND N'${params.end}'
  GROUP BY a.[C_ProductMonitor_Id], a.[C_ProductMonitor_ShopId]
  ) c LEFT JOIN [dbo].[Monitor_C_ProductMonitor] d ON c.[C_ProductMonitor_Id] = d.[C_ProductMonitor_Id]
  LEFT JOIN [dbo].[Monitor_C_ProductMonitor_Ext] e ON c.[C_ProductMonitor_Id] = e.[C_ProductMonitor_Id]
  LEFT JOIN [dbo].[Monitor_C_ProductMonitor_Shop] f ON c.[C_ProductMonitor_ShopId] = f.[C_ProductMonitor_ShopId]
  LEFT JOIN [dbo].[Monitor_D_ProductMonitorData_RealTime] g ON c.[C_ProductMonitor_Id] = g.[C_ProductMonitor_Id]`
const sqlResult = await msClient(sql)
return  sqlResult.recordset
}

export default {
  dailyShop,
  dailyInfo
}
