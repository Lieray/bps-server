import { msClient } from '../../../data'

const getFakeCount = async (params) => {
  const sql = `SELECT COUNT(UniqueKey) AS 'Count' FROM (
    SELECT a.UniqueKey,a.Channel FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData_RealTime b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY a.UniqueKey,a.Channel ) c`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getFakeLink = async (params) => {
  const sql = `select COUNT(C_ProductMonitor_Id) AS N'Count' from V_Daily_Count
  where C_ProductMonitor_Id in
  (select C_ProductMonitor_Id from Monitor_C_ProductMonitor where C_ProductConfigId IN (${params.project}))
  and Count > 0
  and dateString BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getTotalCount = async (params) => {
  const sql = `SELECT COUNT(UniqueKey) AS 'Count' FROM (
    SELECT UniqueKey,Channel FROM V_Item_Key 
    WHERE C_ProductConfigId IN (${params.project})
    GROUP BY UniqueKey,Channel ) a`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getTotalLink = async (params) => {
  const sql = `select COUNT(C_ProductMonitor_Id) AS N'Count' from V_Daily_Count
  where C_ProductMonitor_Id in
  (select C_ProductMonitor_Id from Monitor_C_ProductMonitor where C_ProductConfigId IN (${params.project}))
  and dateString BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getByChannel = async (params) => {
  const sql = `SELECT Channel, COUNT(UniqueKey) AS 'Count' FROM (
    SELECT a.UniqueKey,a.Channel FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData_RealTime b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY UniqueKey,Channel ) c GROUP BY Channel`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getChannelLink = async (params) => {
  const sql = `SELECT Channel, COUNT(C_ProductMonitor_Id) AS 'Count' FROM (
    SELECT a.C_ProductMonitor_Id,a.Channel, b.dateString FROM Monitor_C_ProductMonitor a 
    LEFT JOIN V_Daily_Count b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Count > 0 AND b.dateString BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY a.C_ProductMonitor_Id,a.Channel, b.dateString ) c GROUP BY Channel`
  const sqlResult = await msClient(sql)

  return  sqlResult.recordset
}

const getDailyCount = async (params) => {
  const sql = `SELECT dateString, COUNT(UniqueKey) AS 'Count' FROM (
    SELECT DISTINCT a.UniqueKey,a.Channel,
    convert(varchar(10),b.CurrentDate,120) AS dateString
    FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData_RealTime b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.C_ProductConfigId IN (${params.project})
    AND b.Status = 2 AND b.CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    ) c GROUP BY dateString`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDailyLink = async (params) => {
  const sql = `select COUNT(C_ProductMonitor_Id) AS N'Count', dateString from V_Daily_Count
  where C_ProductMonitor_Id in
  (select C_ProductMonitor_Id from Monitor_C_ProductMonitor where C_ProductConfigId IN (${params.project}))
  and Count > 0
  and dateString BETWEEN '${params.start}' AND '${params.end}'
  GROUP BY dateString ORDER BY dateString ASC`
  
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDailyPrice = async (params) => {
  const sql = `SELECT TOP 7 * FROM (
    SELECT TOP 8 dateString, MIN(FinalPrice) AS 'Price' FROM (
    SELECT b.FinalPrice, convert(varchar(10),b.CurrentDate,120) AS 'dateString' FROM V_Item_Key a 
    LEFT JOIN Monitor_D_ProductMonitorData b
    ON a.[C_ProductMonitor_Id] = b.[C_ProductMonitor_Id]
    WHERE a.UniqueKey = '${params.sku}'
    ) c GROUP BY dateString ORDER BY dateString DESC) d ORDER BY dateString ASC`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}
const getDailyImg = async (params) => {
  const sql = `SELECT * FROM [Monitor_C_ProductMonitor_Img] WHERE UniqueKey = '${params.sku}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getCurrentStatistics = async (params) => {
  const sql = `SELECT CurrentDate, CurrentPrice, MonitorPrice FROM
  Monitor_D_ProductMonitorData WHERE C_ProductMonitor_Id = ${params.id}
  AND CurrentDate >= '${params.current}'`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}

const getDailyStatistics = async (params) => {
  const sql = `SELECT TOP 7 * FROM (SELECT TOP 8 dateString,
    MIN(FinalPrice) AS 'CurrentPrice', MAX(MonitorPrice)  AS 'MonitorPrice' FROM (
    SELECT FinalPrice, MonitorPrice,convert(varchar(10),CurrentDate,120) AS dateString
    FROM Monitor_D_ProductMonitorData WHERE C_ProductMonitor_Id = ${params.id}
    ) c GROUP BY dateString ORDER BY dateString DESC) d ORDER BY dateString ASC`
 const sqlResult = await msClient(sql)
 return  sqlResult.recordset
}

const getCurrentDetail = async (params) => {
  const sql = `SELECT TOP 10 * FROM Monitor_D_ProductMonitorData
  WHERE C_ProductMonitor_Id = ${params.id} ORDER BY CurrentDate DESC`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}
const getCurrentLink = async (params) => {
  const sql = `SELECT TOP 10 CONVERT(varchar(100), Created, 120) AS 'Created'
  FROM Monitor_CrawlerTask WHERE C_ProductMonitor_id = ${params.id} 
  AND Created < '${params.time}' ORDER BY Created DESC`
  const sqlResult = await msClient(sql)
  return  sqlResult.recordset
}
export default {
  getFakeLink,
  getTotalLink,
  getFakeCount,
  getTotalCount,
  getDailyCount,
  getByChannel,
  getDailyPrice,
  getDailyImg,
  getCurrentStatistics,
  getDailyStatistics,
  getCurrentDetail,
  getCurrentLink,
  getChannelLink,
  getDailyLink
}
