import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].Count || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].Count || 1
  return data
}

const dashboardDaily = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.last || params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  const data = await service.getDailyLink(params)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeData',
      FakeData: day.format('YYYY-MM-DD'),
      FakeValue: 0
    }
    const doc = data.find(t => moment(t.dateString).isSame(defaultDoc.FakeData, 'day'))
    if (doc) defaultDoc.FakeValue = doc.Count
    formatData.push(defaultDoc)
  }
  return formatData
}

const dashboardChannel = async (params) => {
  const data = await service.getChannelLink(params)
  return data
}

const dashboardResult = async (params) => {
  const data = await service.getByResult(params)
  return data
}

const detailPrice = async (params) => {
  const data = await service.getDailyPrice(params)
  return data
}

const currentStatistics = async (params) => {
  const query = {
    id: params.id,
    current: Moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss')
  }
  const data = await service.getCurrentStatistics(query)
  return data
}

const detailStatistics = async (params) => {
  const data = await service.getDailyStatistics(params)
  return data
}

const currentDetail = async (params) => {
  const data = await service.getCurrentDetail(params)
  return data
}

const currentLink = async (params) => {
  const query = {
    id: params.id,
    time: Moment().subtract(3, 'hours').format('YYYY-MM-DD HH:mm')
  }
  const data = await service.getCurrentLink(query)
  const cdDocs = []
  data.forEach(element => {
    const filepath = Moment(element.Created).format('YYYYMMDD')
    const filename  = Moment(element.Created).format('YYYYMMDDHH')
    cdDocs.push(`/${filepath}/${params.id}/${filename}00.png`)
  })
  return cdDocs
}

export default {
  dashboardCount,
  dashboardTotal,
  dashboardDaily,
  dashboardChannel,
  dashboardResult,
  detailPrice,
  currentStatistics,
  detailStatistics,
  currentDetail,
  currentLink
}
