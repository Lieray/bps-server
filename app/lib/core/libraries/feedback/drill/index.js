import service from './dal'

const getCount = async (docs) => {
  const data = await service.getCount(docs)
  return data
}

const getList = async (docs) => {
  const data = await service.getList(docs)
  return data
}

const addItem = async (docs) => {
  const data = await service.addItem(docs)
  return data
}

const editItem = async (docs) => {
  const data = await service.editItem(docs)
  return data
}

export default {
  getCount,
  getList,
  addItem,
  editItem
}