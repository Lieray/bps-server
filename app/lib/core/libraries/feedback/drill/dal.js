import { dataBases } from '../../../data'

const getCount = async (params) => {
  let sql = 'SELECT * FROM `drill` WHERE type = 12 '
  sql += `AND mainid = ${params.mainid} AND projectId = ${params.project}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getList = async (params) => {
  let sql = 'SELECT * FROM `drill` WHERE type = 12 AND '
  sql += `mainid = ${params.mainid} AND resultId IN (${params.id}) AND projectId = ${params.project}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const addItem = async (params) => {
  let sql = 'INSERT INTO drill (`mainid`,`userId`,`resultId`, `projectId`,`status`, `result`, type) VALUES '
  sql += `(${params.mainid},${params.userid},${params.resultid},${params.project},${params.status},${params.result},12)`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const editItem = async (params) => {
  let sql = 'UPDATE drill SET `status` = '
  sql += `${params.status}, result = ${params.result} `
  sql += ` WHERE resultId = (${params.resultid}) AND projectId = ${params.project}`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getCount,
  getList,
  addItem,
  editItem
}