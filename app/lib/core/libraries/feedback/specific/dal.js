/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { dataAuth, dataView, sqlClient } from '../../../data'

const getByBoth = async (params) => {
  const sql = `SELECT resultId FROM operation WHERE type = ${params.type} 
              AND resultId IN (${params.id}) AND mainid = ${params.mainid}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const subContent = async (params) => {
  const sql = 'INSERT INTO operation(`mainid`,`userId`,`resultId`,`reportId`, `content`, `type`, `createBy`, `operationId`, `projectId`) VALUES ?'
  return new Promise((resolve, reject) => {
    sqlClient.query(sql, [params], (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

// 反馈奖励
const subReward = async (params) => {
  // 这里不区分模块，只要是反馈全都统计
  const sql = `SELECT id FROM operation WHERE type IN (12,22,32) AND mainid = ${params.mainid} AND updateBy IS NULL ORDER BY id ASC`
  const queryDocs = await dataAuth(sql)
  return new Promise((resolve, reject) => {
    if (queryDocs.length > 9) {
      sqlClient.getConnection((error, connection) => {
        if (error) return reject(error)
        connection.beginTransaction(erro => {
          if (erro) {
            connection.release()
            return reject(erro)
          }
          const docs = []
          for (let i = 0; i < 10; i++) { docs.push(queryDocs[i].id) }
          const resql = `UPDATE operation SET updateBy = "${params.recordid}", updateAt = NOW() WHERE updateBy IS NULL AND id in (${docs.toString()})`
          connection.query(resql, [], (err, results) => {
            if (err) {
              connection.rollback()
              connection.release()
              return reject(err)
            }
            if (results.changedRows === docs.length) {
              const bonus = `INSERT INTO recharge SET mainid = ${params.mainid}, mainid = ${params.mainid}, 
                            userId = ${params.userid}, recordId = "${params.recordid}", amount = 1, reason = "bonus"`
              connection.query(bonus, [], (er) => {
                if (er) {
                  connection.rollback()
                  connection.release()
                  return reject(er)
                }
                connection.commit()
                resolve(params)
              })
            } else {
              connection.rollback()
              resolve(params)
            }
          })
        })
      })
    } resolve(params)
  })
}

const compDetail = async (params) => {
  const sql = 'INSERT INTO quota (`recordId`, `mainid`,`resultId`, `type`, `amount`, `reason`) VALUES ?'
  return new Promise((resolve, reject) => {
    sqlClient.query(sql, [params], (err, results) => {
      if (err) reject(err)
      resolve(results)
    })
  })
}

const checkRecord = async (params) => {
  const costsql = `UPDATE stage SET cost = cost + ${params.cost} WHERE projectId = ${params.projectId} AND init <= NOW() AND expire >= NOW() AND amount >= cost + ${params.cost}`
  const sqlResult = await dataAuth(costsql)
  return sqlResult
}

const compRecord = async (sqlArray,queryDocs,addDocs) => {
  return new Promise((resolve, reject) => {
    sqlClient.getConnection((error, connection) => {
      if (error) return reject(error)
      connection.beginTransaction(erro => {
        if (erro) {
          connection.release()
          return reject(erro)
        }
        // 记录扣款动作
        const costsql = sqlArray[0]
        connection.query(costsql, [], (mst) => {
          if (mst) {
            connection.rollback()
            connection.release()
            return reject(mst)
          }

          const opsql = sqlArray[1]
          connection.query(opsql, [queryDocs], (er) => {
            if (er) {
              connection.rollback()
              connection.release()
              return reject(new Error('Error happens with the status code: 406, the key is: id'))
            }
            // 插入状态
            const stausql = sqlArray[2]
            connection.query(stausql, [addDocs], (e) => {
              if (e) {
                connection.rollback()
                connection.release()
                return reject(e)
              }
              connection.commit()
              resolve(sqlArray)
            })
          })
        })
      })
    })
  })
}

const compItem = async (params, queryDocs, addDocs) => {
  const sqlArray = [
    `INSERT INTO consume SET mainid = ${params.mainid}, userId = ${params.userid}, 
    recordId = "${params.recordid}", amount = ${params.cost}, cost = 0`,
    'INSERT INTO operation(`mainid`,`userId`,`resultId`, `content`, `type`, `createBy`, `operationId`) VALUES ?',
    'INSERT INTO product(`mainid`,`userId`,`resultId`, `status`, `createBy`) VALUES ?'
  ]
  return await compRecord(sqlArray,queryDocs,addDocs)
}

const compStore = async (params, queryDocs, addDocs) => {
  const sqlArray = [
    `INSERT INTO consume SET mainid = ${params.mainid}, userId = ${params.userid}, 
    recordId = "${params.recordid}", amount = ${params.cost}, cost = 0`,
    'INSERT INTO operation(`mainid`,`userId`,`resultId`, `content`, `type`, `createBy`, `operationId`) VALUES ?',
    'INSERT INTO shop(`mainid`,`userId`,`shopId`, `status`, `createBy`) VALUES ?'
  ]
  return await compRecord(sqlArray,queryDocs,addDocs)
}

const compMonitor = async (params, queryDocs, addDocs) => {
  const sqlArray = [
    `INSERT INTO consume SET mainid = ${params.mainid}, userId = ${params.userid}, 
    recordId = "${params.recordid}", amount = ${params.cost}, cost = 0`,
    'INSERT INTO operation(`mainid`,`userId`,`resultId`, `content`, `type`, `createBy`, `operationId`) VALUES ?',
    'INSERT INTO digital(`mainid`,`userId`,`dataId`, `status`, `createBy`) VALUES ?'
  ]
  return await compRecord(sqlArray,queryDocs,addDocs)
}

const getCount = async (params) => {
  const costsql = `SELECT COUNT(id) AS num FROM operation WHERE 
  projectId = ${params.project} AND mainId = ${params.mainid} AND type = ${params.type}`
  const sqlResult = await dataAuth(costsql)
  // 计算等级，每级升级总经验为250n*(n+1)
  const rank = {
    level: 1,
    exp: 0,
    next: 500
  }
  if (sqlResult.length && sqlResult[0].num) {
    rank.exp = sqlResult[0].num
    const disc = Math.pow(1,2) + (4*rank.exp/250)
    rank.level = Math.ceil((Math.sqrt(disc)-1)/2)
    rank.next = 250*rank.level*(rank.level+1)
  }
  return rank
}

const changeResult = async (params) => {
  let sql = 'UPDATE listfilter SET `result` = '
  sql += params.result + ',`level` = '
  sql += params.level + ` WHERE resultId IN (${params.resultId})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const countFeedback = async (params) => {
  let sql = 'SELECT COUNT(*) AS "Feedback" FROM `operation` WHERE type = 12'
  sql += ` AND projectId = ${params.project}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const changeStatus = async (params) => {
  let sql = 'UPDATE listfilter SET `status` = 2'
  sql += ` WHERE resultId IN (${params.resultId})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const setFeedback = async (params) => {
  let sql = 'INSERT INTO feedback (`mainid`,`userId`,`resultId`, `projectId`,`result`, type) VALUES '
  sql += params
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const addStatus = async (params) => {
  let sql = 'INSERT INTO shop(`projectId`,`mainId`,`userId`,`shopId`, `status`, `createBy`) VALUES '
  sql += params
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

export default {
  getByBoth,
  subContent,
  subReward,
  compItem,
  compStore,
  compMonitor,
  compDetail,
  checkRecord,
  getCount,
  changeResult,
  changeStatus,
  addStatus,
  setFeedback,
  countFeedback
}
