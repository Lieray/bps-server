import service from './dal'

const getByBoth = async (docs) => {
  const data = await service.getByBoth(docs)
  return data
}

const subContent = async (docs) => {
  const data = await service.subContent(docs)
  return data
}

const subReward = async (docs) => {
  const data = await service.subReward(docs)
  return data
}

const compItem = async (docs, queryDocs, addDocs) => {
  const data = await service.compItem(docs, queryDocs, addDocs)
  return data
}

const compStore = async (docs, queryDocs, addDocs) => {
  const data = await service.compStore(docs, queryDocs, addDocs)
  return data
}

const compMonitor = async (docs, queryDocs, addDocs) => {
  const data = await service.compMonitor(docs, queryDocs, addDocs)
  return data
}

const compDetail = async (docs) => {
  const data = await service.compDetail(docs)
  return data
}

const checkRecord = async (docs) => {
  const data = await service.checkRecord(docs)
  return data
}

const getCount= async (docs) => {
  const data = await service.getCount(docs)
  return data
}

const changeResult= async (docs) => {
  const data = await service.changeResult(docs)
  return data
}

const changeStatus= async (docs) => {
  const data = await service.changeStatus(docs)
  return data
}

const addStatus= async (docs) => {
  const data = await service.addStatus(docs)
  return data
}
const countFeedback= async (docs) => {
  const data = await service.countFeedback(docs)
  return data
}
const setFeedback= async (docs) => {
  const data = await service.setFeedback(docs)
  return data
}

export default {
  getByBoth,
  subContent,
  subReward,
  compItem,
  compStore,
  compMonitor,
  compDetail,
  checkRecord,
  getCount,
  changeResult,
  changeStatus,
  addStatus,
  setFeedback,
  countFeedback
}
