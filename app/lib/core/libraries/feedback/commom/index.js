import service from './dal'

const subContent = async (docs) => {
  const data = await service.subContent(docs)
  return data
}

export default {
  subContent
}
