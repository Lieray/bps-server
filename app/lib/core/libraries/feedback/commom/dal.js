/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const contentDal = Dal('Advice', localDB.db)

const subContent = async (docs) => {
  const data = (({
    userid,
    content
  }) => ({
    createBy: userid,
    content,
    type: 'feedback'
  }))(docs)
  const createResult = await contentDal.add(data)
  return createResult
}

export default {
  subContent
}
