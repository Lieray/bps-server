import service from './dal'

const getOpts = async (id) => {
  const data = await service.getOpts({ id: id || 0 })
  return data
}

const getStatusList = async () => {
  const data = await service.getStatusList()
  return data
}

const getBasisList = async () => {
  const data = await service.getBasisList()
  return data
}

export default {
  getOpts,
  getStatusList,
  getBasisList
}
