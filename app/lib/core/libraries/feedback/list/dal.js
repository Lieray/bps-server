/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { dataAuth } from '../../../data'

const getOpts = async (docs) => {
  let sql = 'SELECT * FROM configuration WHERE `name` = "feedback" AND `index` ='
  sql += ` ${docs.id} `
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const getStatusList = async () => {
  const sql = 'SELECT * FROM configuration WHERE `name` = "rpstatus" '
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const getBasisList = async () => {
  const sql = 'SELECT * FROM configuration WHERE `name` = "basis" '
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

export default {
  getOpts,
  getStatusList,
  getBasisList
}
