/**
 * 配置可用模块
 */
import commom from './commom'
import list from './list'
import specific from './specific'
import drill from './drill'

export default {
  list,
  commom,
  specific,
  drill
}
