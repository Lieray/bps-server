/**
 * 配置可用模块
 */

import information from './information'
import identify from './identify'
import verify from './verify'
import subscribe from './subscribe'
import monitor from './monitor'
import digital from './digital'
import feedback from './feedback'
import commerce from './commerce'
import order from './order'

export default {
information,
identify,
verify,
subscribe,
monitor,
digital,
feedback,
commerce,
order
}
