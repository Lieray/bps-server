
import moment from 'moment'
import nconf from 'nconf'
import { gen } from '../../../utils'

const dateCheck = (docs) => {
  return {
    start: moment(docs.start).format('YYYY-MM-DD'),
    last: docs.last,
    end: moment(docs.end).format('YYYY-MM-DD HH:mm:ss')
  }
}

const genKFToken = async (docs) => {
  const kfInfo = await nconf.get('kfInfo')
  const tokenInfo = gen.md5(docs.username+docs.time+kfInfo.APIKey.toLowerCase())
  return tokenInfo.toLowerCase()
}

export default {
  dateCheck,
  genKFToken
}