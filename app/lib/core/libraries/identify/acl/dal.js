/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { localDB } from '../../../data'
import Sequelize from 'sequelize'

const Dal = localDB.dbquery
const jwtDal = Dal('ACL', localDB.db)

const createJWT = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {String} Email
   * @param {String} Status
   */
  const timeNow = new Date().getTime()
  const data = (({
    uuid,
    ip,
    email,
    userid,
    mainid,
    expire
  }) => ({
    uuid,
    ip,
    email,
    userId: userid,
    mainId: mainid,
    createAt: new Date(timeNow),
    invalidAt: new Date(timeNow + expire * 1000),
    expire
  }))(docs)
  const userInfo = await jwtDal.add(data)
  return { userid: userInfo.UserId }
}

const getByID = async (docs) => {
  const Op = Sequelize.Op
  /**
   *查询数据库内相关字段
   * @param {String} uuid
   * @param {String} ip
   */
  const query = (({
    email,
    uuid
  }) => ({
    email,
    uuid,
    expire: { [Op.gte]: new Date() },
    status: 'active'
  }))(docs)

  const userInfo = await jwtDal.get(query)
  if (userInfo) return { userid: userInfo.UserId, mainid: userInfo.mainId }
  return userInfo
}

const resetJWT = async (docs) => {
  const timeNow = new Date()
  const Op = Sequelize.Op
  const data = {
    invalidAt: new Date(timeNow.getTime() + docs.expire * 1000)
  }
  const query = (({
    email,
    uuid,
    expire
  }) => ({
    email,
    uuid,
    expire,
    invalidAt: { [Op.gte]: new Date() },
    status: 'active'
  }))(docs)
  const jwtinfo = await jwtDal.get(query, data)
  if (new Date(jwtinfo.invalidAt).getTime() - timeNow.getTime() < docs.expire * 500) jwtinfo.update(data)
  return {
    email: jwtinfo.email,
    userid: jwtinfo.userId,
    mainid: jwtinfo.mainId
  }
}

export default {
  getByID,
  createJWT,
  resetJWT
}
