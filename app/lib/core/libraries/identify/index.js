/**
 * 配置可用模块
 */
import acl from './acl'
import password from './password'
import commom from './commom'

export default {
  acl,
  password,
  commom
}
