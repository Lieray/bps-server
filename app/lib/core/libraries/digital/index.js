/**
 * 配置可用模块
 */
import chart from './chart'
import filter from './filter'
import list from './list'

export default {
  chart,
  filter,
  list
}
