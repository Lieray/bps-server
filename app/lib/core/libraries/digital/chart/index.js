import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 1
  return data
}

const dashboardDaily = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)

  const data = await service.getDailyCount(params)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeData',
      FakeData: day.format('YYYY-MM-DD'),
      FakeValue: 0
    }
    const doc = data.find(t => moment(t.discriminant).isSame(defaultDoc.FakeData, 'day'))
    if (doc) defaultDoc.FakeValue = doc.FakeData
    formatData.push(defaultDoc)
  }
  return formatData
}

const dashboardChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

const dashboardResult = async (params) => {
  const data = await service.getByResult(params)
  return data
}

export default {
  dashboardCount,
  dashboardTotal,
  dashboardDaily,
  dashboardChannel,
  dashboardResult
}
