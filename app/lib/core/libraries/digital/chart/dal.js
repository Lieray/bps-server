import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM digitalfilter WHERE result = 0 AND '
  sql += `projectId IN (${params.project}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM digitalfilter WHERE '
  sql += `projectId IN (${params.project}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getDailyCount = async (params) => {
  let sql = `SELECT discriminant, count(0) AS FakeData FROM 
            digitalfilter WHERE result = 0 AND projectId IN (${params.project}) AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY discriminant ORDER BY discriminant ASC`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, result, count(0) as ChannelCount FROM digitalfilter WHERE '
  sql += `projectId IN (${params.project}) AND result = 0 AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY channelId, result `
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      cbDocs.push({
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: element.channelId,
        FakeProductStatusByChannel_DiscriminantResult: 0,
        FakeProductStatusByChannel_ProductCount: element.ChannelCount
      })
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    cbDocs.forEach(element => {
      const extdoc = viewResult.find(ret => ret.channelId === element.FakeProductStatusByChannel_ChanelId)
      element.FakeProductStatusByChannel_ChanelName = extdoc ? extdoc.channelName : 'Undefined'
    })
  }
  return cbDocs
}

const getByResult = async (params) => {
  let sql = 'SELECT channelId, analysis, count(0) as ChannelCount FROM digitalfilter WHERE '
  sql += `projectId IN (${params.project}) AND result = 0 AND `
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
          GROUP BY channelId, analysis ORDER BY channelId `
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      cbDocs.push({
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: element.channelId,
        FakeProductStatusByChannel_AnalysisResult: element.analysis,
        FakeProductStatusByChannel_ProductCount: element.ChannelCount
      })
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    cbDocs.forEach(element => {
      const extdoc = viewResult.find(ret => ret.channelId === element.FakeProductStatusByChannel_ChanelId)
      element.FakeProductStatusByChannel_ChanelName = extdoc ? extdoc.channelName : 'Undefined'
    })
  }
  return cbDocs
}

export default {
  getFakeCount,
  getTotalCount,
  getDailyCount,
  getByChannel,
  getByResult
}
