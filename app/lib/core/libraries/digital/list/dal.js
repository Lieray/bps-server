import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM digitalfilter f LEFT JOIN digital d ON f.dataId = d.dataId WHERE '

  sql += `f.projectId IN (${params.project}) AND `

  if (params.rpstatus) {
    if (Number.parseInt(params.rpstatus) === 1) sql += '`f`.`result` = 0 AND d.dataId IS NULL AND '
    else sql += 'd.`status` = ' + params.rpstatus + ' AND '
  }

  if (params.channel) sql += `f.channelId = ${params.channel} And `
  if (params.type) sql += 'f.`type` = "' + params.type + '" AND '
  if (params.result) sql += `f.result = ${params.result} AND `
  if (params.analysis) sql += `f.analysis = "${params.analysis}" AND `
  if (params.search) sql += `f.title like "%${params.search}%" And `
  if (params.level) sql += 'f.`level` = "' + params.level + '" AND '
  sql += `f.discriminant >= "${params.start}" AND f.discriminant <= "${params.end}" `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = 'SELECT f.*, ifnull(`d`.`status`,(1 - `f`.`result`)) AS `status`, d.createAt '
  sql += 'FROM digitalfilter f LEFT JOIN digital d ON f.dataId = d.dataId WHERE '
  sql += `f.projectId IN (${params.project}) AND `

  if (params.rpstatus) {
    if (Number.parseInt(params.rpstatus) === 1) sql += '`f`.`result` = 0 AND d.dataId IS NULL AND '
    else sql += 'd.`status` = ' + params.rpstatus + ' AND '
  }

  if (params.channel) sql += `f.channelId = ${params.channel} And `
  if (params.type) sql += 'f.`type` = "' + params.type + '" AND '
  if (params.result) sql += `f.result = ${params.result} AND `
  if (params.analysis) sql += `f.analysis = "${params.analysis}" AND `
  if (params.search) sql += `f.title like "%${params.search}%" And `
  if (params.level) sql += 'f.`level` = "' + params.level + '" AND '
  sql += `f.discriminant >= "${params.start}" AND f.discriminant <= "${params.end}" `
  sql += `LIMIT ${params.skip}, ${params.size}`

  const sqlResult = await dataBases(sql)

  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      channelDocs.add(element.channelId)
      retDocs.push(element.dataId)
    })

    const channelSql = `SELECT channelId as ChannelId, channelName as ChannelName FROM channellist 
                        WHERE channelId IN (${[...channelDocs].toString()})`

    const particulars = `SELECT * FROM digitaldata WHERE DataId IN (${retDocs.toString()})`

    const viewResult = await dataBases(particulars)
    const channelResult = await dataBases(channelSql)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.DataID === element.dataId) || {}
      const channeldoc = channelResult.find(ret => ret.ChannelId === element.channelId)
      const basedoc = {
        DataID: element.dataId,
        SalesType: element.type,
        ConfidenceLevel: element.level,
        title: element.title,
        AnalysisResult: element.analysis,
        DiscriminantResult: element.result,
        RightsProtectionStatus: element.status,
        RightsProtectionTime: element.createAt
      }

      cbDocs.push(Object.assign(extdoc, channeldoc, basedoc))
    })
  }
  return cbDocs
}

export default {
  getFakeList,
  getFakeCount
}
