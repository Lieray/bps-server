import service from './dal'

const pageCount = async (params) => {
  const data = await service.getFakeCount(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}

export default {
  pageCount,
  pageList
}
