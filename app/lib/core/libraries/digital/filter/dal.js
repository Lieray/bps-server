import { dataBases } from '../../../data'

const getByRPStatus = async (params) => {
  let sql = 'SELECT ifnull(`d`.`status`,(1 - `f`.`result`)) AS RightsProtectionStatus, '
  sql += `"CountByRightsProtectionStatus" AS Name, count(0) AS CountNum FROM digitalfilter f
          LEFT JOIN digital d ON d.dataId = f.dataId WHERE `
  if (params.channel) sql += `f.channelId = ${params.channel} And `
  if (params.type) sql += 'f.`type` = "' + params.type + '" AND '
  if (params.result) sql += `f.result = ${params.result} AND `
  if (params.analysis) sql += `f.analysis = "${params.analysis}" AND `
  if (params.search) sql += `f.title like "%${params.search}%" And `
  if (params.level) sql += 'f.`level` = "' + params.level + '" AND '
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"
          AND projectId IN (${params.project}) GROUP BY RightsProtectionStatus`
  const sqlResult = await dataBases(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getByChannel = async (params) => {
  const sql = `SELECT channelId FROM digitalfilter 
              WHERE discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
              AND projectId IN (${params.project}) GROUP BY channelId`

  const sqlResult = await dataBases(sql)

  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId as ChannelId, channelName as ChannelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    return viewResult
  }
  return []
}

const getByType = async (params) => {
  let sql = 'SELECT `type` FROM digitalfilter WHERE '
  sql += `discriminant >= "${params.start}" AND discriminant <= "${params.end}"
          AND projectId IN (${params.project}) `
  sql += 'GROUP BY `type`'
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByResult = async (params) => {
  const sql = `SELECT analysis as AnalysisResult FROM digitalfilter 
                WHERE discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
                AND projectId IN (${params.project}) GROUP BY AnalysisResult`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByLevel = async (params) => {
  const sql = `SELECT level as ConfidenceLevel FROM digitalfilter 
                WHERE discriminant >= "${params.start}" AND discriminant <= "${params.end}" 
                AND projectId IN (${params.project}) GROUP BY ConfidenceLevel`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByBoth = async (params) => {
  const sql = `SELECT dataId FROM digitalfilter WHERE projectId IN (${params.project}) AND dataId IN (${params.id})`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM digitalfilter 
            WHERE projectId IN (${params.project}) 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

export default {
  getLastDate,
  getByRPStatus,
  getByChannel,
  getByType,
  getByResult,
  getByLevel,
  getByBoth
}
