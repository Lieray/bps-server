import service from './dal'
import moment from 'moment'

const filterStatus = async (params) => {
  const data = await service.getByRPStatus(params)
  return data
}
const filterChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}
const filterType = async (params) => {
  const data = await service.getByType(params)
  return data
}
const filterResult = async (params) => {
  const data = await service.getByResult(params)
  return data
}
const filterLevel = async (params) => {
  const data = await service.getByLevel(params)
  return data
}
const getByBoth = async (params) => {
  const data = await service.getByBoth(params)
  return data
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: moment().endOf('day')
  }
  const data = await service.getLastDate(params)
  if (data.length && moment(data[0].discriminant)) {
    defaultDate.end = moment(data[0].discriminant).endOf('day')
  }
  defaultDate.start = moment(defaultDate.end).subtract(6, 'days').startOf('day')
  return defaultDate
}

export default {
  getLastDate,
  filterStatus,
  filterChannel,
  filterType,
  filterResult,
  filterLevel,
  getByBoth
}
