/**
 * 配置可用模块
 */
import account from './account'
import baseinfo from './baseinfo'
import notification from './notification'

export default {
  account,
  baseinfo,
  notify: notification
}
