/**
 * services 层纯业务，没有与 http 相关的任何东西
 * 需要数据请调用 dal 中的方法
 * service 则是业务核心，不受controllers 和 dal等业务边界的影响
 */

import service from './dal'
/**
 * 验证所有非空参数
 * @param {object} bodyParams
 */
const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getByEmail = async (email) => {
  if (!email) throw new Error('Error happens with the status code: 502, the key is: email')
  const query = { email }
  const userInfo = await service.getByEmail(query)
  return userInfo
}
const checkEmail = async (email) => {
  if (!email) throw new Error('Error happens with the status code: 502, the key is: email')
  const query = { email, status: 'active' }
  const userInfo = await service.getByEmail(query)
  return userInfo
}

const getByBoth = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    userid
  }) => ({
    email,
    userid
  }))(params)
  const userInfo = await service.getByBoth(query)
  return userInfo
}

export default {
  getByEmail,
  checkEmail,
  getByBoth
}
