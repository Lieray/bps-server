import service from './dal'

const pageFilter = async (params) => {
  const data = await service.getTotal(params)
  return data
}
const pageCount = async (params) => {
  const data = await service.getCount(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getList(params)
  return data
}

const itemSet = async (params) => {
  const data = await service.setItem(params)
  return data
}

export default {
  pageFilter,
  pageCount,
  pageList,
  itemSet
}