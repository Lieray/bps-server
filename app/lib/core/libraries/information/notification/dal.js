import { dataView } from '../../../data'

const getTotal = async (params) => {
  const sql = `SELECT type, COUNT(*) AS "Count" 
  FROM notification WHERE mainId = ${params.mainid} 
  AND isdel = 0 GROUP BY type`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getCount = async (params) => {
  let sql = `SELECT COUNT(id) AS "Count" FROM notification
  WHERE mainId = ${params.mainid} AND isdel = 0 `
  if (params.type) sql += `AND type = ${params.type} `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getList = async (params) => {
  let sql = `SELECT * FROM notification
  WHERE mainId = ${params.mainid} AND isdel = 0 `
  if (params.type) sql += `AND type = ${params.type} `
  sql += `ORDER BY id DESC LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const setItem = async (params) => {
  const sql = `UPDATE notification SET 
  ${params.content} = 1 WHERE id = ${params.id} `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  getTotal,
  getCount,
  getList,
  setItem
}
