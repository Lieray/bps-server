/**
 * services 层纯业务，没有与 http 相关的任何东西
 * 需要数据请调用 dal 中的方法
 * service 则是业务核心，不受controllers 和 dal等业务边界的影响
 */
import service from './dal'
/**
 * 验证所有非空参数
 * @param {object} bodyParams
 */
const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}
const getByID = async (userid) => {
  if (!Number.isSafeInteger(userid)) throw new Error('Error happens with the status code: 502, the key is: userid')
  const query = { userid }
  const userInfo = await service.getByID(query)
  return userInfo
}

const updateInfo = async (docs) => {
  if (!docs.userid) throw new Error('Error happens with the status code: 502, the key is: userid')
  const data = {
    userid: docs.userid
  }
  if (docs.username) data.username = docs.username
  if (docs.company) data.company = docs.company
  let checkInfo = false
  for (const i in data) {
    if (data[i]) checkInfo = true
    else checkInfo = false
  }
  if (!checkInfo) throw new Error('Error happens with the status code: 502, the key is: userInfo')
  await service.updateInfo(data)
  return docs
}

const updatePhone = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    userid,
    phone
  }) => ({
    userid,
    phone
  }))(params)
  await service.updatePhone(query)
  return docs
}

export default {
  getByID,
  updateInfo,
  updatePhone
}
