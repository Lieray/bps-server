/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const infoDal = Dal('Baseinfo', localDB.db)

const getByID = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {Number} userid
   */
  const query = (({
    userid
  }) => ({
    userid: userid
  }))(docs)
  const userInfo = await infoDal.get(query)
  return {
    name: userInfo.username,
    phone: userInfo.telephone,
    company: userInfo.company,
    url: userInfo.url
  }
}
const updateInfo = async (docs) => {
  /**
   *更新数据库内相关字段
   * @param {Number} UserId
   * @param {String} Username
   * @param {String} Company
   */

  const data = {}
  if (docs.username) data.username = docs.username
  if (docs.company) data.company = docs.company
  const query = (({
    userid
  }) => ({
    userid: userid
  }))(docs)
  const updateinfo = await infoDal.put(query, data)
  return updateinfo
}
const updatePhone = async (docs) => {
  /**
   *更新数据库内相关字段
   * @param {Number} userid
   * @param {String} username
   * @param {String} company
   */

  const data = {
    telephone: docs.phone
  }
  const query = (({
    userid
  }) => ({
    userid: userid
  }))(docs)
  const updateinfo = await infoDal.put(query, data)
  return updateinfo
}

export default {
  updateInfo,
  updatePhone,
  getByID
}
