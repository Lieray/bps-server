/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import Sequelize from 'sequelize'
import { localDB } from '../../../data'

const dbModels = localDB.db

const creation = async (docs) => {
  const baseInfo = (({
    phone,
    username,
    company
  }) => ({
    telephone: phone,
    username,
    company
  }))(docs)

  const extInfo = (({
    password,
    salt
  }) => ({
    password,
    salt
  }))(docs)

  const verifiedInfo = (({
    email,
    userPhone,
    activation,
    uuid,
    token,
    expired
  }) => ({
    email,
    telephone: userPhone,
    class: 2,
    type: 1,
    init: activation,
    uuid,
    token,
    expire: expired
  }))(docs)

  const models = await dbModels
  const db = models.db
  const Activation = models.Verification
  const Info = models.Baseinfo
  const Password = models.Password

  return db.transaction(t => {
    return Activation.create(verifiedInfo, { transaction: t })
      .then(doc => {
        baseInfo.infoId = doc.id
        extInfo.infoId = doc.id
        return Password.create(extInfo, { transaction: t })
          .then(_ret => {
            return Info.create(baseInfo, { transaction: t })
          })
      })
  })
}

const verification = async (docs) => {
  const Op = Sequelize.Op

  const query = (({
    uuid,
    token,
    email
  }) => ({
    uuid,
    email,
    class: 2,
    type: 1,
    status: 'unactivated',
    expire: { [Op.gte]: new Date() },
    token
  }))(docs)

  const data = {
    status: 'activated',
    updateTime: new Date()
  }
  const userInfo = (({
    id,
    email
  }) => ({
    infoId: id,
    email,
    status: 'active'
  }))(docs)

  const baseInfo = { userId : 0 }

  const models = await dbModels
  const db = models.db
  const Activation = models.Verification
  const User = models.Account
  const Info = models.Baseinfo
  const Password = models.Password
  
  return db.transaction(t => {
    return Activation.update(data, {
      where: query,
      transaction: t
    }).then(renovation => {
      if (renovation.length && renovation[0]) {
        return User.create(userInfo, { transaction: t })
        .then(ret => {
          baseInfo.userId = ret.id
          return Password.update(baseInfo, {
            where: {infoId: docs.id},
            transaction: t
          })
        })
        .then(_ret => {
          return Info.update(baseInfo, {
            where: {infoId: docs.id},
            transaction: t
          })
        })
        .then(_ret => {
          return User.update({mainId:baseInfo.userId}, {
            where: {id: baseInfo.userId},
            transaction: t
          })
        })
      }
    })
  })
}

export default {
  creation,
  verification
}
