/**
 * services 层纯业务，没有与 http 相关的任何东西
 * 需要数据请调用 dal 中的方法
 * service 则是业务核心，不受controllers 和 dal等业务边界的影响
 */
import service from './dal'

/**
 * 验证所有非空参数
 * @param {object} bodyParams
 */
const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

/**
 * 生成激活信息
 * @param {string} email
 * @param {string} username
 * @param {string} phone
 * @param {string} company
 * @param {string} password
 */
const createInfo = async (docs, tokenInfo) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    username,
    password,
    company
  }) => ({
    email,
    username,
    company,
    phone: docs.phoneStatus ? docs.phone : '',
    userPhone: docs.phone,
    userPassword: password,
    activation: new Date()
  }))(params)
  await service.creation(Object.assign(query, tokenInfo))
  return Object.assign(query, tokenInfo)
}

/**
 * 直接激活符合条件的条目
 * @param {string} uuid
 * @param {string} token
 */
const createUser = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    id,
    email,
    uuid,
    token
  }) => ({
    id,
    uuid,
    email,
    token
  }))(params)

  const userInfo = await service.verification(query)
  return userInfo
}

export default {
  createInfo,
  createUser
}
