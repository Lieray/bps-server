/**
 * 配置可用模块
 */
import activate from './activate'
import phone from './phone'
import email from './email'

export default {
  activate,
  email,
  phone
}
