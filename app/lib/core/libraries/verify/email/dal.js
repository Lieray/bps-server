/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import Sequelize from 'sequelize'
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const verificationDal = Dal('Verification', localDB.db)

const getByEmail = async (docs) => {
  const Op = Sequelize.Op
  /**
   *查询数据库内相关字段
  * @param {String} Email
  */
  const query = (({
    email,
    type
  }) => ({
    email,
    class: 2,
    type,
    expire: { [Op.gte]: new Date() }
  }))(docs)

  const activationInfos = await verificationDal.find(query)
  return activationInfos
}

const checkEmail = async (docs) => {
  /**
   *查询数据库内相关字段
  * @param {String} Email
  */
  const query = (({
    email,
    type
  }) => ({
    email,
    class: 2,
    type
  }))(docs)

  const activationInfos = await verificationDal.find(query)
  return activationInfos
}

const createToken = async (docs) => {
  const data = (({
    email,
    activation,
    type,
    uuid,
    token,
    expired
  }) => ({
    email,
    class: 2,
    type,
    init: activation,
    uuid,
    token,
    expire: expired
  }))(docs)
  if (docs.phone) data.telephone = docs.phone

  const createResult = await verificationDal.add(data)
  return createResult
}

const createMsg = async (docs) => {
  const data = (({
    email,
    activation,
    expired
  }) => ({
    email,
    class: 2,
    type: 0,
    init: activation,
    token: email,
    status: 'used',
    expire: expired
  }))(docs)
  const createResult = await verificationDal.add(data)
  return createResult
}

const getByCode = async (docs) => {
  /**
   *查询数据库内相关字段
  * @param {String} Email
  */
  const query = (({
    uuid,
    token,
    type
  }) => ({
    uuid,
    token,
    class: 2,
    type
  }))(docs)

  const activationInfos = await verificationDal.get(query)
  if (!activationInfos) return activationInfos
  const verifiedInfo = (({
    id,
    email,
    telephone: userPhone,
    init: activation,
    uuid,
    token,
    expire,
    status
  }) => ({
    id,
    email,
    userPhone,
    activation,
    uuid,
    token,
    expire,
    status
  }))(activationInfos)

  return verifiedInfo
}

const useToken = async (docs) => {
  const Op = Sequelize.Op

  const query = (({
    email,
    uuid,
    token
  }) => ({
    email,
    uuid,
    class: 2,
    type: 2,
    expire: { [Op.gte]: new Date() },
    status: 'unactivated',
    token
  }))(docs)
  // 直接对信息进行更新，并返回更新后的信息
  const data = {
    status: 'activated',
    updateTime: new Date()
  }
  const updateinfo = await verificationDal.put(query, data)
  return updateinfo
}

export default {
  getByEmail,
  checkEmail,
  createToken,
  createMsg,
  getByCode,
  useToken
}
