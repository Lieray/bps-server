import { support, gen } from '../../../utils'
import service from './dal'
import nconf from 'nconf'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key] || target[key] === 0) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

/**
 * 通过email查询激活信息
 * @param {string} email
 */
const getByEmail = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    type
  }) => ({
    email,
    type
  }))(params)

  const checkData = await service.getByEmail(query)
  return checkData
}

/**
 * 未激活且已过期则视为不存在
 * @param {Date} expire
 * @param {string} status
 */
const isActivationInProgress = ({ expired, status }) => {
  if (!expired || !status) return false
  if (expired.getTime() < new Date().getTime() && status === 'unactivated') return false
  return true
}

/**
 * 生成验证码信息
 * 1、生成邮件认证码
 * 2、生成申请记录
 */

const createToken = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email
  }) => ({
    email,
    type: 2,
    activation: new Date()
  }))(params)
  const tokenInfo = gen.genToken()
  const tokenConfig = await nconf.get('passwordToken')
  tokenInfo.expired = new Date(query.activation.getTime() + tokenConfig.expiresIn * 1000)

  const result = await service.createToken(Object.assign(query, tokenInfo))
  return result
}

const createMsg = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email
  }) => ({
    email,
    activation: new Date()
  }))(params)
  const applyConfig = await nconf.get('membership')
  query.expired = new Date(query.activation.getTime() + applyConfig.expiresIn * 1000)

  const result = await service.createMsg(query)
  return result
}

/**
 * 验证激活码
 * 未激活且已过期则视为不存在
 * @param {string} uuid
 * @param {string} token
 */
const getByCode = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    uuid,
    token,
    type
  }) => ({
    uuid,
    token,
    type
  }))(params)

  // 检查激活码是否存在
  const checkData = await service.getByCode(query)
  // 未激活且已过期则视为不存在
  // if (checkData && isActivationInProgress({ expired: checkData.expire, status: checkData.status })) return checkData
  return checkData
}

const useToken = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    uuid,
    token
  }) => ({
    email,
    uuid,
    token
  }))(params)
  const updateinfo = await service.useToken(query)
  return updateinfo
}

/**
 * 向指定邮箱发送激活码
 * @param {string} email
 * @param {string} uuid
 * @param {string} activationToken
 */
const activateInfo = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    uuid,
    token
  }) => ({
    email,
    uuid,
    token
  }))(params)
  const checkData = await support.email.sendToken(query)
  return checkData
}
/**
 * 向指定邮箱发送验证码
 * @param {string} email
 * @param {string} uuid
 * @param {string} activationToken
 */
const verifyInfo = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    uuid,
    token
  }) => ({
    email,
    uuid,
    token
  }))(params)
  const checkData = await support.email.resetToken(query)
  return checkData
}

/**
 * 向指定邮箱发送通知
 * @param {string} email
 * @param {string} name
 * @param {string} phone
 * @param {string} level
 * @param {string} demand
 * @param {string} company
 */
const sendMsg = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    name,
    level,
    demand,
    company
  }) => ({
    email,
    name,
    phone: docs.phone || '未填写',
    level,
    demand,
    company
  }))(params)
  const checkData = await support.email.notify(query)
  return checkData
}

export default {
  activateInfo,
  verifyInfo,
  getByEmail,
  createToken,
  createMsg,
  getByCode,
  useToken,
  sendMsg
}
