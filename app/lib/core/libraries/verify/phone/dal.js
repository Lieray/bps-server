/**
 * 数据访问层，只于数据打交到，其它都不管
 * DAL层里不处理错误，在一个专门的对象里面处理错误
 */
import Sequelize from 'sequelize'
import { localDB } from '../../../data'

const Dal = localDB.dbquery
const verificationDal = Dal('Verification', localDB.db)

const getByBoth = async (docs) => {
  /**
   *查询数据库内相关字段
   * @param {String} Email
   * @param {String} Telephone
   * @param {Date} ActivationInitTime
   * @param {String} ActivationType
   */
  const params = (({
    email,
    phone
  }) => ({
    email,
    telephone: phone
  }))(docs)
  const Op = Sequelize.Op
  const query = {
    init: { [Op.gte]: docs.activation },
    class: 1,
    type: docs.type,
    [Op.or]: params
  }
  const activationInfos = await verificationDal.find(query)
  return activationInfos
}

const createToken = async (docs) => {
  const data = (({
    email,
    phone,
    activation,
    type,
    uuid,
    token,
    expired
  }) => ({
    email,
    telephone: phone,
    class: 1,
    type,
    init: activation,
    uuid,
    token,
    expire: expired
  }))(docs)

  const createResult = await verificationDal.add(data)
  return createResult
}

const checkSms = async (docs) => {
  const Op = Sequelize.Op

  const query = (({
    email,
    phone,
    token,
    type
  }) => ({
    email,
    telephone: phone,
    class: 1,
    type,
    token,
    expire: { [Op.gte]: new Date() }
  }))(docs)

  const verificationInfos = await verificationDal.find(query)
  const verificationInfo = verificationInfos.pop() || {}
  return { status: verificationInfo.status }
}

const putSms = async (docs) => {
  const Op = Sequelize.Op

  const query = (({
    email,
    phone,
    token,
    type
  }) => ({
    email,
    telephone: phone,
    class: 1,
    type,
    expire: { [Op.gte]: new Date() },
    status: 'unactivated',
    token
  }))(docs)
  // 直接对信息进行更新，并返回更新后的信息
  const data = {
    status: 'activated',
    updateTime: new Date()
  }
  const updateinfo = await verificationDal.put(query, data)
  return updateinfo
}

const useSms = async (docs) => {
  const Op = Sequelize.Op

  const query = (({
    email,
    phone,
    type
  }) => ({
    email,
    telephone: phone,
    class: 1,
    type,
    expire: { [Op.gte]: new Date() },
    status: 'activated'
  }))(docs)

  // 直接对信息进行更新，并返回更新后的信息
  const data = {
    status: 'used'
  }
  const updateinfo = await verificationDal.put(query, data)
  return updateinfo
}

export default {
  getByBoth,
  createToken,
  checkSms,
  putSms,
  useSms
}
