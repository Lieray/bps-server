import { parseString as xmlparser } from 'xml2js'
import { support, gen } from '../../../utils'
import service from './dal'
import nconf from 'nconf'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const tokenGen = () => {
  return gen.genSMS()
}

/**
 *通过手机号和邮箱获取已存在的信息条数
 * @param {String} email
 * @param {String} phone
 */
const getByBoth = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    phone,
    type
  }) => ({
    email,
    phone,
    type
  }))(params)
  const tokenConfig = await nconf.get('smsToken')
  query.activation = new Date(new Date() - tokenConfig.limitIn * 1000)
  const activationInfos = await service.getByBoth(query)
  const checkInfo = activationInfos.length > tokenConfig.limits
  return checkInfo
}

const createSMS = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    phone,
    type
  }) => ({
    email,
    phone,
    type,
    activation: new Date()
  }))(params)
  const tokenInfo = gen.genSMS()
  const tokenConfig = await nconf.get('smsToken')
  tokenInfo.expired = new Date(query.activation.getTime() + tokenConfig.expiresIn * 1000)

  await service.createToken(Object.assign(query, tokenInfo))
  return tokenInfo
}

/**
 * 查看已激活待使用的验证码
 * @param {String} email
 * @param {String} phone
 * @param {String} token
 */
const checkSms = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    phone,
    token,
    type
  }) => ({
    email,
    phone,
    token,
    type
  }))(params)
  const smsInfo = await service.checkSms(query)
  return smsInfo
}

/**
 * 激活验证码
 * @param {String} email
 * @param {String} phone
 * @param {String} token
 */
const putSms = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    phone,
    token,
    type
  }) => ({
    email,
    phone,
    token,
    type
  }))(params)
  const updateinfo = await service.putSms(query)
  return updateinfo
}

/**
 * 使用验证码
 * @param {String} email
 * @param {String} phone
 */
const useSms = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    email,
    phone,
    type
  }) => ({
    email,
    phone,
    type
  }))(params)
  const updateinfo = await service.useSms(query)
  return updateinfo
}

/**
 * 向指定手机号发送验证码
 * @param {String} phone
 * @param {String} token
 */
const sendSms = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    phone,
    msg
  }) => ({
    phone,
    msg
  }))(params)
  const smsResp = await support.sms.sendSms(query)
  xmlparser(smsResp, { trim: true }, (err, result) => {
    if (err) throw new Error('Error happens with the status code: 502, the key is: xmlparser')
    const jsResult = JSON.parse(JSON.stringify(result))
    return jsResult
  })
}

export default {
  tokenGen,
  getByBoth,
  createSMS,
  checkSms,
  putSms,
  useSms,
  sendSms
}
