import { dataAuth } from '../../../data'

const checkemail = async (params) => {
  const sql = `SELECT notifier, status FROM email
  WHERE userid =  '${params.userid}'
  ORDER BY id DESC LIMIT 1`
  const sqlResult = await dataAuth(sql)
  return  sqlResult
}

const setemail = async (params) => {
  const sql = `INSERT INTO email (userId, mainid, notifier) VALUES 
  ('${params.userid}', '${params.mainid}', '${params.notifier}') ON
  DUPLICATE KEY UPDATE notifier = '${params.notifier}'`
  const sqlResult = await dataAuth(sql)
  return  sqlResult
}

const setstatus = async (params) => {
  const sql = `INSERT INTO email (userId, mainid, status) VALUES 
  ('${params.userid}', '${params.mainid}', '${params.status}') ON
  DUPLICATE KEY UPDATE status = '${params.status}'`
  const sqlResult = await dataAuth(sql)
  return  sqlResult
}

export default {
  checkemail,
  setemail,
  setstatus
}
