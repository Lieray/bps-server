import service from './dal'

const checkemail = async (params) => {
  const data = await service.checkemail(params)
  return data
}

const setemail = async (params) => {
  const data = await service.setemail(params)
  return data
}

const setstatus = async (params) => {
  const data = await service.setstatus(params)
  return data
}

export default {
  checkemail,
  setemail,
  setstatus
}