import service from './dal'

const paramsFormat = (bodyParams) => {
  const paramHandler = {
    get: (target, key) => {
      if (target[key]) return target[key]
      else throw new Error(`Error happens with the status code: 502, the key is: ${key}`)
    },
    set: (target, key, value) => {
      target[key] = value
    }
  }
  const bodyParse = new Proxy(bodyParams, paramHandler)
  return bodyParse
}

const getList = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    mainId
  }) => ({
    mainId
  }))(params)
  const dataInfo = await service.getList(query)
  return dataInfo
}

const getInfo = async (docs) => {
  const params = paramsFormat(docs)
  const query = (({
    subid,
    mainId
  }) => ({
    subid,
    mainId
  }))(params)
  const dataInfo = await service.getInfo(query)
  return dataInfo
}

export default {
    getList,
    getInfo
}
