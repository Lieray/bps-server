import { dataAuth } from '../../../data'

const getList = async (params) => {
  let sql =  'SELECT * FROM subinfo '
  sql += `WHERE mainId = ${params.mainId}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const getInfo = async (params) => {
  let sql =  'SELECT * FROM subinfo '
  sql += `WHERE subid = ${params.subid} AND mainId = ${params.mainId}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

export default {
    getList,
    getInfo
}