/**
 * 配置可用模块
 */
import info from './info'
import notify from './notification'

export default {
    info,
    notify
}