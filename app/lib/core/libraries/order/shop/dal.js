import { dataView, dataAuth } from '../../../data'

const setOrder = async (params) => {
  let sql = `UPDATE shop SET orderId = ${params.id} WHERE shopId = ${params.result}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const getShopByBoth = async (params) => {
  const sql = `SELECT shopId FROM listfilter WHERE projectId =  ${params.project} AND shopId IN (${params.id}) GROUP BY shopId`

  const sqlResult = await dataView(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    const channelDocs = new Set()

    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
      channelDocs.add(element.channelId)
    })

    const particulars = `SELECT * FROM store WHERE shopId IN (${retDocs.toString()})`
    const viewResult = await dataAuth(particulars)

    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.shopId === element.shopId)

      const formatDoc = {
        ShopId: element.shopId,
        ShopName: extdoc? extdoc.shopName : null,
        ShopURL: extdoc? extdoc.shopLink : null,
        ChannelName: extdoc? extdoc.channelName : null,
        ChannelId: extdoc? extdoc.channelId : null,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      }

      cbDocs.push(formatDoc)
    })
  }
  return cbDocs
}

export default {
    setOrder,
    getShopByBoth
}
