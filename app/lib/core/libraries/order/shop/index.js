import { support } from '../../../utils'
import service from './dal'

const sendOrder = async (docs) => {
  for (let doc of docs) {
    const orderString = await support.order.createOrder(doc)
    const orderInfo = JSON.parse(orderString)
    await service.setOrder({id: orderInfo.request.id, result: doc.id})
  }
  return docs
}

const getshopByBoth = async (params) => {
  const data = await service.getShopByBoth(params)
  return data
}
  
export default {
  sendOrder,
  getshopByBoth
}