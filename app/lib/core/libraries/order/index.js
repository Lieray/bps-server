/**
 * 配置可用模块
 */
import item from './item'
import shop from './shop'
import submit from './submit'

export default {
    item,
    shop,
    submit
}
