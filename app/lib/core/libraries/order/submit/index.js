import { support } from '../../../utils'
import service from './dal'

const sendOrder = async (docs) => {
  for (let doc of docs) {
    const orderid = await createOrder(doc)
    await checkTag({id: doc.id, orderid, email: doc.email})

  }
  return docs
}

const createOrder = async (doc) => {
  const orderString = await support.order.createOrder({
    id: doc.id,
    email: doc.email,
    title: doc.title,
    comment: doc.comment
  })
  const orderInfo = JSON.parse(orderString)

  await service.compItem(
    [[
      doc.mainid,
      doc.userid,
      doc.id,
      doc.content,
      31,
      doc.userid,
      doc.id + '@31'
    ]],[[
      doc.mainid,
      doc.userid,
      doc.id,
      31,
      2,
      doc.userid,
      orderInfo.request.id
    ]])
  await service.setStatus({id:doc.id})
  return orderInfo.request.id
}

const checkTag = async (params) => {
  const imgDocs = await service.getItemImg(params)
  const tagDocs = await service.getItemTag(params)
  const baseTag = [{
    name: '滥用商标词',
    value: 0
  },{
    name: '明显假冒',
    value: 0
  },{
    name: '盗图',
    value: imgDocs.length
  }]

  tagDocs.forEach(taginfo => {
    const tagworeds = JSON.parse(taginfo.KeywordsLabel.replace(/'/g,'"'))
    if (tagworeds.brd_label !== 'NoMatch') baseTag[0].value = 1
    if (tagworeds.ct_label !== 'NoMatch') baseTag[1].value = 1
  })

  let tagcontent = ''
  baseTag.forEach(thetage => {
    if (thetage.value) tagcontent += ` <span>${thetage.name}</span> `
  })
  let imgcontent = ''
  imgDocs.forEach(imginfo => {
    imgcontent += `<div>
    <p>所盗图片：</p>
    <img src="https://image.simplybrand.com/xsd/${imginfo.DataImg}">
    <p>官方原图：</p>
    <img src="https://image.simplybrand.com/offical/${imginfo.AssetImg}">
    </div>`
  })
  const orderComment = tagcontent.length&&`<p>涉及问题：${tagcontent}</p>` + imgcontent

  const orderString = await support.order.replyOrder({
    id: params.orderid,
    email: params.email,
    content: orderComment
  })

  return params
}

const getByBoth = async (params) => {
  const data = await service.getByBoth(params)
  return data
}
    
export default {
  sendOrder,
  getByBoth
}
