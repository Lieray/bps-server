import { dataAuth, dataView, sqlClient } from '../../../data'

const setStatus = async (params) => {
  const sql = `UPDATE monitorlist SET MonitorStatus = 2 WHERE MonitorId IN (${params.id})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getItemImg = async (params) => {
  const sql = `SELECT * FROM monitordata.image_checkout WHERE DataId IN (${params.id})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getItemTag = async (params) => {
  const sql = `SELECT * FROM monitordata.assetlabel WHERE AssetID IN (${params.id})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getByBoth = async (params) => {
  const sql = `SELECT * FROM monitorlist WHERE ProjectId =  ${params.project} AND MonitorId IN (${params.id})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const compItem = async (queryDocs, addDocs) => {
  const sqlArray = [
    'INSERT INTO operation(`mainid`,`userId`,`resultId`, `content`, `type`, `createBy`, `operationId`) VALUES ?',
    'INSERT INTO monitor(`mainid`,`userId`,`itemId`, `itemtype`, `status`, `createBy`, orderId) VALUES ?'
  ]
  return await compRecord(sqlArray,queryDocs,addDocs)
}

const compRecord = async (sqlArray,queryDocs,addDocs) => {
  return new Promise((resolve, reject) => {
    sqlClient.getConnection((error, connection) => {
      if (error) return reject(error)
      connection.beginTransaction(erro => {
        if (erro) {
          connection.release()
          return reject(erro)
        }
        const costsql = sqlArray[0]
        connection.query(costsql, [queryDocs], (mst) => {
          if (mst) {
            connection.rollback()
            connection.release()
            return reject(mst)
          }

          const opsql = sqlArray[1]
          connection.query(opsql, [addDocs], (er) => {
            if (er) {
              connection.rollback()
              connection.release()
              return reject(e)
            }
            connection.commit()
            resolve(sqlArray)
          })
        })
      })
    })
  })
}

export default {
  getByBoth,
  compItem,
  setStatus,
  getItemImg,
  getItemTag
}