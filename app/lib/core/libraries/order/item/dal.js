import { dataView, dataAuth } from '../../../data'

const setOrder = async (params) => {
  let sql = `UPDATE product SET orderId = ${params.id} WHERE resultId = ${params.result}`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

const getItemByBoth = async (params) => {
  const sql = `SELECT * FROM listfilter WHERE projectId =  ${params.project} AND resultId IN (${params.id})`
  const sqlResult = await dataView(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
  // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brand)
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`

    const viewResult = await dataView(particulars)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.ResultId == element.resultId) || {}

      const basedoc = {
        ResultId: element.resultId,
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        ConfidenceLevelBucket: element.level,
        ChannelId: element.channelId,
        ChannelName: element.channelName,
        RightsProtectionStatus: Number.parseInt(element.status),
        RightsProtectionTime: element.createAt || ''
      }
      cbDocs.push(Object.assign(extdoc, basedoc))
    })
  }
  return cbDocs
}


export default {
    setOrder,
    getItemByBoth
}
