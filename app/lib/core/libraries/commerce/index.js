/**
 * 配置可用模块
 */
import apply from './apply'
import chart from './chart'
import filter from './filter'
import list from './list'

export default {
    apply,
    chart,
    filter,
    list
  }