import { dataView } from '../../../data'
import Moment from 'moment'

const getFakeItem = async (params) => {
  let sql = `SELECT count(0) AS FakeProductCount 
            FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getTotalItem = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getDailyCount = async (params) => {
  let sql = `SELECT 'FakeProductValueTrade' AS name, discriminant AS FakeProductValueTradeData, 
              count(0) AS FakeProductValueTradeValue FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += 'GROUP BY discriminant ORDER BY discriminant ASC'
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getItemByChannel = async (params) => {
  let sql = 'SELECT channelId, channelName, result, count(0) as channelCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" GROUP BY channelId, channelName, result`
  const sqlResult = await dataView(sql)

  const cbDocs = []
  sqlResult.forEach(element => {
    cbDocs.push({
      name: 'FakeProductStatusByChannel',
      FakeProductStatusByChannel_ChanelId: element.channelId,
      FakeProductStatusByChannel_ChanelName: element.channelName,
      FakeProductStatusByChannel_DiscriminantResult: 1,
      FakeProductStatusByChannel_ProductCount: element.result === 1 ? element.channelCount : 0
    }, {
      name: 'FakeProductStatusByChannel',
      FakeProductStatusByChannel_ChanelId: element.channelId,
      FakeProductStatusByChannel_ChanelName: element.channelName,
      FakeProductStatusByChannel_DiscriminantResult: 0,
      FakeProductStatusByChannel_ProductCount: element.result === 0 ? element.channelCount : 0
    })
  })

  return cbDocs
}

const getFakeShop = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM listfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteShop) sql += `shopId NOT IN (${params.whiteShop}) AND `
  else sql += 'result = 0 AND '
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getTotalShop = async (params) => {
  let sql = `SELECT count(shopId) AS FakeShopCount FROM ( 
              SELECT shopId FROM listfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getShopByChannel = async (params) => {
  let sql = 'SELECT channelId, channelName, discriminant, count(shopId) AS shopCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY channelId, channelName, discriminant ORDER BY discriminant ASC`
  const sqlResult = await dataView(sql)
  const cbDocs = []
  if (sqlResult.length) {
    sqlResult.forEach(element => {
      cbDocs.push({
        name: 'FakeShopStatusByChannel',
        FakeShopStatusByChannel_ChannelId: element.channelId,
        FakeShopStatusByChannel_ChannelName: element.channelName,
        FakeShopStatusByChannel_ShopCount: element.shopCount,
        FakeShopStatusByChannel_DiscriminantTime: element.discriminant
      })
    })
  }
  return cbDocs
}

export default {
  getFakeItem,
  getTotalItem,
  getDailyCount,
  getItemByChannel,
  getFakeShop,
  getTotalShop,
  getShopByChannel
}
