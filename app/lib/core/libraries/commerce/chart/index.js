import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const itemCount = async (params) => {
  const result = await service.getFakeItem(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 0
  return data
}

const itemTotal = async (params) => {
  const result = await service.getTotalItem(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].FakeProductCount || 1
  return data
}

const itemDaily = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)

  const data = await service.getDailyCount(params)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeProductValueTrade',
      FakeProductValueTradeData: day.format('YYYY-MM-DD'),
      FakeProductValueTradeValue: 0
    }
    const doc = data.find(t => moment(t.FakeProductValueTradeData).isSame(defaultDoc.FakeProductValueTradeData, 'day'))
    if (doc) defaultDoc.FakeProductValueTradeValue = doc.FakeProductValueTradeValue 
    formatData.push(defaultDoc)
  }
  return formatData
}

const itemChannel = async (params) => {
  const data = await service.getItemByChannel(params)
  return data
}

const shopCount = async (params) => {
  const result = await service.getFakeShop(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 0
  return data
}

const shopTotal = async (params) => {
  const result = await service.getTotalShop(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 1
  }
  if (result.length) data.FakeShopCount = result[0].FakeShopCount || 1
  return data
}

const shopChannel = async (params) => {
  const moment = extendMoment(Moment)
  const dates = [moment(params.start, 'YYYY-MM-DD'), moment(params.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  params.dataRange = []
  for (const day of range.by('days')) {
    params.dataRange.push(day.format('YYYY-MM-DD'))
  }
  const data = await service.getShopByChannel(params)
  return data
}

export default {
    itemCount,
    itemTotal,
    itemDaily,
    itemChannel,
    shopCount,
    shopTotal,
    shopChannel
}
