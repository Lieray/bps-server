import { dataBases } from '../../../data'

const getFakeCount = async (params) => {
  let sql = `SELECT count(0) AS FakeProductCount 
            FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getTotalCount = async (params) => {
  let sql = 'SELECT count(0) AS FakeProductCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getDailyCount = async (params) => {
  let sql = `SELECT 'FakeProductValueTrade' AS name, discriminant AS FakeProductValueTradeData, 
              count(0) AS FakeProductValueTradeValue FROM listfilter WHERE result = 0 AND `
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += 'GROUP BY discriminant ORDER BY discriminant ASC'
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, result, count(0) as channelCount FROM listfilter WHERE '
  sql += `projectId =  ${params.project} AND `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" GROUP BY channelId, result`
  const sqlResult = await dataBases(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const sqlDocs = new Set()
    sqlResult.forEach(element => {
      sqlDocs.add(element.channelId)
    })
    const particulars = `SELECT channelId, channelName FROM channellist 
                        WHERE channelId IN (${[...sqlDocs].toString()})`
    const viewResult = await dataBases(particulars)
    for (const keyId of sqlDocs.keys()) {
      const extdoc = viewResult.find(ret => ret.channelId === keyId)
      const topdoc = sqlResult.find(ret => ret.channelId === keyId && ret.result === 1)
      const btmdoc = sqlResult.find(ret => ret.channelId === keyId && ret.result === 0)
      // 实际数据应该按照channelId来进一步分组
      cbDocs.push({
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 1,
        FakeProductStatusByChannel_ProductCount: topdoc ? topdoc.channelCount : 0
      }, {
        name: 'FakeProductStatusByChannel',
        FakeProductStatusByChannel_ChanelId: keyId,
        FakeProductStatusByChannel_ChanelName: extdoc ? extdoc.channelName : 'Undefined',
        FakeProductStatusByChannel_DiscriminantResult: 0,
        FakeProductStatusByChannel_ProductCount: btmdoc ? btmdoc.channelCount : 0
      })
    }
  }
  return cbDocs
}

const getByRPStatus = async (params) => {
  let sql = 'SELECT `status` AS RightsProtectionStatus, count(0) AS CountNum, '
  sql += '"CountByRightsProtectionStatus" AS Name FROM listfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"
          GROUP BY RightsProtectionStatus`
  const sqlResult = await dataBases(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getByChannel = async (params) => {
  let sql = 'SELECT channelId, channelName FROM listfilter WHERE '
  sql += `projectId =  ${params.project} `
  sql += 'GROUP BY channelId ORDER BY channelId'
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getByBoth = async (params) => {
  const sql = `SELECT resultId FROM listfilter WHERE projectId =  ${params.project} AND resultId IN (${params.id})`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM listfilter 
            WHERE projectId = ${params.project} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const fakeListCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM itemfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getFakeList = async (params) => {
  let sql = `SELECT * FROM itemfilter WHERE projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  if (params.order % 10) sql += 'ORDER BY discriminant ASC, '
  else sql += 'ORDER BY discriminant DESC, '
  if (params.order > 9) sql += '`level` ASC '
  else sql += '`level` DESC '

  sql += `LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataBases(sql)
  // 如果有结果
  return sqlResult
}

const getCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM listfilter WHERE '
  sql += `projectId = ${params.project} `
  const sqlResult = await dataBases(sql)
  return sqlResult
}

const getList = async (params) => {
  let sql = `SELECT * FROM itemfilter WHERE projectId = ${params.project} `
  sql += 'ORDER BY discriminant ASC '
  sql += `LIMIT ${params.skip}, ${params.size}`

  const sqlResult = await dataBases(sql)

  return sqlResult
}

export default {
  getFakeCount,
  getTotalCount,
  getDailyCount,
  getByChannel,
  getLastDate,
  getByRPStatus,
  getByChannel,
  getByBoth
}

