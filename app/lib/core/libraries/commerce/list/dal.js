import { dataView, dataAuth } from '../../../data'

const getFakeItemCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM listfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getFakeItemList = async (params) => {
  let sql = `SELECT * FROM listfilter WHERE projectId = ${params.project} AND `
  if (params.rpstatus) sql += '`status` = ' + params.rpstatus + ' AND '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" `
  if (params.order % 10) sql += 'ORDER BY discriminant ASC, '
  else sql += 'ORDER BY discriminant DESC, '
  if (params.order > 9) sql += '`level` ASC '
  else sql += '`level` DESC '

  sql += `LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brand)
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`
    const statusInfo = `SELECT * FROM product WHERE resultId IN (${retDocs.toString()})`

    const viewResult = await dataView(particulars)
    const statusResult = await dataAuth(statusInfo)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.ResultId == element.resultId) || {}
      const statusdoc = statusResult.find(ret => ret.resultId == element.resultId) || {}

      const basedoc = {
        ResultId: element.resultId,
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        ConfidenceLevelBucket: element.level,
        ChannelId: element.channelId,
        ChannelName: element.channelName,
        RightsProtectionStatus: Number.parseInt(element.status),
        RightsProtectionTime: element.createAt || '',
        RightsProtectionStartTime: statusdoc.createAt ? statusdoc.createAt : ''
      }
      cbDocs.push(Object.assign(extdoc, basedoc))
    })
  }
  return cbDocs
}

const getItemCount = async (params) => {
  let sql = 'SELECT count(0) AS CountNum FROM listfilter WHERE '
  sql += `projectId = ${params.project} `
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getItemList = async (params) => {
  let sql = `SELECT * FROM listfilter WHERE projectId = ${params.project} `
  sql += 'ORDER BY discriminant ASC '
  sql += `LIMIT ${params.skip}, ${params.size}`

  const sqlResult = await dataView(sql)
  // 如果有结果
  const cbDocs = []
  if (sqlResult.length) {
    // 需要去数据库查询这些条目的具体名称
    const retDocs = []
    const brandDocs = new Set()
    const channelDocs = new Set()
    sqlResult.forEach(element => {
      brandDocs.add(element.brand)
      brandDocs.add(element.brandId)
      channelDocs.add(element.channelId)
      retDocs.push(element.resultId)
    })

    const particulars = `SELECT * FROM itemdata WHERE ResultId IN (${retDocs.toString()})`
    const statusInfo = `SELECT * FROM product WHERE resultId IN (${retDocs.toString()})`

    const viewResult = await dataView(particulars)
    const statusResult = await dataAuth(statusInfo)

    // 将结果合并
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.ResultId == element.resultId) || {}
      const statusdoc = statusResult.find(ret => ret.resultId == element.resultId) || {}

      const basedoc = {
        ResultId: element.resultId,
        ProductDescription: element.description,
        Score: element.score,
        DiscriminantResult: element.result,
        ConfidenceLevelBucket: element.level,
        ChannelId: element.channelId,
        ChannelName: element.channelName,
        RightsProtectionStatus: element.status,
        RightsProtectionTime: element.createAt || '',
        RightsProtectionStartTime: statusdoc.createAt ? statusdoc.createAt : ''
      }
      cbDocs.push(Object.assign(extdoc, basedoc))
    })
  }
  return cbDocs
}

const getFakeShopCount = async (params) => {
  let sql = `SELECT COUNT(DISTINCT shopId) AS FakeShopCount FROM ( 
    SELECT shopId FROM listfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getShopByStatus = async (params) => {
  let sql = 'SELECT COUNT(DISTINCT c.shopId) AS FakeShopCount FROM ( SELECT a.shopId FROM listfilter a '
  if (Number.parseInt(params.rpstatus) === 1) sql += 'LEFT JOIN bps_subscript.shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE b.shopId IS NULL AND '
  else sql += 'INNER JOIN shop b ON a.shopId = b.shopId  AND a.projectId = b.projectId AND `b`.`status` = ' + `${params.rpstatus} WHERE `
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `
  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `
  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" ) c`
  const sqlResult = await dataView(sql)
  return sqlResult
}
  
const getFakeShopList = async (params) => {
  let sql = `SELECT shopId, COUNT(IF(result = 0,1,null)) AS fakenum,
              COUNT(IF(result = 0 AND link = 1,1,null)) AS linknum, `
  sql += 'COUNT(IF(`status`=4,1,null)) AS rpnum FROM `listfilter`'

  sql += `WHERE projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList

  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `

  sql += `(discriminant BETWEEN "${params.start}" AND "${params.end}") 
          GROUP BY shopId LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    const channelDocs = new Set()

    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
      channelDocs.add(element.channelId)
    })
    const statusSql = 'SELECT shopId, `status`, createAt FROM shop ' + `WHERE shopId IN (${retDocs.toString()})`
    const statusDocs = await dataAuth(statusSql)

    const particulars = `SELECT * FROM store WHERE shopId IN (${retDocs.toString()})`
    const viewResult = await dataAuth(particulars)

    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.shopId === element.shopId)

      const formatDoc = {
        ShopId: element.shopId,
        ShopName: extdoc? extdoc.shopName : null,
        ShopURL: extdoc? extdoc.shopLink : null,
        ChannelName: extdoc? extdoc.channelName : null,
        ChannelId: extdoc? extdoc.channelId : null,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      }

      if (params.authList) {
        const rpResult = statusDocs.find(ret => ret.shopId === element.shopId)
        const shopInfo = params.authList.find(t => t === element.shopId)
        if (shopInfo) {
          formatDoc.Authorize = 1
          formatDoc.RightsProtectionStatus = 0
          formatDoc.RightsProtectionTime = ''
        } else {
          formatDoc.Authorize = 0
          formatDoc.RightsProtectionStatus = rpResult ? rpResult.status : 1
          formatDoc.RightsProtectionTime = rpResult ? rpResult.createAt : ''
        }
      }

      cbDocs.push(formatDoc)
    })
  }
  return cbDocs
}

const getShopStatusList = async (params) => {
  let sql = `SELECT shopId, COUNT(IF(result = 0,1,null)) AS fakenum,
              COUNT(IF(result = 0 AND link = 1,1,null)) AS linknum, `
  sql += 'COUNT(IF(`status`=4,1,null)) AS rpnum FROM `listfilter` a'

  if (Number.parseInt(params.rpstatus) === 1) sql += 'LEFT JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE c.shopId IS NULL AND '
  else sql += 'INNER JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId AND `b`.`status` = ' + `${params.rpstatus} WHERE `
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `

  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `

  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY a.shopId LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  const cbDocs = []
  if (sqlResult.length) {
    const retDocs = []
    sqlResult.forEach(element => {
      retDocs.push(`"${element.shopId}"`)
    })
    const statusSql = 'SELECT shopId, `status`, createAt FROM shop ' + `WHERE shopId IN (${retDocs.toString()})`
    const statusDocs = await dataAuth(statusSql)

    const particulars = `SELECT * FROM store WHERE ShopId IN (${retDocs.toString()})`
    const viewResult = await dataAuth(particulars)
    sqlResult.forEach(element => {
      const extdoc = viewResult.find(ret => ret.shopId == element.shopId)
      const formatDoc = {
        ShopId: extdoc.shopId,
        ShopName: extdoc.shopName,
        ShopURL: extdoc.shopLink,
        ChannelName: extdoc.channelName,
        ChannelId: extdoc.channelId,
        FakeProductNum: element.fakenum,
        RightProctedLinkNum: element.linknum,
        RightProctedNum: element.rpnum
      }

      const rpResult = statusDocs.find(ret => ret.shopId == element.shopId)
      formatDoc.Authorize = 0
      formatDoc.RightsProtectionStatus = rpResult ? rpResult.status : 1
      formatDoc.RightsProtectionTime = rpResult ? rpResult.createAt : ''

      cbDocs.push(formatDoc)
    })
  }
  return cbDocs
}

export default {
    getFakeItemCount,
    getFakeItemList,
    getItemCount,
    getItemList,
    getFakeShopCount,
    getShopByStatus,
    getFakeShopList,
    getShopStatusList
}