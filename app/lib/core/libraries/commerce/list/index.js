import service from './dal'

const itempageCount = async (params) => {
  const data = await service.getFakeItemCount(params)
  return data
}
const itempageList = async (params) => {
  const data = await service.getFakeItemList(params)
  return data
}
const getItemCount = async (params) => {
  const data = await service.getItemCount(params)
  return data
}
const getItemList = async (params) => {
  const data = await service.getItemList(params)
  return data
}

const shoppageCount = async (params) => {
  const data = await service.getFakeShopCount(params)
  return data
}

const shoppageList = async (params) => {
  const data = await service.getFakeShopList(params)
  return data
}

const shopstatusCount = async (params) => {
  const data = await service.getShopByStatus(params)
  return data
}

const shopstatusList = async (params) => {
  const data = await service.getShopStatusList(params)
  return data
}

export default {
    itempageCount,
    itempageList,
    getItemCount,
    getItemList,
    shoppageCount,
    shoppageList,
    shopstatusCount,
    shopstatusList
}