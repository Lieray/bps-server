import service from './dal'
import moment from 'moment'

const itemfilterStatus = async (params) => {
  const data = await service.getItemByRPStatus(params)
  return data
}
const itemfilterChannel = async (params) => {
  const data = await service.getItemByChannel(params)
  return data
}
const getitemByBoth = async (params) => {
  const data = await service.getItemByBoth(params)
  return data
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: moment().endOf('day').toDate()
  }
  const data = await service.getLastDate(params)
  if (data.length && moment(data[0].discriminant)) {
    defaultDate.end = moment(data[0].discriminant).endOf('day').toDate()
  }
  defaultDate.start = moment(defaultDate.end).subtract(6, 'days').startOf('day').toDate()
  return defaultDate
}

const shopfilterCount = async (params) => {
  const data = await service.getShopCount(params)
  return data
}

const shopfilterStatus = async (params) => {
  const data = await service.getShopByStatus(params)
  return data
}

const shopfilterChannel = async (params) => {
  const data = await service.getShopByChannel(params)
  return data
}

const getShopByBoth = async (params) => {
  const data = await service.getShopByBoth(params)
  return data
}

export default {
  getLastDate,
  itemfilterStatus,
  itemfilterChannel,
  getitemByBoth,
  shopfilterCount,
  shopfilterStatus,
  shopfilterChannel,
  getShopByBoth
}
