import { dataView } from '../../../data'

const getItemByRPStatus = async (params) => {
  let sql = 'SELECT `status` AS RightsProtectionStatus, count(0) AS CountNum, '
  sql += '"CountByRightsProtectionStatus" AS Name FROM listfilter WHERE '
  sql += `projectId = ${params.project} AND `
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.result) sql += `result = ${params.result} And `
  if (params.level) sql += '`level`' + ` = ${params.level} And `
  if (params.bucket) sql += '`level`' + `IN (${params.bucket}) And `
  if (params.search) sql += `description like "%${params.search}%" And `
  if (params.whiteList) sql += params.whiteList
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}"
          GROUP BY RightsProtectionStatus`
  const sqlResult = await dataView(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getItemByChannel = async (params) => {
  let sql = 'SELECT channelId as ChannelId, channelName as ChannelName FROM listfilter WHERE '
  sql += `projectId =  ${params.project} `
  sql += 'GROUP BY channelId, channelName ORDER BY channelId'
  console.log(sql)
  const sqlResult = await dataView(sql)

  return sqlResult
}

const getItemByBoth = async (params) => {
  const sql = `SELECT * FROM listfilter WHERE projectId =  ${params.project} AND resultId IN (${params.id})`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getShopCount = async (params) => {
  let sql = `SELECT COUNT(DISTINCT shopId) AS FakeShopCount FROM ( 
    SELECT shopId FROM listfilter WHERE `
  sql += `projectId =  ${params.project} AND `
  if (params.whiteList) sql += params.whiteList
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `discriminant BETWEEN "${params.start}" AND "${params.end}" 
          GROUP BY shopId) a`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getShopByStatus = async (params) => {
  let sql = 'SELECT COUNT(DISTINCT c.shopId) AS FakeShopCount, c.RightsProtectionStatus FROM ( '
  sql += 'SELECT a.shopId, ifnull(b.`status`,1) AS RightsProtectionStatus FROM listfilter a '
  sql += 'LEFT JOIN shop b ON a.shopId = b.shopId AND a.projectId = b.projectId WHERE '
  sql += `a.projectId =  ${params.project} AND a.shopId NOT IN (${params.whiteShop}) AND `
  if (params.channel) sql += `a.channelId = ${params.channel} And `
  if (params.search) sql += `a.shopName like "%${params.search}%" And `
  sql += `a.discriminant BETWEEN "${params.start}" AND "${params.end}" `
  sql += ') c GROUP BY c.RightsProtectionStatus'
  const sqlResult = await dataView(sql)
  sqlResult.forEach(element => {
    element.RightsProtectionStatus = Number.parseInt(element.RightsProtectionStatus)
  })
  return sqlResult
}

const getShopByChannel = async (params) => {
  let sql = 'SELECT channelId as ChannelId, ChannelName as ChannelName FROM listfilter WHERE '
  if (params.channel) sql += `channelId = ${params.channel} And `
  if (params.search) sql += `shopName like "%${params.search}%" And `
  sql += `projectId =  ${params.project} `
  sql += 'GROUP BY channelId, channelName ORDER BY channelId '
  const sqlResult = await dataView(sql)

  return sqlResult
}

const getShopByBoth = async (params) => {
  const sql = `SELECT shopId FROM listfilter WHERE projectId =  ${params.project} AND shopId IN (${params.id}) GROUP BY shopId`
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT discriminant FROM listfilter 
            WHERE projectId =  ${params.project} 
            ORDER BY discriminant DESC LIMIT 1`
  const sqlResult = await dataView(sql)
  return sqlResult
}

export default {
  getLastDate,
  getItemByRPStatus,
  getItemByChannel,
  getItemByBoth,
  getShopCount,
  getShopByStatus,
  getShopByChannel,
  getShopByBoth
}