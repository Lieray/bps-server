import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].Count || 0
  return data
}

const dashboardAuth = async (params) => {
  const result = await service.getAuthCount(params)
  const data = {
    name: 'FakeShopCount',
    FakeShopCount: 0
  }
  if (result.length) data.FakeShopCount = result[0].Count || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalCount(params)
  const data = {
    name: 'ShopCount',
    ShopCount: 1
  }
  if (result.length) data.ShopCount = result[0].Count || 1
  return data
}

const dailyChannel = async (params) => {
  const query = {
    start: Moment(params.start).subtract(6, 'days').format('YYYY-MM-DD'),
    end: Moment(params.end).subtract(1, 'days').format('YYYY-MM-DD'),
    project: params.project
  }

  const moment = extendMoment(Moment)
  const dates = [moment(query.start, 'YYYY-MM-DD'), moment(query.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  query.end += ' 23:59:59'
  const data = await service.getDailyChannel(query)
  const formatData = {
    xdatetime:[],
    yvalue:[]
  }
  const channelDocs = new Set()
  data.forEach(element => {
    channelDocs.add(element.Channel)
  });
  for(let item of channelDocs.keys()) {
    formatData.yvalue.push({
      name: item,
      data:[]
    })
  }

  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    formatData.xdatetime.push(day.format('YYYY-MM-DD'))
    const doc = data.filter(t => moment(t.dateString).isSame(day, 'day'))

    formatData.yvalue.forEach(element => {
      const currentData = doc.find(t => t.Channel === element.name) || {}
      element.data.push(currentData.Count || 0)
    })
  }
  return formatData
}

const pageCount = async (params) => {
  const data = await service.getTotalItem(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}

const getDetail = async (params) => {
  const data = await service.getDetail(params)
  return data
}

const getStatusLevel = async (params) => {
  const cbData = [0,0,0,0,0]
  const result = await service.getStatusLevel(params)

  cbData.forEach( (_currentValue, index) => {
    const dataValue = result.find(ret => ret.statusLevel === `status${index}`)
    if (dataValue) cbData[index] = dataValue.cnt
  })

  return cbData
}


export default {
  dashboardCount,
  dashboardAuth,
  dashboardTotal,
  dailyChannel,
  pageCount,
  pageList,
  getDetail,
  getStatusLevel
}