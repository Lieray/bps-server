import { dataView } from '../../../data'

const getFakeCount = async (params) => {
  // const sql = `SELECT COUNT(DISTINCT ShopId) AS 'Count' FROM (
  //   SELECT ShopId FROM monitorfilter WHERE PriceStatus = 2 
  //   AND ProjectId IN (${params.project}) AND 
  //   InsertDate BETWEEN '${params.start}' AND '${params.end}'
  //   GROUP BY ShopId) a`

  const sql = `SELECT COUNT(DISTINCT ShopId) AS 'Count' FROM (
    SELECT ShopId FROM monitorlist WHERE MarginPrice < '0' 
    AND ProjectId IN (${params.project}) 
    AND InsertDate  BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY ShopId) a`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getTotalCount = async (params) => {
  // const sql = `SELECT COUNT(a.ShopId) AS 'Count' FROM (
  //   SELECT ShopId FROM monitorfilter WHERE ProjectId IN (${params.project}) 
  //   AND InsertDate BETWEEN '${params.start}' AND '${params.end}'
  //   GROUP BY ShopId) a LEFT JOIN monitorshop d ON a.ShopId = d.ShopId`

  const sql = `SELECT COUNT(a.ShopId) AS 'Count' FROM (
    SELECT ShopId FROM monitorlist WHERE ProjectId IN (${params.project}) 
    GROUP BY ShopId) a`
const sqlResult = await dataView(sql)
  return  sqlResult
}

const getAuthCount = async (params) => {
  const sql = `SELECT COUNT(b.ShopId) AS 'Count' FROM (
    SELECT a.ShopId, d.ShopId AS authShop FROM 
    (SELECT ShopId FROM monitorlist WHERE ProjectId IN (${params.project})
    AND InsertDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY ShopId) a LEFT JOIN authshop d ON (a.ShopId = d.ShopId
    AND d.IsAuthor = 1 AND d.ProjectId = ${params.project}) 
    HAVING d.ShopId IS NULL) b`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDailyChannel = async (params) => {
  const sql = `SELECT CurrentDate AS dateString, ShopCount AS "Count", Channel
  FROM v_channel_daily WHERE ProjectId = ${params.project}
  AND CurrentDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getTotalItem = async (params) => {
  // let sql = `SELECT COUNT(a.ShopId) AS 'Count' FROM (
  //   SELECT ShopId FROM monitorfilter WHERE ProjectId IN (${params.project}) 
  //   AND InsertDate BETWEEN '${params.start}' AND '${params.end}'
  //   GROUP BY ShopId) a LEFT JOIN 
  //   (SELECT b.ShopId, c.ProjectId,b.ShopName,b.ShopURL,b.channelId,
  //     IFNULL(c.IsAuthor,0) AS IsAuthor,b.Channel FROM monitorshop b
  //     LEFT JOIN authshop c ON b.ShopId = c.ShopId AND c.ProjectId = ${params.project})
  //   d ON a.ShopId = d.ShopId WHERE `
  let sql = `SELECT COUNT(a.ShopId) AS 'Count' FROM (
    SELECT ShopId FROM monitorlist WHERE ProjectId IN (${params.project}) 
    GROUP BY ShopId) a LEFT JOIN 
    (SELECT b.ShopId, c.ProjectId,b.ShopName,b.ShopURL,b.channelId,
      IFNULL(c.IsAuthor,0) AS IsAuthor,b.Channel FROM monitorshop b
      LEFT JOIN authshop c ON b.ShopId = c.ShopId AND c.ProjectId = ${params.project})
    d ON a.ShopId = d.ShopId WHERE `
    if (params.channel) sql += `d.Channel = '${params.channel}' AND `
    if (params.authorize) sql += `d.IsAuthor  = '${params.authorize}' AND `
    if (params.search) sql += `d.ShopName LIKE '%${params.search}%' AND `
    sql += `a.ShopId IS NOT NULL `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getFakeList = async (params) => {
  // let sql = `SELECT  d.ShopName, d.ShopUrl, d.Channel, d.IsAuthor, d.ShopId,a.*
  // FROM ( SELECT ShopId  AS InfoId, COUNT(DISTINCT MonitorId) AS TotalLink,
  // COUNT(DISTINCT (IF(PriceStatus = 2,MonitorId,NULL))) AS FakeLink,
  // COUNT(DISTINCT IF(PriceStatus = 2,date_format(InsertDate,'%Y-%m-%d'),NULL)) AS FakeDay,
  // COUNT(IF(PriceStatus = 2,MonitorId,NULL)) AS FakeCount,  
  // COUNT(DISTINCT (IF(PriceStatus = 2,SkuId,NULL))) AS FakeItem,
  // "0"  AS ReportLink, MIN(FinalPrice/MonitorPrice) AS Discount
  // FROM monitorfilter WHERE ProjectId = ${params.project} AND
  // InsertDate BETWEEN '${params.start}' AND '${params.end}'
  // GROUP BY ShopId) a LEFT JOIN 
  // (SELECT b.ShopId, c.ProjectId,b.ShopName,b.ShopURL,b.channelId,
  //   IFNULL(c.IsAuthor,0) AS IsAuthor,b.Channel FROM monitorshop b
  //   LEFT JOIN authshop c ON b.ShopId = c.ShopId AND c.ProjectId = ${params.project})
  // d ON a.InfoId = d.ShopId WHERE `

  let sql = `SELECT  d.ShopName, d.ShopUrl, d.Channel, d.ShopLab,d.IsAuthor, d.ShopId,
  d.SkuTotalTrade, d.SkuTotalAmount, a.*
  FROM (SELECT ShopId  AS InfoId, COUNT(DISTINCT MonitorId) AS TotalLink,
  COUNT(DISTINCT (IF(MarginPrice < '0',MonitorId,NULL))) AS FakeLink,
  COUNT(DISTINCT IF(MarginPrice < '0',date_format(InsertDate,'%Y-%m-%d'),NULL)) AS FakeDay,
  COUNT(IF(MarginPrice < '0',MonitorId,NULL)) AS FakeCount,  
  COUNT(DISTINCT (IF(MarginPrice < '0',SkuId,NULL))) AS FakeItem,
  "0"  AS ReportLink, MIN(FinalPrice/MonitorPrice) AS Discount
  FROM monitorlist WHERE ProjectId =  ${params.project}
  GROUP BY ShopId) a LEFT JOIN 
  (SELECT b.ShopId, c.ProjectId,b.ShopName,b.ShopURL,b.channelId, b.ShopLab,
    b.SkuTotalTrade, b.SkuTotalAmount,
    IFNULL(c.IsAuthor,0) AS IsAuthor,b.Channel FROM monitorshop b
    LEFT JOIN authshop c ON b.ShopId = c.ShopId AND c.ProjectId = ${params.project})
  d ON a.InfoId = d.ShopId WHERE `

  if (params.channel) sql += `d.Channel = '${params.channel}' AND `
  if (params.authorize) sql += `d.IsAuthor  = '${params.authorize}' AND `
  if (params.search) sql += `d.ShopName LIKE '%${params.search}%' AND `
  sql += ` a.InfoId IS NOT NULL Order BY ${params.order}
  LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDetail = async (params) => {
  let sql = `SELECT a.*, d.BaseAmount, d.TotalAmount,
    f.hasbrd, f.hasct, f.haspic FROM 
    monitorlist a LEFT JOIN itemsales d ON a.AssetId = d.AssetID
    LEFT JOIN monitorlable f ON a.AssetId = f.AssetID
    WHERE a.MarginPrice < '0' AND a.shopId = ${params.shopId} 
    AND a.ProjectId = ${params.project} ORDER BY MarginPercent `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getStatusLevel = async (params) => {
  // let sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
  //   'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
  //   count(ShopId) as cnt FROM (SELECT MIN(o.MarginPercent) AS Discount, 
  //   o.ShopId FROM monitorfilter o JOIN monitorlist r ON o.MonitorId = r.MonitorId
  //   WHERE o.PriceStatus = 2 AND o.ProjectId = ${params.project} 
  //   AND o.InsertDate BETWEEN '${params.start}' AND '${params.end}'
  //   GROUP BY o.ShopId) a group by statusLevel`


  const sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
    'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
    count(ShopId) as cnt FROM (SELECT MIN(MarginPercent) AS Discount, 
    ShopId FROM monitorlist WHERE MarginPrice < '0' AND ProjectId = ${params.project}
    GROUP BY ShopId) a group by statusLevel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  getFakeCount,
  getAuthCount,
  getTotalCount,
  getDailyChannel,
  getTotalItem,
  getFakeList,
  getDetail,
  getStatusLevel
}