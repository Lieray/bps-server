import { dataView } from '../../../data'

const getFakeLink = async (params) => {
  const sql = `SELECT Count(*) AS "Count" FROM monitorlist WHERE MarginPrice < 0
  AND ProjectId IN (${params.project}) AND InsertDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getTotalLink = async (params) => {
  const sql = `SELECT Count(*) AS "Count" FROM monitorlist WHERE ProjectId IN (${params.project})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getChannelLink = async (params) => {
  const sql = `SELECT Channel, SUM(ItemCount) AS "Count" FROM v_channel_daily
  WHERE ProjectId IN (${params.project}) AND 
  CurrentDate BETWEEN '${params.start}' AND '${params.end}' GROUP BY Channel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDailyLink = async (params) => {
  const sql = `SELECT CurrentDate AS dateString, SUM(ItemCount) AS "Count" 
  FROM v_channel_daily WHERE ProjectId  IN (${params.project}) AND 
  CurrentDate BETWEEN '${params.start}' AND '${params.end}' GROUP BY CurrentDate`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const findFakeLink = async (params) => {
  let sql = `SELECT Count(r.AssetId) AS "Count" from monitordata a
  join monitorlist r ON a.skuid = r.SkuId
  where a.project IN (${params.project}) AND r.MarginPrice < 0 `
  if (params.channel) sql += `AND r.ChannelId = ${params.channel} `
  if (params.series) sql += `AND a.series = "${params.series}" `
  if (params.model) sql += `AND a.model = "${params.model}" `
  if (params.search) sql += 'AND (a.`name`' + ` LIKE '%${params.search}%' OR 
  a.series LIKE '%${params.search}%' OR a.model LIKE '%${params.search}%'
  OR a.descripition LIKE '%${params.search}%' OR a.type LIKE '%${params.search}%') `
  sql += `AND r.InsertDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const findTotalLink = async (params) => {
  let sql = `SELECT Count(r.AssetId) AS "Count" from monitordata a
  join monitorlist r ON a.skuid = r.SkuId where a.project IN (${params.project}) `
  if (params.channel) sql += `AND r.ChannelId = ${params.channel} `
  if (params.series) sql += `AND a.series = "${params.series}" `
  if (params.model) sql += `AND a.model = "${params.model}" `
  if (params.search) sql += 'AND (a.`name`' + ` LIKE '%${params.search}%' OR 
  a.series LIKE '%${params.search}%' OR a.model LIKE '%${params.search}%'
  OR a.descripition LIKE '%${params.search}%' OR a.type LIKE '%${params.search}%') `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const findDailyLink = async (params) => {
  let sql = `SELECT dateString, SUM(ItemCount) AS "Count" from
    (select date_format(o.InsertDate,"%Y-%m-%d") AS dateString,
    count(distinct o.MonitorId) AS ItemCount from monitordata a
    join monitorlist r ON a.skuid = r.SkuId
    join monitorfilter o ON r.AssetId = o.AssetId
    where a.project IN (${params.project}) AND o.PriceStatus = 2 `
  if (params.channel) sql += `AND r.ChannelId = ${params.channel} `
  if (params.series) sql += `AND a.series = "${params.series}" `
  if (params.model) sql += `AND a.model = "${params.model}" `
  if (params.search) sql += 'AND (a.`name`' + ` LIKE '%${params.search}%' OR 
  a.series LIKE '%${params.search}%' OR a.model LIKE '%${params.search}%'
  OR a.descripition LIKE '%${params.search}%' OR a.type LIKE '%${params.search}%') `
  sql += `AND o.InsertDate BETWEEN '${params.start}' AND '${params.end}'
    group by dateString)d GROUP BY dateString`

  const sqlResult = await dataView(sql)
  return  sqlResult
}

const findChannelLink = async (params) => {
  let sql = `SELECT Channel, SUM(ItemCount) AS "Count" FROM (select  c.dateString AS CurrentDate,
    b.ChannelName AS Channel, c.ItemCount from (
    select date_format(o.InsertDate,"%Y-%m-%d") AS dateString, 
    r.ChannelId, count(distinct o.MonitorId) AS ItemCount
    from monitordata a join monitorlist r ON a.skuid = r.SkuId
    join monitorfilter o ON r.AssetId = o.AssetId
    where a.project IN (${params.project}) AND o.PriceStatus = 2 `
  if (params.channel) sql += `AND r.ChannelId = ${params.channel} `
  if (params.series) sql += `AND a.series = "${params.series}" `
  if (params.model) sql += `AND a.model = "${params.model}" `
  if (params.search) sql += 'AND (a.`name`' + ` LIKE '%${params.search}%' OR 
    a.series LIKE '%${params.search}%' OR a.model LIKE '%${params.search}%'
    OR a.descripition LIKE '%${params.search}%' OR a.type LIKE '%${params.search}%') `
  sql += `AND o.InsertDate BETWEEN '${params.start}' AND '${params.end}'
    group by dateString, r.ChannelId) c left join monitordata.channel b 
    on c.ChannelId = b.ChannelId) d  GROUP BY Channel`

  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  getFakeLink,
  getTotalLink,
  getChannelLink,
  getDailyLink,
  findFakeLink,
  findTotalLink,
  findChannelLink,
  findDailyLink
}
