import service from './dal'
import Moment from 'moment'
import { extendMoment } from 'moment-range'

const dashboardCount = async (params) => {
  const result = await service.getFakeLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].Count || 0
  return data
}

const dashboardTotal = async (params) => {
  const result = await service.getTotalLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].Count || 1
  return data
}

const dashboardDaily = async (params) => {
  const query = {
    start: Moment(params.start).subtract(6, 'days').format('YYYY-MM-DD'),
    end: Moment(params.end).subtract(1, 'days').format('YYYY-MM-DD'),
    project: params.project
  }

  const moment = extendMoment(Moment)
  const dates = [moment(query.start, 'YYYY-MM-DD'), moment(query.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  query.end += ' 23:59:59'
  const data = await service.getDailyLink(query)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeData',
      FakeData: day.format('YYYY-MM-DD'),
      FakeValue: 0
    }
    const doc = data.find(t => moment(t.dateString).isSame(defaultDoc.FakeData, 'day'))
    if (doc) defaultDoc.FakeValue = doc.Count
    formatData.push(defaultDoc)
  }
  return formatData
}

const dashboardChannel = async (params) => {
  const data = await service.getChannelLink(params)
  return data
}

const chartCount = async (params) => {
  const result = await service.findFakeLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 0
  }
  if (result.length) data.FakeProductCount = result[0].Count || 0
  return data
}

const chartTotal = async (params) => {
  const result = await service.findTotalLink(params)
  const data = {
    name: 'FakeProductCount',
    FakeProductCount: 1
  }
  if (result.length) data.FakeProductCount = result[0].Count || 1
  return data
}

const chartDaily = async (params) => {
  const query = {
    start: Moment(params.start).subtract(6, 'days').format('YYYY-MM-DD'),
    end: Moment(params.end).subtract(1, 'days').format('YYYY-MM-DD'),
    project: params.project,
    channel: params.channel,
    series: params.series,
    model: params.model,
    search: params.search
  }

  const moment = extendMoment(Moment)
  const dates = [moment(query.start, 'YYYY-MM-DD'), moment(query.end, 'YYYY-MM-DD')]
  const range = moment.range(dates)
  query.end += ' 23:59:59'
  const data = await service.findDailyLink(query)
  const formatData = []
  // 按时间补全每日的内容
  for (const day of range.by('days')) {
    const defaultDoc = {
      name: 'FakeData',
      FakeData: day.format('YYYY-MM-DD'),
      FakeValue: 0
    }
    const doc = data.find(t => moment(t.dateString).isSame(defaultDoc.FakeData, 'day'))
    if (doc) defaultDoc.FakeValue = doc.Count
    formatData.push(defaultDoc)
  }
  return formatData
}

const chartChannel = async (params) => {
  const data = await service.findChannelLink(params)
  return data
}


export default {
  dashboardCount,
  dashboardTotal,
  dashboardDaily,
  dashboardChannel,
  chartCount,
  chartTotal,
  chartDaily,
  chartChannel
}
