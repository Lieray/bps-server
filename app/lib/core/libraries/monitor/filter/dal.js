import { dataView } from '../../../data'

const getTotalItem = async (params) => {
  let sql = `SELECT COUNT(DISTINCT SKUID) AS 'Count'  FROM monitorlist WHERE `
  if (params.series) sql += `Series = '${params.series}' AND `
  if (params.model) sql += `Model = '${params.model}' AND `
  if (params.search) sql += `ProductName LIKE '%${params.search}%' AND `
  sql += `ProjectId IN (${params.project})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getTotalLink = async (params) => {
  let sql = `SELECT COUNT(DISTINCT monitorId) AS 'Count' FROM monitorfilter
    WHERE SKUID IN (SELECT DISTINCT SKUID FROM monitorlist WHERE `
  if (params.series) sql += `Series = '${params.series}' AND `
  if (params.model) sql += `Model = '${params.model}' AND `
  if (params.search) sql += `ProductName LIKE '%${params.search}%' AND `
  sql += `ProjectId IN (${params.project}) ) AND
    InsertDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getFakeLink = async (params) => {
  let sql = `SELECT COUNT(DISTINCT monitorId) AS 'Count' FROM monitorfilter
    WHERE SKUID IN (SELECT DISTINCT SKUID FROM monitorlist WHERE `
    if (params.series) sql += `Series = '${params.series}' AND `
    if (params.model) sql += `Model = '${params.model}' AND `
    if (params.search) sql += `ProductName LIKE '%${params.search}%' AND `
  sql += `ProjectId IN (${params.project}) ) AND PriceStatus = 2 AND
    InsertDate BETWEEN '${params.start}' AND '${params.end}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}


const getByChannel = async (params) => {
  const sql = `SELECT Channel FROM monitorlist
  WHERE ProjectId IN (${params.project}) GROUP BY Channel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getBySeries = async (params) => {
  const sql = `SELECT series AS Series FROM monitordata
  WHERE project IN (${params.project}) GROUP BY series
  HAVING series IS NOT NULL AND series <> ''`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getByModel = async (params) => {
  const sql = `SELECT model AS Model FROM monitordata
  WHERE project IN (${params.project}) GROUP BY model
  HAVING model IS NOT NULL AND model <> ''`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT InsertDate AS 'CurrentDate' FROM monitorlist
  WHERE ProjectId IN (${params.project})
  ORDER BY InsertDate DESC LIMIT 1`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getFirstDate = async (params) => {
  const sql = `SELECT InsertDate AS 'CurrentDate' FROM monitorfilter
  WHERE ProjectId IN (${params.project})
  ORDER BY InsertDate ASC LIMIT 1`
  const sqlResult = await dataView(sql)
  return sqlResult
}

export default {
  getTotalItem,
  getTotalLink,
  getFakeLink,
  getFirstDate,
  getLastDate,
  getByChannel,
  getBySeries,
  getByModel
}
