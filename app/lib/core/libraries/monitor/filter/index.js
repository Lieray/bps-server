import service from './dal'
import moment from 'moment'

const totalItem = async (params) => {
  const data = await service.getTotalItem(params)
  return data
}
const totalLink = async (params) => {
  const data = await service.getTotalLink(params)
  return data
}
const fakeLink = async (params) => {
  const data = await service.getFakeLink(params)
  return data
}
const filterChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}
const filterSeries = async (params) => {
  const data = await service.getBySeries(params)
  return data
}
const filterMode = async (params) => {
  const data = await service.getByModel(params)
  return data
}

const getUpdateTime = async (params) => {
  return await service.getLastDate(params)
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: moment().endOf('day')
  }
  const data = await service.getLastDate(params)
  if (data.length && moment(data[0].CurrentDate)) {
    defaultDate.end = moment(data[0].CurrentDate).format('YYYYMMDD')
  }
  defaultDate.start = defaultDate.end
  defaultDate.end = moment(defaultDate.end).add(1, 'days').format('YYYYMMDD')
  // defaultDate.start = moment(defaultDate.end).subtract(7, 'days').format('YYYYMMDD')
  // defaultDate.last = moment(defaultDate.end).subtract(1, 'days').format('YYYYMMDD')
  return defaultDate
}
const getFirstDate = async (params) => {
  const defaultDate = {
    init: moment().endOf('day')
  }
  const data = await service.getFirstDate(params)
  if (data.length && moment(data[0].CurrentDate)) 
  defaultDate.init = moment(data[0].CurrentDate)

  return defaultDate.init
}

export default {
  totalItem,
  fakeLink,
  totalLink,
  getFirstDate,
  getLastDate,
  filterChannel,
  filterMode,
  filterSeries,
  getUpdateTime
}
