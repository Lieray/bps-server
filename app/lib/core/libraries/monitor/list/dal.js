import { dataView } from '../../../data'

const getTotalItem = async (params) => {
  let sql = `SELECT COUNT(DISTINCT skuid) AS 'Count' FROM monitordata WHERE `
  if (params.series) sql += `series = '${params.series}' AND `
  if (params.model) sql += `model = '${params.model}' AND `
  if (params.search) sql += '(`name`' + ` LIKE '%${params.search}%' OR 
  series LIKE '%${params.search}%' OR model LIKE '%${params.search}%'
  OR descripition LIKE '%${params.search}%' OR type LIKE '%${params.search}%') AND `
  sql += `project IN (${params.project})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getFakeList = async (params) => {
  let sql = `SELECT * FROM skuamount WHERE `
    if (params.series) sql += `series = '${params.series}' AND `
    if (params.model) sql += `model = '${params.model}' AND `
    if (params.search) sql += '(`name`' + ` LIKE '%${params.search}%' OR 
    series LIKE '%${params.search}%' OR model LIKE '%${params.search}%'
    OR descripition LIKE '%${params.search}%' OR type LIKE '%${params.search}%') AND `
  sql += `project IN (${params.project}) ORDER BY ${params.order} LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDBCount = async (params) => {
  let sql = `SELECT COUNT(a.MonitorId) AS 'Count' FROM monitorlist a
  LEFT JOIN monitordata e ON e.skuid = a.SkuId WHERE a.MarginPrice < '0'
  AND a.ProjectId IN (${params.project}) AND a.InsertDate BETWEEN '${params.start}' AND '${params.end}' `
  if (params.channel) sql += `AND a.ChannelId = ${params.channel} `
  if (params.series) sql += `AND e.series = "${params.series}" `
  if (params.model) sql += `AND e.model = "${params.model}" `
  if (params.search) sql += 'AND (e.`name`' + ` LIKE '%${params.search}%' OR 
    e.series LIKE '%${params.search}%' OR e.model LIKE '%${params.search}%'
    OR e.descripition LIKE '%${params.search}%' OR e.type LIKE '%${params.search}%') `
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDBList = async (params) => {
  let sql = `SELECT a.*, b.ShopName,b.ShopURL,IFNULL(c.IsAuthor,0) AS IsAuthor,
  a.statisticsCount AS LinkCount, a.statisticsDay AS DayCount, d.BaseAmount, 
  d.TotalAmount,f.hasbrd, f.hasct, f.haspic
  FROM monitorlist a LEFT JOIN monitorshop b ON a.ShopId = b.ShopId 
  LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
  LEFT JOIN itemsales d ON a.AssetId = d.AssetID
  LEFT JOIN monitordata e ON e.skuid = a.SkuId
  LEFT JOIN monitorlable f ON a.AssetId = f.AssetID
  WHERE a.MarginPrice < '0' AND a.ProjectId IN (${params.project})
  AND InsertDate BETWEEN '${params.start}' AND '${params.end}' `
  if (params.channel) sql += `AND a.ChannelId = ${params.channel} `
  if (params.series) sql += `AND e.series = "${params.series}" `
  if (params.model) sql += `AND e.model = "${params.model}" `
  if (params.search) sql += 'AND (e.`name`' + ` LIKE '%${params.search}%' OR 
    e.series LIKE '%${params.search}%' OR e.model LIKE '%${params.search}%'
    OR e.descripition LIKE '%${params.search}%' OR e.type LIKE '%${params.search}%') `
  sql += `ORDER BY ${params.order} LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getLog = async (params) => {
  let sql = `SELECT * FROM datalog ORDER BY createdAt DESC LIMIT 1 `
  const sqlResult = await dataView(sql)

  return sqlResult
}

const addLog = async (params) => {
  let sql = 'CALL `pricemonitor`.`view_table`() '
  const sqlResult = await dataView(sql)

  return sqlResult
}

export default {
  getFakeList,
  getTotalItem,
  getDBCount,
  getDBList
}
