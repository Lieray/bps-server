import service from './dal'

const pageCount = async (params) => {
  const data = await service.getTotalItem(params)
  return data
}
const pageList = async (params) => {
  const data = await service.getFakeList(params)
  return data
}

const listCount = async (params) => {
  const data = await service.getDBCount(params)
  return data
}
const fakeList = async (params) => {
  const data = await service.getDBList(params)
  return data
}

export default {
  pageCount,
  pageList,
  listCount,
  fakeList
}
