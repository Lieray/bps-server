import service from './dal'
import moment from 'moment'

const monthFilter = async (params) => {
  const result = await service.getTotalMonth(params)
  return result
}

const dashboardItem = async (params) => {
  const query = {
    project: params.project
  }
  const lastMonth = moment(`${params.month}-01`).subtract(1, 'months').format('YYYY-MM')
  query.month = `'${lastMonth}','${params.month}'`
  const result = await service.getBaseItem(query)

  const defaultDocs = [{
    CurrentMonth: lastMonth,
    CurrentLink: 0,
    FakeLink: 0,
    FakeCount: 0
  },{
    CurrentMonth: params.month,
    CurrentLink: 0,
    FakeLink: 0,
    FakeCount: 0
  }]
  defaultDocs.forEach(element => {
    const doc = result.find(t => t.CurrentMonth === element.CurrentMonth)
    element.CurrentLink = doc? doc.CurrentLink : 0
    element.FakeLink = doc? doc.FakeLink : 0
    element.FakeCount = doc? doc.FakeCount : 0
  })
  return defaultDocs
}

const dashboardShop = async (params) => {
  const query = {
    project: params.project
  }
  const lastMonth = moment(`${params.month}-01`).subtract(1, 'months').format('YYYY-MM')
  query.month = `'${lastMonth}','${params.month}'`
  const result = await service.getBaseShop(query)
  const defaultDocs = [{
    CurrentMonth: lastMonth,
    FakeShop: 0,
    CurrentShop: 0,
    AuthShop: 0,
    OtherShop: 0
  },{
    CurrentMonth: params.month,
    FakeShop: 0,
    CurrentShop: 0,
    AuthShop: 0
    // OtherShop: 0
  }]
  defaultDocs.forEach(element => {
    const doc = result.find(t => t.CurrentMonth === element.CurrentMonth)
    element.FakeShop =  doc? doc.FakeShop : 0
    element.CurrentShop =  doc? doc.CurrentShop : 0
    element.AuthShop = doc? doc.AuthShop : 0
    // element.AuthShop =  element.CurrentShop - element.OtherShop
  })
  return defaultDocs
}

const dashboardDaily = async (params) => {
  const query = {
    project: params.project,
    start: `${params.month}-01`,
    end: moment(`${params.month}-01`).endOf('month').format("YYYY-MM-DD")
  }
  const result = await service.getDailyInfo(query)
  return result
}

const channelItem = async (params) => {
  const result = await service.getChannelItem(params)
  return result
}
const channelShop = async (params) => {
  const result = await service.getChannelShop(params)
  return result
}
const itemDetail = async (params) => {
  const result = await service.getBySeries(params)
  return result
}
const shopDetail = async (params) => {
  const cbData = [{
    ShopLab: '一般',
    Count: 0
  }, {
    ShopLab: '跨境',
    Count: 0
  }, {
    ShopLab: '代购',
    Count: 0
  }]
  const result = await service.getByShopLab(params)
  result.forEach( (currentValue) => {
    if(!currentValue.ShopLab) cbData[0].Count += currentValue.Count
    else if(currentValue.ShopLab === '跨境') cbData[1].Count += currentValue.Count
    else cbData[2].Count += currentValue.Count
  })

  return cbData
}

const itemList = async (params) => {
  const data = await service.getFakeLink(params)
  return data
}
const linkList = async (params) => {
  const data = await service.getDiscountList(params)
  return data
}
const shopList = async (params) => {
  const data = await service.getShopList(params)
  return data
}

const getShopDiscount = async (params) => {
  const cbData = [0,0,0,0,0]
  const result = await service.getShopDiscount(params)

  cbData.forEach( (_currentValue, index) => {
    const dataValue = result.find(ret => ret.statusLevel === `status${index}`)
    if (dataValue) cbData[index] = dataValue.cnt
  })

  return cbData
}

const getSkuDiscount = async (params) => {
  const cbData = [0,0,0,0,0]
  if (!params.month) params.month = moment(params.querymonth).subtract(1, 'months').format('YYYY-MM')
  const result = await service.getSkuDiscount(params)

  cbData.forEach( (_currentValue, index) => {
    const dataValue = result.find(ret => ret.statusLevel === `status${index}`)
    if (dataValue) cbData[index] = dataValue.cnt
  })

  return cbData
}

export default {
  monthFilter,
  dashboardItem,
  dashboardShop,
  dashboardDaily,
  channelItem,
  channelShop,
  itemDetail,
  shopDetail,
  itemList,
  linkList,
  shopList,
  getShopDiscount,
  getSkuDiscount
}
