import { dataView } from '../../../data'

const getTotalMonth = async (params) => {
  const sql = `SELECT CurrentMonth FROM v_product_month
    WHERE ProjectId = ${params.project}
    GROUP BY CurrentMonth ORDER BY CurrentMonth DESC`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getBaseItem = async (params) => {
  const sql = `SELECT * FROM v_product_month
  WHERE ProjectId = ${params.project} 
  AND CurrentMonth IN (${params.month})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getBaseShop = async (params) => {
  const sql = `SELECT * FROM v_shop_month
  WHERE ProjectId = ${params.project} 
  AND CurrentMonth IN (${params.month})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDailyInfo = async (params) => {
  const sql = `SELECT CurrentDate, SUM(ItemCount) AS ItemCount, 
    SUM(ShopCount) AS ShopCount FROM v_channel_year
    WHERE ProjectId = ${params.project} AND
    CurrentDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY CurrentDate ORDER BY CurrentDate ASC`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getChannelItem = async (params) => {
  const sql = `SELECT Channel, 
    SUM(IF(PriceStatus = 2,ItemCount,0)) AS FakeItem, 
    SUM(IF(PriceStatus < 2,ItemCount,0)) AS OtherItem
    FROM v_channel_month WHERE ProjectId = ${params.project}
    AND CurrentMonth = '${params.month}' GROUP BY Channel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getChannelShop = async (params) => {
  const sql = `SELECT Channel, 
  SUM(IF(PriceStatus = 2,ShopCount,0)) AS FakeShop, 
  SUM(IF(PriceStatus < 2,ShopCount,0)) AS OtherShop
  FROM v_channel_month WHERE ProjectId =  ${params.project} AND 
  CurrentMonth = '${params.month}' GROUP BY Channel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}
const getBySeries = async (params) => {
  const sql = `SELECT * FROM v_series_month WHERE 
    ProjectId = ${params.project} AND CurrentMonth = '${params.month}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getByAuth = async (params) => {
  const sql = `SELECT * FROM v_auth_month WHERE IsAuthor = 1 AND
    ProjectId = ${params.project} AND CurrentMonth = '${params.month}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getFakeLink = async (params) => {
  const sql = `SELECT m.*, o.TotalCount FROM ( SELECT a.SkuId, 
  Count(IF(b.ItemCount > 0,b.MonitorId,NULL)) AS TotalCount
  FROM monitorlist a LEFT JOIN v_monitor_count b ON 
  a.MonitorId = b.MonitorId WHERE a.ProjectId = ${params.project} 
  AND b.CurrentMonth = '${params.month}' GROUP BY a.SkuId) o LEFT JOIN 
  p_monitordata m ON m.skuid = o.SkuId ORDER BY o.TotalCount DESC LIMIT 10`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getDiscountList = async (params) => {
  const sql = `SELECT b.ProductName,b.URL,b.Channel,
  d.model AS Model,d.series AS Series,d.type AS ConfigValue,
  c.ShopName,c.ShopURL,IFNULL(e.IsAuthor,0) AS IsAuthor, a.* FROM v_discount_month a
  LEFT JOIN monitorlist b ON a.MonitorId = b.MonitorId
  LEFT JOIN monitorshop c ON b.ShopId = c.ShopId
  LEFT JOIN p_monitordata d ON b.SKUID = d.skuid
  LEFT JOIN authshop e ON b.ShopId = e.ShopId AND b.ProjectId = e.ProjectId
  WHERE a.MonitorPrice > 0 AND b.ProjectId = ${params.project}
  AND a.CurrentMonth = '${params.month}'
  ORDER BY a.MarginPercent ASC LIMIT 10`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getShopList = async (params) => {
  const sql = `SELECT o.*, m.Channel, IFNULL(e.IsAuthor,0) AS IsAuthor, m.ShopName, m.ShopURL FROM (
  SELECT a.ShopId, Count(IF(b.ItemCount > 0,a.MonitorId,NULL)) AS FakeLink,
  SUM(b.ItemCount) AS FakeCount, MIN(b.Discount) AS Discount
  FROM monitorlist a LEFT JOIN v_monitor_count b ON a.MonitorId = b.MonitorId
  WHERE a.ProjectId = ${params.project} AND b.CurrentMonth = '${params.month}'
  GROUP BY a.ShopId ORDER BY ${params.order} LIMIT 10) o LEFT JOIN 
  monitorshop m ON o.ShopId = m.ShopId 
  LEFT JOIN authshop e ON o.ShopId = e.ShopId AND e.ProjectId =  ${params.project}
  ORDER BY o.${params.order}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getShopDiscount = async (params) => {
  const sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
  'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
  count(ShopId) as cnt FROM (SELECT MIN(Discount) AS Discount, 
  ShopId FROM v_min_month WHERE ProjectId = ${params.project} AND 
  CurrentMonth = "${params.month}" GROUP BY ShopId) a group by statusLevel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getByShopLab = async (params) => {
  const sql = `SELECT ShopLab, Count(ShopId) AS Count FROM v_monitorshop_month
  WHERE ProjectId = ${params.project} AND CurrentMonth = '${params.month}'
  GROUP BY ShopLab`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getSkuDiscount = async (params) => {
  const sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
  'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
  count(ShopId) as cnt FROM (SELECT MIN(Discount) AS Discount, 
  ShopId FROM v_min_month WHERE SkuId = ${params.sku} AND 
  CurrentMonth = "${params.month}" GROUP BY ShopId) a group by statusLevel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  getTotalMonth,
  getBaseItem,
  getBaseShop,
  getDailyInfo,
  getChannelItem,
  getChannelShop,
  getBySeries,
  getByAuth,
  getFakeLink,
  getDiscountList,
  getShopList,
  getShopDiscount,
  getByShopLab,
  getSkuDiscount
}
