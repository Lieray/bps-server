/**
 * 配置可用模块
 */
import chart from './chart'
import filter from './filter'
import list from './list'
import daily from './daily'
import vendor from './vendor'
import info from './info'
import month from './month'

export default {
  chart,
  filter,
  list,
  daily,
  vendor,
  info,
  month
}
