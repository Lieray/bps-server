import { dataView } from '../../../data'

const getItemConfig = async (params) => {
  let sql = `SELECT * FROM monitorconfig WHERE configId IN (${params.id})`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getInfo = async (params) => {
  let sql = `SELECT * FROM monitordata WHERE sku = '${params.sku}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getWeek = async (params) => {
  let sql = `SELECT date_format(CurrentDate,'%Y-%m-%d') AS dateString, FinalPrice AS Price FROM monitorweek WHERE SkuId = '${params.skuid}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getItemFilter = async (params) => {
  let sql = `SELECT MonitorStatus AS 'Status', COUNT(MonitorId) AS 'Count' FROM (
    SELECT a.MonitorStatus,  a.MonitorId FROM monitorlist a
        LEFT JOIN monitorshop b ON a.ShopId = b.ShopId 
        LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
        WHERE `
  if (params.channel) sql += `b.Channel = '${params.channel}' AND `
  if (params.authorize) {
    if (params.authorize === "0") sql += `c.IsAuthor IS NULL AND `
    else sql += `c.IsAuthor = '${params.authorize}' AND `
  }
  if (params.search) sql += `(b.ShopName LIKE '%${params.search}%' OR a.ProductName LIKE '%${params.search}%')AND `
  sql += `a.SkuId = '${params.skuid}' ) c `
  if (params.status) sql += ` WHERE MonitorStatus = ${params.status} `
  sql += `GROUP BY MonitorStatus`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getByChannel = async (params) => {
  const sql = `SELECT Channel FROM monitorlist
  WHERE SkuId = '${params.skuid}' GROUP BY Channel`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getItemCount = async (params) => {
  let sql = `SELECT COUNT(a.MonitorId) AS Count FROM monitorlist a
    LEFT JOIN monitorshop b ON a.ShopId = b.ShopId 
    LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
    WHERE `
  if (params.channel) sql += `b.Channel = '${params.channel}' AND `
  if (params.authorize) {
    if (params.authorize === "0") sql += `c.IsAuthor IS NULL AND `
    else sql += `c.IsAuthor = '${params.authorize}' AND `
  }
  if (params.search) sql += `(b.ShopName LIKE '%${params.search}%' OR a.ProductName LIKE '%${params.search}%') AND `
  if (params.status) sql += ` a.MonitorStatus = ${params.status}  AND `
  sql += `a.SkuId = '${params.skuid}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}
const getItemList = async (params) => {
  let sql = `SELECT a.*, b.ShopName,b.ShopURL,IFNULL(c.IsAuthor,0) AS IsAuthor, 
    d.BaseAmount, d.TotalAmount, e.hasbrd, e.hasct, e.haspic FROM monitorlist a 
    LEFT JOIN monitorshop b ON a.ShopId = b.ShopId 
    LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
    LEFT JOIN itemsales d ON a.AssetId = d.AssetID 
    LEFT JOIN monitorlable e ON a.AssetId = e.AssetID WHERE `
  if (params.channel) sql += `b.Channel = '${params.channel}' AND `
  if (params.authorize) {
    if (params.authorize === "0") sql += `c.IsAuthor IS NULL AND `
    else sql += `c.IsAuthor = '${params.authorize}' AND `
  }
  if (params.search) sql += `(b.ShopName LIKE '%${params.search}%' OR a.ProductName LIKE '%${params.search}%') AND `
  if (params.status) sql += ` a.MonitorStatus = ${params.status} AND `
  sql += `a.SkuId = '${params.skuid}' ` 
  if (params.order) sql += `Order BY ${params.order} ` 
  sql += `LIMIT ${params.skip}, ${params.size}`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getItemInfo = async (params) => {
  const sql = `SELECT a.*, b.ShopName AS Seller, b.ShopURL AS ShopUrl,IFNULL(c.IsAuthor,0) AS IsAuthor FROM 
  monitorlist a LEFT JOIN monitorshop b ON a.ShopId = b.ShopId 
  LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
  WHERE a.MonitorId = '${params.id}'`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getcurrentDetail = async (params) => {
  const sql = `SELECT InsertDate AS CurrentDate, FinalPrice, MonitorPrice 
  FROM monitorfilter WHERE MonitorId = '${params.id}' AND InsertDate > (curdate() - interval 24 HOUR)`
const sqlResult = await dataView(sql)
return  sqlResult
}

const getweekDetail = async (params) => {
  let sql = `SELECT CurrentDate, FinalPrice, MonitorPrice FROM monitorprice
  WHERE MonitorId = '${params.id}' AND CurrentDate between 
  (curdate() - interval 7 day) AND (curdate() - interval 1 day)`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getcurrentList = async (params) => {
  let sql = `SELECT a.*, IFNULL(c.IsAuthor,0) AS IsAuthor,b.ShopName,b.ShopURL FROM monitorfilter a
  LEFT JOIN monitorshop b ON a.ShopId = b.ShopId
  LEFT JOIN authshop c ON a.ShopId = c.ShopId AND a.ProjectId = c.ProjectId
  WHERE a.MonitorId = '${params.id}' ORDER BY InsertDate DESC LIMIT 10`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getcurrentLink = async (params) => {
  let sql = `SELECT Created FROM monitortask WHERE 
  monitorId = '${params.id}' ORDER BY Created DESC LIMIT 10`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getStatusLevel = async (params) => {
  // let sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
  // 'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
  // count(MonitorId) as cnt FROM (SELECT MIN(o.MarginPercent) AS Discount, 
  // o.MonitorId FROM monitorfilter o JOIN monitorlist r ON o.MonitorId = r.MonitorId
  // WHERE o.PriceStatus = 2 AND o.SkuId = ${params.skuid} 
  // AND o.InsertDate BETWEEN '${params.start}' AND '${params.end}'
  // GROUP BY  o.MonitorId) a group by statusLevel`


  const sql = `SELECT elt(INTERVAL ( Discount, 0, 0.2, 0.4, 0.6, 0.8, 1),
    'status0', 'status1', 'status2', 'status3', 'status4') AS statusLevel,
    count(MonitorId) as cnt FROM (SELECT MIN(MarginPercent) AS Discount, 
    MonitorId FROM monitorlist WHERE MarginPrice < '0' AND SkuId = ${params.skuid}
    GROUP BY MonitorId) a group by statusLevel`

  const sqlResult = await dataView(sql)
  return  sqlResult
}

const getLastDate = async (params) => {
  const sql = `SELECT InsertDate AS 'CurrentDate' FROM monitorlist
  WHERE SkuId = ${params.skuid} ORDER BY InsertDate DESC LIMIT 1`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  getItemConfig,
  getWeek,
  getInfo,
  getItemFilter,
  getItemCount,
  getItemList,
  getItemInfo,
  getcurrentDetail,
  getweekDetail,
  getcurrentList,
  getcurrentLink,
  getByChannel,
  getStatusLevel,
  getLastDate
}