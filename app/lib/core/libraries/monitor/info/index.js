import service from './dal'
import Moment from 'moment'

const getItemConfig = async (params) => {
  const data = await service.getItemConfig(params)
  return data
}

const pageInfo = async (params) => {
  const data = await service.getInfo(params)
  return data
}

const getItemCount = async (params) => {
  const data = await service.getItemCount(params)
  return data
}

const getItemFilter = async (params) => {
  const data = await service.getItemFilter(params)
  return data
}

const getItemList = async (params) => {
  const data = await service.getItemList(params)
  return data
}
const getItemInfo = async (params) => {
  const data = await service.getItemInfo(params)
  return data
}

const weekPrice = async (params) => {
  const data = await service.getWeek(params)
  return data
}

const filterChannel = async (params) => {
  const data = await service.getByChannel(params)
  return data
}

const currentDetail = async (params) => {
  const query = {
    id: params.id
  }
  const data = await service.getcurrentDetail(query)
  return data
}

const weekDetail = async (params) => {
  const data = await service.getweekDetail(params)
  return data
}

const currentList = async (params) => {
  const data = await service.getcurrentList(params)
  return data
}

const currentLink = async (params) => {
  const data = await service.getcurrentLink(params)
  const cdDocs = []
  data.forEach(element => {
    const filepath = Moment(element.Created).format('YYYYMMDD')
    const filename  = Moment(element.Created).format('YYYYMMDDHH')
    cdDocs.push(`/${filepath}/${params.id}/${filename}00.png`)
  })
  return cdDocs
}

const getStatusLevel = async (params) => {
  const cbData = [0,0,0,0,0]
  const result = await service.getStatusLevel(params)

  cbData.forEach( (_currentValue, index) => {
    const dataValue = result.find(ret => ret.statusLevel === `status${index}`)
    if (dataValue) cbData[index] = dataValue.cnt
  })

  return cbData
}

const getLastDate = async (params) => {
  const defaultDate = {
    end: Moment().endOf('day')
  }
  const data = await service.getLastDate(params)
  if (data.length && Moment(data[0].CurrentDate)) {
    defaultDate.end = Moment(data[0].CurrentDate).format('YYYYMMDD')
  }
  defaultDate.start = defaultDate.end
  defaultDate.end = Moment(defaultDate.end).add(1, 'days').format('YYYYMMDD')
  return defaultDate
}

export default {
  getItemConfig,
  getItemFilter,
  getItemCount,
  getItemList,
  getItemInfo,
  weekPrice,
  pageInfo,
  currentDetail,
  weekDetail,
  currentList,
  currentLink,
  filterChannel,
  getStatusLevel,
  getLastDate
}