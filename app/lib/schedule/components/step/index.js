import service from './dal'

const getSub = async () => {
  const data = await service.getSub()
  return data
}

const getItem = async (pid) => {
  const data = await service.getItem(pid)
  return data
}

const getShop = async (pid) => {
  const data = await service.getShop(pid)
  return data
}

const getDetail = async (docs) => {
  const data = await service.getDetail(docs)
  return data
}

const getChannel = async (docs) => {
  const data = await service.getChannel(docs)
  return data
}

const setNote = async (docs) => {
  const data = await service.setNote(docs)
  return data
}

const setCount = async (docs) => {
  const data = await service.setCount(docs)
  return data
}

const setMonth = async (docs) => {
  const data = await service.setMonth(docs)
  return data
}

export default {
  getSub,
  getItem,
  getShop,
  getDetail,
  getChannel,
  setNote,
  setCount,
  setMonth
}