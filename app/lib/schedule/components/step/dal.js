import { dataView } from '../../../core/data'

// 生成每日统计
const setCount = async () => {
  const sql = 'CALL daily_report()'
  dataView(sql)
  return
}

const setMonth = async () => {
  const sql = 'CALL month_view()'
  dataView(sql)
  return
}

// 获取所有Task的内容
const getSub = async () => {
  const sql = 'SELECT * FROM taskinfo'
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getItem = async (pid) => {
  const sql = `SELECT * FROM noteitem WHERE ProjectId = ${pid} ORDER BY MarginPercent `
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getShop = async (pid) => {
  const sql = `SELECT * FROM noteshop WHERE ProjectId = ${pid} ORDER BY discount `
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getDetail = async (docs) => {
  const sql = `SELECT * FROM v_notify_daily WHERE ProjectId = ${docs.pid} AND CurrentDate = ' ${docs.cdate}' `
  const sqlResult = await dataView(sql)
  return sqlResult
}

const getChannel = async (docs) => {
  const sql = `SELECT * FROM v_channel_daily WHERE ProjectId = ${docs.pid} AND CurrentDate = ' ${docs.cdate}' `
  const sqlResult = await dataView(sql)
  return sqlResult
}

// 插入通知
const setNote = async (docs) => {
  let sql = 'INSERT INTO notification (`content`, `mainid`, `status`, `isdel`, `createdAt`, `createdBy`, `type`) VALUES '
  sql += `("${docs.content}", ${docs.mainid}, 0, 0,"${docs.cdate}" , 0, 2)`
  const sqlResult = await dataView(sql)
  return sqlResult
}

// 生成邮件

export default {
  getSub,
  getItem,
  getShop,
  getDetail,
  getChannel,
  setNote,
  setCount,
  setMonth
}
