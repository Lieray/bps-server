import daily from './daily'
import step from './step'

export default {
    daily,
    step
}