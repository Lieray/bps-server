import { dataView } from '../../../core/data'

const dailyShop = async (params) => {
  const sql = `CALL Daily_Shop(${params.project},'${params.start}','${params.end}') `
  const sqlResult = await dataView(sql)
  return  sqlResult[0]
}
  
const dailyInfo = async (params) => {
  const sql = `CALL Daily_Product(${params.project},'${params.start}','${params.end}') `
  const sqlResult = await dataView(sql)
  return  sqlResult[0]
}

const otherInfo = async (params) => {
  const sql = `SELECT 
    g.monitorId AS '编码',
    d.series AS '系列',
    g.ProductName AS '产品名称',
    d.type AS '参数',
    g.URL AS '产品链接',
    g.Channel AS '渠道',
    e.ShopName AS '店铺名称',
    e.ShopURL AS '店铺链接',
    e.ShopLab AS '店铺类型',
    d.model AS '型号',
    IF(f.IsAuthor = 1,'是','否') AS '授权',
    g.CurrentPrice AS '页面价格',
    g.FinalPrice AS '券后价格',
    g.MonitorPrice AS '检测价格',
    g.MarginPrice AS '价差',
    round(g.MarginPrice/g.MonitorPrice,4) AS '价差百分比',
    date_format(g.InsertDate,'%Y-%m-%d') AS '最近更新时间',
    IF(h.hasct = 1,'是','否') AS '明显假冒',
		IF(h.hasbrd = 1,'是','否') AS '滥用商标词',
		IF(h.haspic = 1,'是','否') AS '盗图'
    FROM  monitorlist g
    LEFT JOIN monitordata d ON g.SkuId = d.skuid
    LEFT JOIN monitorshop e ON g.ShopId = e.ShopId
    LEFT JOIN authshop f ON g.ShopId = f.ShopId AND  f.ProjectId = g.ProjectId
    LEFT JOIN monitorlable h ON g.MonitorId = h.AssetID
    WHERE g.ProjectId = ${params.project} AND g.MonitorId NOT IN (
    SELECT MonitorId FROM monitorfilter
    WHERE PriceStatus = 2 AND ProjectId = ${params.project}
    AND InsertDate BETWEEN '${params.start}' AND '${params.end}'
    GROUP BY MonitorId
    ) ORDER BY g.MarginPercent ASC`
  const sqlResult = await dataView(sql)
  return  sqlResult
}

const linkInfo = async (params) => {
  let sql = 'SELECT * FROM linkinfo WHERE `项目编号` = '
  sql += `'${params.project}'`
  sql +=  'ORDER BY `检测序列`'
  const sqlResult = await dataView(sql)
  return  sqlResult
}

export default {
  dailyShop,
  dailyInfo,
  otherInfo,
  linkInfo
}
