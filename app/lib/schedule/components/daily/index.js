import service from './dal'
import moment from 'moment'
import fetch from 'node-fetch'

const serverIP = async () => {
  return new Promise((resolve, reject) => {
    fetch('http://firewall.simplybrand.io/Index.aspx?action=get&name=simply', 
    { credentials: 'include', headers: { 'Content-Type': 'text/plain; charset=UTF-8', Connection: 'keep-alive' } }
    ).then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

const deateChecker = async () => {
  const data = {
    project: 1,
    start:moment().subtract(1, 'days').format("YYYY-MM-DD"),
    end:moment().format("YYYY-MM-DD")
  }
  return data
}

const shopStatistics = async (params) => {
  const data = await service.dailyShop(params)
  return data
}
  
const itemStatistics = async (params) => {
  const data = await service.dailyInfo(params)
  return data
}

const otherStatistics = async (params) => {
  const data = await service.otherInfo(params)
  return data
}
  
const linkInfo = async (params) => {
  const data = await service.linkInfo(params)
  return data
}

export default {
  serverIP,
  deateChecker,
  shopStatistics,
  itemStatistics,
  otherStatistics,
  linkInfo
}
