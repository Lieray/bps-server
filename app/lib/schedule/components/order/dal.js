import { dataAuth } from '../../../core/data'

const setStatus = async (params) => {
  const sql = 'UPDATE monitor SET status = ' + `${params.status} WHERE orderId IN (${params.id})`
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

export default {
  setStatus
}
