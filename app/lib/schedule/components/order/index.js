import service from './dal'

const setStatus = async (params) => {
    const data = await service.setStatus(params)
    return data
}

export default {
    setStatus
}
