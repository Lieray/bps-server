import service from './dal'

const getSub = async () => {
  const data = await service.getSub()
  return data
}

const getUser = async (pid) => {
  const data = await service.getUser(pid)
  return data
}

export default {
  getSub,
  getUser
}
