import { dataAuth } from '../../../core/data'

// 获取当前有效套餐
const getSub = async () => {
  const sql = 'SELECT * FROM subinfo'
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

// 获取所有需要发邮件的邮箱
const getUser = async () => {
  const sql = 'SELECT * FROM usereamil'
  const sqlResult = await dataAuth(sql)
  return sqlResult
}

export default {
  getSub,
  getUser
}