import fileset from './files'
import noteset from './sub'
import orderset from './order'
import schedule from 'node-schedule'
 
const  scheduleCronstyle = ()=>{
  // 限定PM2的进程
  if (Number.parseInt(process.env.NODE_APP_INSTANCE) === 0) {

    schedule.scheduleJob('30 20 0 1 * *',()=>{
      fileset.monthCreated()
    }); 
  
    schedule.scheduleJob('30 10 1 * * *',()=>{
      fileset.dataCreated()
    }); 
  
    schedule.scheduleJob('30 30 1 * * *',()=>{
      noteset.noteCreated()
    }); 
  
    schedule.scheduleJob('30 10 2 * * *',()=>{
      fileset.fileCreated()
    }); 
  
    schedule.scheduleJob('30 30 9 * * *',()=>{
      noteset.noteSend()
    }); 

    schedule.scheduleJob('30 30 12 * * *',()=>{
      orderset.setStatus()
    }); 

    schedule.scheduleJob('30 55 23 * * *',()=>{
      orderset.setStatus()
    }); 

  }
}
  
scheduleCronstyle();

export default {
    fileset,
    noteset,
    orderset
}
