import moment from 'moment'
import service from '../components/step'
import servicedata from '../components/subscribe'
import util from './email'

const textFormat = (docs) => {
  let itemChannel = ''
  let shopChannel = ''
  let TotalItem = 0
  let TotalShop = 0
  docs.channelInfo.forEach(infodoc => {
    TotalItem += Number.parseInt(infodoc.ItemCount)
    TotalShop += Number.parseInt(infodoc.ShopCount)
    itemChannel += infodoc.Channel + (docs.type? `<span>${infodoc.ItemCount}</span>条，`: ` ${infodoc.ItemCount}条
    `)
    shopChannel += infodoc.Channel + (docs.type? `<span>${infodoc.ShopCount}</span>家，`: ` ${infodoc.ShopCount}家
    `)
  })
  const textArray = [
    docs.ReportDate,
    docs.name,
    docs.type? `<span>${TotalItem}</span>`: TotalItem,
    itemChannel,
    docs.type? `<span>${docs.LowCount}</span>`: docs.LowCount,
    docs.type? `<span>${TotalShop}</span>`: TotalShop,
    shopChannel,
    docs.type? `<span>${(docs.Discount* 100).toFixed(2)}</span>`: (docs.Discount* 100).toFixed(2),
    docs.type? `<span>${docs.LowChannel}</span>`: docs.LowChannel,
    docs.type? `<span>${docs.LowShop}</span>`: docs.LowShop,
    docs.type? `<span>${docs.ItemChannel}</span>`: docs.ItemChannel,
    docs.type? `<span>${docs.ItemShop}</span>`: docs.ItemShop,
    docs.type? `<span>${docs.ItemCount}</span>`: docs.ItemCount
  ]

  if(docs.type) {
    const textcontent = `【低价日报】${textArray[0]}日您的订阅${textArray[1]}总共有${textArray[2]}条商品链接发生了低价，`
    +`各渠道低价情况如下：${textArray[3]}低价折扣力度在60%以下的链接共有${textArray[4]}条。发生低价行为的卖家一共有${textArray[5]}家，`
    +`各渠道分布情况如下：${textArray[6]}其中最大折扣为${textArray[7]}%，卖家为${textArray[8]}平台的${textArray[9]}；`
    +`低价链接数量最多的卖家为${textArray[10]}平台的${textArray[11]}，店铺内有${textArray[12]}条链接发生了低价。`
    +`更多数据信息请及时前往BPS平台（https://bps.simplybrand.com/）查看。`
    return textcontent
  } else {
    const textcontent = `
    尊敬的${textArray[1]}用户您好！

    以下是您订阅的【低价日报】总结:
    总共监测到 ${textArray[2]}条商品链接发生了低价
    ${textArray[3]}折扣力度在 60%以下的商品链接共有 ${textArray[4]}条。
    
    总共监测到 ${textArray[5]}家卖家发生了低价行为
    ${textArray[6]}其中最大折扣为 ${textArray[7]}%，卖家为 ${textArray[10]}平台的 ${textArray[11]}；
    低价商品链接数量最多的卖家为 ${textArray[10]}平台的 ${textArray[11]}，店铺内有 ${textArray[12]}条链接发生了低价。
    
    更多数据信息请及时前往BPS平台查看: https://bps.simplybrand.com/#/Login
    
    simplyBrand BPS
    系统服务邮件`
    return textcontent
  }
}

const noteCreated = async () => {
  const taskData = await servicedata.getSub()
  const dateset = moment().subtract(1, 'days').format('YYYY-MM-DD')

  for(let taskdoc of taskData){
    const pid = taskdoc.taskid
    // 生成通知
    const taskDaily = await service.getDetail({pid,cdate: dateset})
    const channelDaily = await service.getChannel({pid,cdate: dateset})
    const textDocs = {
      ReportDate: dateset,
      name: taskdoc.name,
      type: 1
    }
    const textcontent = textFormat(Object.assign(textDocs,taskDaily[0],{channelInfo: channelDaily}))
    await service.setNote({content:textcontent,mainid: taskdoc.mainId,cdate:(moment().format('YYYY-MM-DD'))+' 00:30:00'})
  }
}

const noteSend = async () => {
  const taskData = await servicedata.getUser()
  const dateset = moment().subtract(1, 'days').format('YYYY-MM-DD')
  for(let taskdoc of taskData){
    const pid = taskdoc.taskid
    // 生成通知
    const taskDaily = await service.getDetail({pid,cdate: dateset})
    const channelDaily = await service.getChannel({pid,cdate: dateset})
    const textDocs = {
      ReportDate: dateset,
      name: taskdoc.name,
      type: 0
    }
    const textcontent = textFormat(Object.assign(textDocs,taskDaily[0],{channelInfo: channelDaily}))
    // 数据库中录入的邮箱是JSON化的数组，需要解析
    const docemail = JSON.parse(taskdoc.notifier.replace('"[','[').replace(']"',']'))
    const filename = taskdoc.name.indexOf('/') > 0 ? taskdoc.name.split('/')[1] : taskdoc.name
    for(let usermail of docemail){
      await util.snedfile({
        email:usermail,
        tittle:`【低价日报】${dateset}日 ${taskdoc.name}`,
        content:textcontent,
        file:`${taskdoc.name.replace(/\\/g,'').replace(/\//g,'')}_${dateset.replace(/-/g,'')}日报.xlsx`,
        filename: `${filename}_Daily_Report_${dateset.replace(/-/g,'')}.xlsx`
      })
    }
  }
}


export default {
  noteCreated,
  noteSend
}
