import fetch from 'node-fetch'
import nconf from 'nconf'
import moment from 'moment'
import service from '../components/order'

const checkOrder = async (params) => {
  const kfInfo = await nconf.get('kfInfo')
  const code = kfInfo.token

  return new Promise((resolve, reject) => {
    fetch(`https://simplybrand.kf5.com/apiv2/tickets.json?updated_start=${params.nowdate}&page=${params.page}`, { 
      method: 'get',
      credentials: 'include', 
      headers: { 
        'Authorization': `Basic ${code}`,
        'Content-Type': 'application/json; charset=UTF-8'
      } 
    }).then(res => {
      if (res.ok) return res.text()
      else throw new Error('Error happens with the status code: 504, the key is: SMS')
    }).then(resbody => {
      resolve(resbody)
    }).catch(err => reject(err))
  })
}

const getpagedata = async (params) => {
  const orderString = await checkOrder(params)
  const orderInfo = JSON.parse(orderString)
  return orderInfo
}

const checkdata = async(docs) => {
  // const orderStatus = ["new","open", "pending", "solved", "closed"]
  const dealdocs = []
  const solvedocs = []
  const closedocs = []
  for (let doc of docs) {
    switch(doc.status) {
      case 'closed':
        closedocs.push(doc.id)
        break
      case 'solved':
        solvedocs.push(doc.id)
        break
      case 'new':
        break
      default:
        dealdocs.push(doc.id)
    } 
  }
  if(dealdocs.length) await service.setStatus({status:3, id:dealdocs.toString()})
  if(solvedocs.length) await service.setStatus({status:4, id:solvedocs.toString()})
  if(closedocs.length) await service.setStatus({status:5, id:closedocs.toString()})
 return 
}

const setStatus  = async () => {
  const params = {
    nowdate: moment().subtract(1, 'days').format('YYYY-MM-DD') + ' 00:00:00',
    page: 1
  }

  let pagenumber = 1
  let docdata = []
  for (let index = 0; index < pagenumber; index ++) {
    const orderInfo = await getpagedata(params)
    docdata = orderInfo.tickets
    await checkdata(docdata)
    pagenumber = orderInfo.next_page
    params.page = pagenumber
  }
  return docdata
}

export default {
  setStatus
}