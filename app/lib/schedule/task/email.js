/* eslint no-console: ["error", { allow: ["log"] }] */
import path from 'path'
import nodemailer from 'nodemailer'
import nconf from 'nconf'

const logger = global.logger

const transporter = async () => {
  const emailConfig = await nconf.get('email')
  return nodemailer.createTransport({
    host: 'hwsmtp.exmail.qq.com',
    port: 465,
    secure: true,
    auth: {
      type: 'login',
      user: process.env.EMAIL_USER ? process.env.EMAIL_USER : emailConfig.username,
      pass: process.env.EMAIL_PASSWORD ? process.env.EMAIL_PASSWORD : emailConfig.password
    }
  })
}


/**
 * Activate account
 * @param {string} email
 * @param {string} uuid
 * @param {string} token
 */
const snedfile = async (docs) => {
  try {
    // const reg = /^[A-Za-z0-9]+([-_.][A-Za-z0-9]+)*@([A-Za-z0-9]+[-.])+[A-Za-z0-9]{2,5}$/;
    // if (!mail || !mail.match(reg)) return

    const emailConfig = await nconf.get('email')
    const text = docs.content
    const mailOptions = {
      from: `"simplyBrand BPS email service" <${emailConfig.username}>`,
      to: docs.email,
      subject: docs.tittle,
      text: text,
      attachments:[
        {
            //当前目录下的文件
            filename:docs.filename,
            path:path.join(__dirname,`../../../../static/files/${docs.file}`)
        }
      ]
    }
  
    const emailTransporter = await transporter()
  
    emailTransporter.verify((error, success) => {
      if (error) {
        logger.info(error)
      } else {
        logger.debug('Server is ready to take our messages ' + success)
      }
    })
    const info = await emailTransporter.sendMail(mailOptions)
    logger.debug('[EMAIL] Message %s sent: %s', info.messageId, info.response)
  } catch (e) {
    logger.error(
      `Failed!\n EADDRINUSE 
      ${e.stack}`
    )
  } finally {
    return 
  }

}

export default {
  snedfile
}
