import path from 'path'
import XLSX from 'xlsx'
import moment from 'moment'
import service from '../components/step'

const shopStatistics = (docs) => {
  const xlsxObj = [
  ['平台','店铺','是否授权','当天店铺破价商品数','当天店铺商品异常次数','折扣']
  ]
  docs.forEach(element => {
  xlsxObj.push([
      element['Channel'],
      element['shopName'],
      element['isAuthor'],
      element['fakeLink'],
      element['fakeCount'],
      (element['discount']*100).toFixed(2) + '%'
  ])
  })
  return xlsxObj
}
  
const itemStatistics = (docs) => {
  const xlsxObj = [
      ['AssetID','系列/Series','产品名称/Product Name','参数/Spec','产品链接/Product_URL','渠道/Platform',
      '店铺名称/StoreName','型号/Model','授权/Authorization','页面价格/PagePrice',
      '券后价格/DiscountedPrice','检测价格/SetPrice','价差/PriceDiff','折扣率/Discount%', '降价率/%Off',
      '最近低价时间/Last Price Break Time','近期异常次数/Issue times','近期异常天数/Issue days']
      ]
  docs.forEach(element => {
      xlsxObj.push([
          element['monitorId'],
          element['series'],
          element['ProductName'],
          element['type'],
          element['URL'],
          element['Channel'],
          element['ShopName'],
          element['model'],
          element['IsAuthor'],
          element['CurrentPrice'],
          element['FinalPrice'],
          element['MonitorPrice'],
          element['MarginPrice'],
          (element['FinalPrice']/element['MonitorPrice']*100).toFixed(2) + '%',
          (100 - element['FinalPrice']/element['MonitorPrice']*100).toFixed(2)  + '%',
          element['InsertDate'],
          element['currentCount'],
          element['currentDay']
      ])
      })
  return xlsxObj
}

const fileCreated = async () => {
  const taskData = await service.getSub()
  const dateset = moment().subtract(1, 'days').format('YYYY-MM-DD')

  for(let taskdoc of taskData){
    const pid = taskdoc.taskid
    // 生成Excel
    const shopData = await service.getShop(pid)
    const shopdocs = shopStatistics(shopData)
    const itemData =await service.getItem(pid)
    const itemdocs = itemStatistics(itemData)
    const book = XLSX.utils.book_new()
    const shopsheet = XLSX.utils.aoa_to_sheet(shopdocs)
    XLSX.utils.book_append_sheet(book, shopsheet, 'Summary')
    const itemsheet = XLSX.utils.aoa_to_sheet(itemdocs)
    XLSX.utils.book_append_sheet(book, itemsheet, '链接统计')
    const filename = `${taskdoc.name.replace(/\\/g,'').replace(/\//g,'')}_${dateset.replace(/-/g,'')}日报.xlsx`
    await XLSX.writeFile(book,  path.join(__dirname,`../../../../static/files/${filename}`))
  }
}

const dataCreated = async () => {
  service.setCount()
}

const monthCreated = async () => {
  service.setMonth()
}


export default {
  fileCreated,
  dataCreated,
  monthCreated
}