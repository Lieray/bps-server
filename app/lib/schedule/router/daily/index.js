/**
 * 模块访问的入口
 * 该模块不进行任何逻辑处理
 */
'use strict'
import express from 'express'
import controller from './controller'
const baseUri = '/V0'
const tempUri = '/V1'
const currentUri = '/V3'


const router = express.Router()

router.get(baseUri + '/daily/statistics/ip', controller.tempIP)
router.get(baseUri + '/daily/statistics/shop', controller.shopStatistics)
router.get(baseUri + '/daily/statistics/item', controller.itemStatistics)
router.get(tempUri + '/daily/statistics/link', controller.linkStatistics)
router.get(currentUri + '/price/statistics/item', controller.itemInfo)
// router.get(currentUri + '/price/statistics/file', controller.fileStep)
// router.get(currentUri + '/price/statistics/note', controller.noteStep)
// router.get(currentUri + '/price/statistics/email', controller.emailStep)
router.get(currentUri + '/price/statistics/status', controller.setStatus)

export default router
