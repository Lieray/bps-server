import service from '../../../core/libraries'
import servicedata from '../../components'
import taskstep from '../../task'

const checkProject = async (params) => {
  let userProject = []
  // 检查项目的具体内容
  const policy = await service.subscribe.info.getInfo({subid: params.project, mainId: params.mainid})
  policy.forEach(element => {
    if (element.taskid) {
      userProject.push(element.taskid)
    }
  })
  if (userProject.length) {
    const cbData = {
      project: userProject.toString(),
      subname: policy[0].name
    }

    // const defaultData = await servicedata.daily.deateChecker()
    const defaultDate = await service.monitor.filter.getLastDate(cbData)
    cbData.start = params.start || defaultDate.start
    cbData.end = params.end || defaultDate.end
    return cbData
  } else throw new Error('Can not find the project!')
}


const checkdate = async (params) => {
  const defaultData = await servicedata.daily.deateChecker()
  const defaultDocs = {
    project: params.project || defaultData.project,
    start: params.start || defaultData.start,
    end: params.end || defaultData.end
  }

  return defaultDocs
}

const fileStep = async () => {
  await taskstep.fileset.fileCreated()
  return
}

const noteStep = async () => {
  await taskstep.noteset.noteCreated()
  return
}

const emailStep = async () => {
  await taskstep.noteset.noteSend()
  return
}

const tempIP = async () => {
  const data = await servicedata.daily.serverIP()
  return data
}

const shopStatistics = async (params) => {
  const data = await servicedata.daily.shopStatistics(params)
  return data
}

const itemStatistics = async (params) => {
  const data = await servicedata.daily.itemStatistics(params)
  return data
}

const otherStatistics = async (params) => {
  const data = await servicedata.daily.otherStatistics(params)
  return data
}

const checkTemp = async (params) => {
  const defaultData = await service.price.daily.deateChecker()
  const defaultDocs = {
    project: params.project || defaultData.project,
    start: params.start || defaultData.start,
    end: params.end || defaultData.end
  }

  return defaultDocs
}

const linkInfo = async (params) => {
  const data = await servicedata.daily.linkInfo(params)
  return data
}

const baseData = async (params) => {
  const cdate = await service.monitor.filter.getUpdateTime(params)
  const lastDateTime = JSON.stringify(cdate[0].CurrentDate).split('T')
  const lastDate = lastDateTime[0].replace('"','').split('-')
  const lastTime = lastDateTime[1].split(':')
  return params.subname + lastDate[0]+ lastDate[1]+ lastDate[2]+ lastTime[0]+ lastTime[1]
}


const setStatus = async () => {
  const orderDocs = await taskstep.orderset.setStatus()
  return orderDocs
}

export default {
  fileStep,
  noteStep,
  emailStep,
  tempIP,
  checkdate,
  shopStatistics,
  itemStatistics,
  otherStatistics,
  checkTemp,
  checkProject,
  linkInfo,
  baseData,
  setStatus
}