/**
 * 配置可用模块
 */
import daily from './daily'

/**
 * ToDo
 * 建立目录，直观表现每个模块注册的路由
 */

const routers = Object.assign({daily})

export default routers