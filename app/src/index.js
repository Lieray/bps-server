'use strict'
import serverHandle from './handle'
import app from './server'

export {
  app,
  serverHandle
}
