'use strict'

const logger = global.logger

/**
 * 路由错误
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const pageNotFound = (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  if (req.xhr) {
    const errMessage = `StatusCode: 404 Not Found, path: ${req.method} ${req.originalUrl}, from: ${req.ip}`
    logger.verbose(errMessage)
    res.status(404).end('Path Not Exist!')
  }
  next()
}

/**
 * 服务器端的错误
 * @param {*} err
 * @param {*} req
 * @param {*} _res
 * @param {*} next
 */

const serverError = (err, req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  logger.verbose(`StatusCode: ${res.statusCode}, path: ${req.method} ${req.originalUrl}, error :${err.stack} `)
  if (!res.statusCode) {
    const errMessage = `StatusCode: 500 Server Error, path: ${req.method} ${req.originalUrl}, error :${err.stack}`
    logger.warn(errMessage)
    res.status(500).end('Server Error!')
  }
  next()
}

/**
 * 服务器端的无响应
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const serverTimeout = (req, res, next) => {
  res.setHeader('Content-Type', 'application/json')
  if (!req.timedout) next()
  else {
    const errMessage = `StatusCode: 503 Server Timeout, path: ${req.method} ${req.originalUrl}, query :${req.query}, params :${req.body}`
    logger.warn(errMessage)
    res.status(503).end('Request Timeout!')
  }
}

export default {
  pageNotFound,
  serverError,
  serverTimeout
}
