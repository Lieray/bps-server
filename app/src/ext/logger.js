'use strict'

import path from 'path'
import moment from 'moment'
import winston from 'winston'

const current = moment().format('YYYYMMDD')
const logConfiguration = {
  test: {
    logsConsole: {
      level: 'silly'
    },
    logsRecord: {
      level: 'debug',
      filename: path.join(__dirname, `../../../logs/test_${current}.log`)
    }
  },
  development: {
    logsConsole: {
      level: 'debug'
    },
    logsRecord: {
      level: 'verbose',
      filename: path.join(__dirname, `../../../logs/dev_${current}.log`)
    }
  },
  production: {
    logsConsole: {
      level: 'info'
    },
    logsRecord: {
      level: 'warn',
      maxsize: 5242880, // 5MB
      filename: path.join(__dirname, `../../../logs/prd_${current}.log`)
    }
  }
}
if (!process.env.NODE_ENV ||
  !Object.prototype.hasOwnProperty.call(logConfiguration, process.env.NODE_ENV)) {
  winston.error(new Error('Crashed!\n Undefined NODE_ENV!'))
  process.exit(0)
}

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console(
      logConfiguration[process.env.NODE_ENV].logsConsole
    ),
    new winston.transports.File(
      logConfiguration[process.env.NODE_ENV].logsRecord
    )
  ],
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.printf((info) =>
      `${moment(info.timestamp).format('YYYY-MM-DD HH:mm:ss')} - [${info.level}]: ${info.message}`
    ),
    winston.format.colorize({ all: true })
  )
})

export default logger
