'use strict'
import express from 'express'
// import timeout from 'connect-timeout'
// import { RateLimiterMemory } from 'rate-limiter-flexible'
import serverAPI from '../../lib'
const logger = global.logger

const router = express.Router()

/**
 * 使用中间件node-rate-limiter-flexible限制并发请求
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const reqLimiter = (req, res, next) => {
  logger.debug(`Connection: path: ${req.method} ${req.originalUrl}, from :${req.ip} `)
  next()
  // const opts = {
  //   points: 100,
  //   duration: 1 // Per second
  // }
  // const rateLimiter = new RateLimiterMemory(opts)
  // rateLimiter.consume(req.ip, 0)
  //   .then(() => {
  //     next()
  //   })
  //   .catch(_ => {
  //     logger.info(`Too Many Requests From ${req.ip}`)
  //     res.setHeader('Content-Type', 'application/json')
  //     return res.status(429).end('Too Many Requests')
  //   })
}
// const connectTimeout = timeout('10s')

router.use(reqLimiter, serverAPI)

export default router
